
importScripts('https://www.gstatic.com/firebasejs/8.6.3/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.6.3/firebase-messaging.js');
const firebaseConfig =  {
  apiKey: "AIzaSyCR2FEiueWED6JIdrnSneAKE21an33oGVM",
  authDomain: "capstone2-24fd3.firebaseapp.com",
  databaseURL: "https://capstone2-24fd3-default-rtdb.firebaseio.com",
  projectId: "capstone2-24fd3",
  storageBucket: "capstone2-24fd3.appspot.com",
  messagingSenderId: "569887834310",
  appId: "1:569887834310:web:28a25710c09ba7688557c7",
  measurementId: "G-YVV7JJ7J9T"
};

firebase.initializeApp(firebaseConfig);

// const storage = firebase.storage()
const messaging = firebase.messaging();