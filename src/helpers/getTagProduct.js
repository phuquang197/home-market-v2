export function getTag(tag) {
  var result = "";
  switch (tag) {
    case "FRUIT":
      result = "Trái Cây";
      break;
    case "DRINKS":
      result = "Đồ Uống";
      break;
    case "MEAT":
      result = "Thịt";
      break;
    case "SEA_FOOD":
      result = "Hải sản";
      break;
    case "MILK_AND_AGE":
      result = "Trứng Sữa";
      break;
    case "BREAD":
      result = "Bánh";
      break;
    case "FROZREN":
      result = "Đồ Đông Lạnh";
      break;
    case "ORGANIC":
      result = "Sản phẩm hữ cơ";
      break;
  }
  return result;
}

export function getTagToVN(tag) {
  var result = "";
  switch (tag) {
    case "Trái Cây":
      result = "FRUIT";
      break;
    case "Đồ Uống":
      result = "DRINKS";
      break;
    case "Thịt":
      result = "MEAT";
      break;
    case "Hải sản":
      result = "SEA_FOOD";
      break;
    case "Trứng Sữa":
      result = "MILK_AND_AGE";
      break;
    case "Bánh":
      result = "BREAD";
      break;
    case "Đồ Đông Lạnh":
      result = "FROZREN";
      break;
    case "Sản phẩm hữ cơ":
      result = "ORGANIC";
      break;
  }
  return result;
}

export function getPriceSort(tag) {
  var result = "";
  switch (tag) {
    case "Tăng dần":
      result = { sortBy: "price", sortDirection: "ASC" };
      break;
    case "Giảm dần":
      result = { sortBy: "price", sortDirection: "DESC" };
      break;
  }
  return result;
}

export function getGiaoHangStatus(paymentStatus) {
  var result = "";
  switch (paymentStatus) {
    case "GIAO_HANG":
      result = "Giao hàng";
      break;
    case "TU_LAY_HANG":
      result = "Tự đến lấy hàng";
      break;

  }
  return result;
}

export function gePaymentStatus(paymentStatus) {
  var result = "";
  switch (paymentStatus) {
    case "DIRECT_PAYMENT":
      result = "Thanh toán sau";
      break;
    case "PENDING":
      result = "Đang chờ thanh toán online";
      break;
    case "PAYMENTED":
      result = "Đã thanh toán";
      break;
    case "SUCCESS":
      result = "Rất tốt";
      break;
    case "ERROR":
      result = "Lỗi";
      break;
  }
  return result;
}
export function getDatHangStatus(DatHangStatus) {
  var result = "";
  switch (DatHangStatus) {
    case "PREPARING_ORDER":
      result = "Đang Soạn Hàng";
      break;
    

  }
  return result;
}
export function getcuahang(tencuahang) {
  var result = "";
  switch (tencuahang) {
    case "607f10a0a4521e067da79e33":
      result = "Annam Gourmet";
      break;
      case "607f111da4521e067da79e39":
        result = " Nam An Market";
        break;
        case "60a66b1b756e5f003b250248":
          result = "Lotte Mart";
          break;
          case "607f1183a4521e067da79e3b":
        result = "Cho Pho Fresh Food";
        break;
        case "607f0f78a4521e067da79e2d":
        result = "Tops Market	";
        break;
  }
  return result;
}
// 607f10a0a4521e067da79e33