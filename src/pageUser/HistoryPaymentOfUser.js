import React, { useState, useEffect } from "react";
import axios from "axios";
import Loadpage from "../component/Loadpage";
import { API } from "../config/ConfigENV";
import Menu from '../component/nav/Menu';
import Footer from '../component/Footer';
import { makeid } from "../helpers/create/create_key_index";

import PaymentHistory from "./PaymentHistory";
function HistoryPaymentOfUser() {
  const token = localStorage.getItem('access_token');
  const [payments, setPayments] = useState();
  const [page, setPage] = useState();
  const userID = localStorage.getItem("userID");
  // const userID = localStorage.getItem("userID");
  const nextPage = () => {
    setPage(page + 1);
};
const previousPage = () => {
    setPage(page - 1);
};
  const fetchData = async () => {
    const result = await axios({
      method: "get",
      url: `${API}/payment?limit=5${page}&createdBy=${userID}`,
      headers: {
          Authorization: `Bearer ${token}`,
      },
  });

  return result.data
  }

  useEffect(() => {
    fetchData().then(data => {

      setPayments(data)
    })
  }, [page])

  if (!payments || payments.lengh === 0) {
    return <Loadpage></Loadpage>
  }
 

  return (
    <>
      <Menu token={token}></Menu>

      <div className="container mt-5 mb-5 bg-white">
        <div className="row p-5">
          <h6 className="MenuRow text-center">LỊCH SỬ THANH TOÁN</h6>
          
        {
          
            payments.list.map((payment) => {
                return <PaymentHistory key={makeid(10)} payment={payment}></PaymentHistory>

            })
        }
        <div className="text-center mt-2 mb-3">
                    <button className="previousPage btn btn-primary mr-2" onClick={previousPage}>Ẩn Bớt</button>
                    <button className="nextPage btn btn-primary" onClick={nextPage}>Xem Thêm</button>
                </div>
        {/* <h6> Mã Đơn Hàng:  {payment.fullName}</h6> */}

        </div>
      </div>
      <Footer></Footer>
    </>
  );
}

export default HistoryPaymentOfUser;
