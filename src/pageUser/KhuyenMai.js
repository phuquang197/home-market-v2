import './Spkm.css';
import BannerPrd from '../component/BannerPrd';
import CardDemo2 from '../component/user/combined/CardDemo2';
import React, { useEffect, useState } from 'react';
import Menu from '../component/nav/Menu';
import Footer from '../component/Footer';
import Loadpage from "../component/Loadpage";
import { API } from "../config/ConfigENV"
import axios from 'axios';
import { makeid } from "../helpers/create/create_key_index";
import SlideSale from '../component/SlideSale';
// import Slide from '../component/Slide';

function KhuyenMai() {
    const token = localStorage.getItem('access_token')
    const [productPercentDiscount, setProductPercentDiscount] = useState();
    const fetchData = async () => {
        const result = await axios.get(`${API}/products?filterPercentDiscount=2`, {
        })

        return result.data
    }

    useEffect(() => {
        fetchData().then(data => {
            setProductPercentDiscount(data)
        })
    }, [])


    if (!productPercentDiscount || productPercentDiscount.lengh === 0) {
        return <Loadpage></Loadpage>
    }
    return (

        <>
            <Menu token={token}></Menu>
            <div className="mb-5">
            <div className="container ">
                <div className="row">
          <div className="col-md-3 bg-white">
                    <BannerPrd></BannerPrd>
                </div>
                <div className="col-md-9">
          <SlideSale></SlideSale>
          
               
                <div className="container p-0 mt-3">
                    <div className="row">
                    <div className="col-md-4">
                    <img className="w-100"  src="https://dl.airtable.com/.attachments/9aa5b7a5d908e2c007826ac280f711a3/7ce1a41b/fs-vi2x.jpg"></img>
                </div>
                <div className="col-md-4">
                <img className="w-100" src="https://dl.airtable.com/.attachments/46cc386371ef59da6de6c2a51b7291f2/3fc95d38/FS-vi2x-.jpg" ></img>

                </div>
                <div className="col-md-4">
                <img className="w-100" src="https://dl.airtable.com/.attachments/1b701bf439a2cc7282774e26e67c43f1/cee7af61/FS-vi2x.jpg" ></img>

                </div>
                    </div>
                </div>
                <div className="container bg-white">
                    <div className="row mt-3 p-4">
                        {
                            productPercentDiscount.list.map((product) => {
                                return <CardDemo2 key={makeid(10)} product={product}></CardDemo2>
                            })
                        }
                    </div>
                    </div>


</div>
</div>

                </div>
            </div>
            <Footer></Footer>
        </>
    );
}

export default KhuyenMai;