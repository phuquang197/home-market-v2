import React, { useContext, useEffect, useState } from "react";
import axios from "axios";
import { API } from "../../config/ConfigENV";
export default function Modal_layHang() {
  const token = localStorage.getItem("access_token");
  const addToCard = localStorage.getItem("addToCard");
  const editOne = async () => {
    let productId = document.getElementById("dataProductID").value;
    let amountData = document.getElementById("amount").value;
    const addToCardOBJ = JSON.parse(addToCard);
 
    //await window.location.reload();

    addToCardOBJ.forEach(product =>{
      if (product.productID === productId) {
        product.amount = amountData
      }
      localStorage.setItem("addToCard", JSON.stringify(addToCardOBJ));
    })
    await window.location.reload();
    // for (var i = 0; i <= addToCardOBJ.length - 1; i++) {
    //   console.log(i);
    //   console.log(addToCardOBJ[i].productID);
    //   console.log(productId);
    //   if (addToCardOBJ[i].productID === productId) {
    //     console.log('nook');
    //     addToCardOBJ.splice(i, 1);

    //   }
    //   console.log('ok');
    //   localStorage.setItem("addToCard", JSON.stringify(addToCardOBJ));

    // }

    // await window.location.reload();
  };


  return (
    <div
      className="modal fade"
      id="exampleModal232"
      tabIndex={-1}
      role="dialog"
      aria-labelledby="exampleModalLabel232"
      aria-hidden="true"
    >
      <div className="modal-dialog" role="document">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title" id="exampleModalLabel232">
              Chỉnh sửa
            </h5>
            <button
              type="button"
              className="close"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div className="modal-body">
            {/* style={{display: "none"}} */}
            <input
              id="dataProductID"
              defaultValue="d"
              style={{ display: "none" }}
            ></input>
             <input
              id="amount"
              defaultValue="d"
             type="number"
            ></input>
            <strong>
              Nhập số lượng bạn muốn chỉnh sửa
            </strong>
          </div>
          <div className="modal-footer">
            <button
              type="button"
              className="btn btn-secondary"
              data-dismiss="modal"
            >
              Không, Quay lại!
            </button>
            <button
              type="button"
              className="btn btn-primary"
              data-dismiss="modal"
              onClick={() => editOne()}
            >
              Chỉnh sửa
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
