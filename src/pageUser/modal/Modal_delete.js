import React from "react";
// import axios from "axios";
// import { API } from "../../config/ConfigENV";
export default function Modal_delete() {
  // const token = localStorage.getItem("access_token");
  const addToCard = localStorage.getItem("addToCard");
  const deleteOne = async () => {
    let productId = document.getElementById("dataProductID").value;
    const addToCardOBJ = JSON.parse(addToCard);

    //await window.location.reload();

    for (var i = 0; i <= addToCardOBJ.length - 1; i++) {

      if (addToCardOBJ[i].productID === productId) {

        addToCardOBJ.splice(i, 1);

      }
 
      localStorage.setItem("addToCard", JSON.stringify(addToCardOBJ));

    }

    await window.location.reload();
  };


  return (
    <div
      className="modal fade"
      id="exampleModal"
      tabIndex={-1}
      role="dialog"
      aria-labelledby="exampleModalLabel"
      aria-hidden="true"
    >
      <div className="modal-dialog" role="document">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title" id="exampleModalLabel">
              Cảnh báo!
            </h5>
            <button
              type="button"
              className="close"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div className="modal-body">
            {/* style={{display: "none"}} */}
            <input
              id="dataProductID"
              defaultValue="d"
              style={{ display: "none" }}
            ></input>
            <strong>
              Bạn chắc chắn muốn xoá sản phẩm này ra khỏi giỏ hàng?
            </strong>
          </div>
          <div className="modal-footer">
            <button
              type="button"
              className="btn btn-secondary"
              data-dismiss="modal"
            >
              Không, Quay lại!
            </button>
            <button
              type="button"
              className="btn btn-primary"
              data-dismiss="modal"
              onClick={() => deleteOne()}
            >
              Có, tôi muốn xoá
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
