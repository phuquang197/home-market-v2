
import React, { useEffect, useState} from 'react';
import axios from 'axios';
import { API } from "../config/ConfigENV";
import CardDemo from '../component/user/combined/CardDemo';
import { makeid } from "../helpers/create/create_key_index";
import {
  useParams
} from "react-router-dom";
function LikeProduct() {
    let { userID } = useParams();
    const [product, setProduct] = useState();
  const fetchData = async () => {
    const result = await axios({
      method: "PUT",
      url: `${API}/products/likeAndUnLike/${userID}`,
     
    });

    return result.data
  }

  useEffect(() => {
    fetchData().then(data => {
      setProduct(data)
    })
  }, [])

  if (!product || product.lengh === 0) {
    return ("")
  }
    return (
        <div>
            {
                  product.list.map((product) => {
                    return <CardDemo key={makeid(10)} product={product}></CardDemo>
                  })
              }
        </div>
    );
}

export default LikeProduct;