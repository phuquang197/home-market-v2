import React, { useEffect, useState } from 'react';
import CardDemo from '../../component/user/combined/CardDemo';
import axios from 'axios';
import { makeid } from "../../helpers/create/create_key_index";
import {API} from '../../config/ConfigENV';
function VegartableLotteMart(props) {
    const [vegatable , setVegatable] = useState([])
    const {storeOwnerID} = props
        useEffect(()=>{
            async  function getData(){
              let response = await axios.get(`${API}/products`)
                let product = response.data.list.filter(i => i.storeOwnerID._id === storeOwnerID && i.tag === 'ORGANIC')
                setVegatable(product)


        }
        getData()
        } , [])

    return (

        <div>
        <div className="container ">


          <div className="cardDemo">
            <div className="row mt-3 pl-3">
              {
                vegatable.map((vegatable) => {
                              return <CardDemo key={makeid(10)} product={vegatable}></CardDemo>
                            })
              }
            </div>
          </div>
          <div className="text-center m-4">
            <button type="button" className="btn btn-outline-success">Xem thêm</button>
          </div>
        </div>

    </div>
    );
}

export default VegartableLotteMart;