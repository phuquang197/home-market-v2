import React, { useEffect, useState } from 'react';
import CardDemo from '../../component/user/combined/CardDemo';
import axios from 'axios';
import { makeid } from "../../helpers/create/create_key_index";
import {API} from '../../config/ConfigENV';
function SeaFoodLotteMart(props) {
    const [seaFood , setSeaFood] = useState([])
    const {storeOwnerID} = props
    const token = localStorage.getItem('access_token');

        useEffect(()=>{
            async  function getData(){
              let response = await axios.get(`${API}/products`)
                let product = response.data.list.filter(i => i.storeOwnerID._id === storeOwnerID && i.tag === 'SEA_FOOD')
                setSeaFood(product)


        }
        getData()
        } , [])

    return (

        <div>
        <div className="container ">


          <div className="cardDemo">
            <div className="row mt-3 pl-3">
              {
                seaFood.map((seaFood) => {
                              return <CardDemo key={makeid(10)} product={seaFood}></CardDemo>
                            })
              }
            </div>
          </div>
          <div className="text-center m-4">
            <button type="button" className="btn btn-outline-success">Xem thêm</button>
          </div>
        </div>

    </div>
    );
}

export default SeaFoodLotteMart;