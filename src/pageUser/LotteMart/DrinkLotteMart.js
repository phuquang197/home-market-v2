import React, { useEffect, useState } from 'react';
import CardDemo from '../../component/user/combined/CardDemo';
import axios from 'axios';
import { makeid } from "../../helpers/create/create_key_index";
import {API} from '../../config/ConfigENV';
function DrinkLotteMart(props) {
    const [poultry , setPoultry] = useState([])
    const {storeOwnerID} = props

        useEffect(()=>{
            async  function getData(){
              let response = await axios.get(`${API}/products`)
                let product = response.data.list.filter(i => i.storeOwnerID._id === storeOwnerID && i.tag === 'DRINKS')
                setPoultry(product)


        }
        getData()
        } , [])

    return (

        <div>
        <div className="container ">
          <div className="cardDemo">
            <div className="row mt-3 pl-3">
              {
                poultry.map((poultry) => {
                              return <CardDemo key={makeid(10)} product={poultry}></CardDemo>
                            })
              }
            </div>
          </div>
          <div className="text-center mb-4">
            <button type="button" className="btn btn-outline-success">Xem thêm</button>
          </div>
        </div>
    </div>
    );
}

export default DrinkLotteMart;