import React, { useEffect, useState } from 'react';
import CardDemo from '../../component/user/combined/CardDemo';
import axios from 'axios';
import { makeid } from "../../helpers/create/create_key_index";
import {API} from '../../config/ConfigENV';
function FrozenfoodLotteMart(props) {
    const [frozren , setFrozren] = useState([])
    const {storeOwnerID} = props

        useEffect(()=>{
            async  function getData(){
              let response = await axios.get(`${API}/products`)
                let product = response.data.list.filter(i => i.storeOwnerID._id === storeOwnerID && i.tag === 'FROZREN')
                setFrozren(product)


        }
        getData()
        } , [])

    return (

        <div>
        <div className="container ">


          <div className="cardDemo">
            <div className="row mt-3 pl-3">
              {
                frozren.map((frozren) => {
                              return <CardDemo key={makeid(10)} product={frozren}></CardDemo>
                            })
              }
            </div>
          </div>
          <div className="text-center m-4">
            <button type="button" className="btn btn-outline-success">Xem thêm</button>
          </div>
        </div>

    </div>
    );
}

export default FrozenfoodLotteMart;