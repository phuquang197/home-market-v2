import React from "react";
import { NavLink } from "react-router-dom";

export default function InfoStore(props) {
  const { storeOwner } = props;
   
  return (
    <div
      style={{ borderBottom: "solid 1px #dfdfdf" }}
      className="row p-5 bg-white">
      <div className="col-md-5 m-0 p-0">
        <NavLink
          className="TopMarket"
          to={`/InformationStore/${storeOwner._id}`}
        >
          <img
            className="w-100"
            style={{ height: "254px" }}
            src={storeOwner.photos}
          ></img>
        </NavLink>
      </div>
      <div className="col-md-7 m-0 p-5 bg-white">
        <h1>
          <b>Giới thiệu về {storeOwner.name}</b>
        </h1>
        <h6 className="mt-4">{storeOwner.description}</h6>
      </div>
    </div>
  );
}

