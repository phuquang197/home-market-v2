import React from 'react';
import { NavLink } from "react-router-dom";

function InforUser(props) {
    const { user } = props;
    
    return (
        <div>
            <NavLink className="dropdown-item mb-2 MenuRow" to={`/InformationUser/${user._id}`}>
            Thông Tin Cá Nhân
          </NavLink>
        </div>
    );
}

export default InforUser;
