import React, { useEffect, useState } from 'react';
import axios from 'axios';
import Loadpage from "../component/Loadpage";
import { API } from "../config/ConfigENV";
import {
  useParams
} from "react-router-dom";
import Menu from '../component/nav/Menu';
import Footer from '../component/Footer';

function InfomationUser() {


  const token = localStorage.getItem('access_token');

  let { userID } = useParams();
  const [user, setUser] = useState();
  const fetchData = async () => {
    const result = await axios.get(`${API}/profile/${userID}`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    },
    )
    return result.data
  }

  useEffect(() => {
    fetchData().then(data => {
        setUser(data)
    })
  }, [])

  if (!user || user.lengh === 0) {
    return <Loadpage></Loadpage>
  }
  async function ResetProfile() {
    let response = await axios.put(`${API}/profile/${userID}`, {
        name: user.name,
        photos: user.photos,
        email: user.email,
        phone: user.phoneNumbers,
        address: user.address,
        status: 'UNLOCK'
    },
        {
            headers: {
                Authorization: `Bearer ${token}`,

            }
        }
    )
    await alert('Đã Cập Nhật');
}
    return (
      <>
      <Menu token={token}></Menu>
        <div className="mb-5 pt-5">
            <div className="container">
                <div style={{backgroundColor:'white',boxShadow: '0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22)'}} className="row p-4">
                    <div className="col-md-5 imgEdit">
                        <img className="w-100" src="https://firebasestorage.googleapis.com/v0/b/fir-firebase-4d563.appspot.com/o/images%2Flogo.png?alt=media&token=1f57f48d-ef22-46f2-a27e-c6099a6f662a"></img>
                    </div>
                   <div style={{}}  className=" col-md-7 editUser w-100 p-5">
                   <h5 className='MenuRow'>THÔNG TIN CÁ NHÂN</h5>
                   
                    <h6>Tên</h6>
                    <input type="text"
                        className="form-control mb-2" name="" id="" aria-describedby="helpId" placeholder={user.fullName} />
                    <h6>Email</h6>
                    <input type="email"
                        className="form-control mb-2" name="" id="" aria-describedby="helpId" placeholder={user.email} />
                    <h6>Số Điện Thoại</h6>
                    <input type="number"
                        className="form-control mb-2" name="" id="" aria-describedby="helpId" placeholder={user.phone} />
                    <h6>Địa Chỉ</h6>
                    <input type="text"
                        className="form-control" name="" id="" aria-describedby="helpId" placeholder={user.address} />
                    <button onClick={ResetProfile} type="button" className="btnsave p-2 mt-4">Lưu</button>
                   </div>
                </div>
            </div>
        </div>
        <Footer></Footer>
    </>
    );
}

export default InfomationUser;