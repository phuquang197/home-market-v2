import Menu from "../component/nav/Menu";
import Footer from "../component/Footer";
import Comment from "../component/Comment";
import React, { useEffect, useState } from "react";
import axios from "axios";
import Loadpage from "../component/Loadpage";
import { API } from "../config/ConfigENV";
import { getTag } from "../helpers/getTagProduct";
import { useParams } from "react-router-dom";
import Sphammuachung from "../component/Sphammuachung";
import NumberFormat from 'react-number-format';
import { getVND } from '../helpers/getVND';

function Product() {
  // let productId = document.getElementById("dataProductID").value
  const token = localStorage.getItem("access_token");
  const addToCard = localStorage.getItem("addToCard");
  const [shake, setShake] = useState(false);
  const addToCardOBJ = JSON.parse(addToCard);
  let { productID } = useParams();

  const [product, setProduct] = useState();

  const fetchData = async () => {
    const result = await axios.get(`${API}/products/${productID}`, {});
    return result.data;
  };

  useEffect(() => {
    fetchData().then((data) => {
      setProduct(data);
    });
  }, []);

  const upValueProductOnclick = () => {
    document.getElementById("soLuong").value =  parseInt(document.getElementById("soLuong").value)  +1
    document.getElementById("giaSanPham").innerHTML = getVND(parseInt(document.getElementById("soLuong").value)* product.price ,'')
  }

  const downValueProductOnclick = () => {
    if(parseInt(document.getElementById("soLuong").value)>0){
      document.getElementById("soLuong").value =  parseInt(document.getElementById("soLuong").value)  -1
    document.getElementById("giaSanPham").innerHTML = getVND(parseInt(document.getElementById("soLuong").value)* product.price ,'')
    }

    if(parseInt(document.getElementById("soLuong").value)<= 0){
      document.getElementById("soLuong").value =  1
      document.getElementById("giaSanPham").innerHTML = getVND(product.price ,'')
    }
  }

  async function LikeProduct() {
    let response = await axios.put(`${API}/products/${productID}`, {
      name: product.name,
      description: product.description,
      photos: product.photos,
      price: product.price,
      origin: product.origin,
      nextWeight: product.nextWeight,
      status: product.status,
      tradeMark: product.tradeMark,
      tag: product.tag,
      inCard: true,
      percentDiscount: product.percentDiscount,
      quantitySold: product.quantitySold,
      calories: product.calories,
      statusAccount: "UNLOCK",
      amount: product.amount,
    },
      {
        headers: {
          Authorization: `Bearer ${token}`,

        }
      }
    )
    await alert('Đã Thêm Vào yêu Thích');
  }
  if (!product || product.lengh === 0) {
    return <Loadpage></Loadpage>;
  }

  const addToCardHandle = () => {
    const amount = document.getElementById("soLuong").value;
    const productID = product._id;
    const price = product.price;
    const userCreatedProductID = product.createdBy;
    const name = product.name;
    const photo = product.photos;
    const storeOwnerID = product.storeOwnerID._id;
    const data = {
      amount,
      productID,
      price,
      userCreatedProductID,
      name,
      photo,
      storeOwnerID,
    };
    const isProduct = [];
    addToCardOBJ.forEach((valuePrduct) => {

      if (valuePrduct.productID.toString() === data.productID.toString()) {

        const dataSoLuong =
          parseInt(valuePrduct.amount) + parseInt(data.amount);

        valuePrduct.amount = dataSoLuong.toString();
        isProduct.push("isProduct");
      }
    });
    if (isProduct.length === 0) {
   
      addToCardOBJ.push(data);
    }

    localStorage.setItem("addToCard", JSON.stringify(addToCardOBJ));

    // Button begins to shake
    setShake(true);

    // Buttons tops to shake after 2 seconds
    setTimeout(() => setShake(false), 3000);
  };


  return (
    <>
      <Menu token={token}></Menu>

      <div style={{ marginTop: "45px" }}>

        <div>

          <div
            style={{
              backgroundColor: "#fff",
              borderColor: "#eee #eee #d5d5d5 #eee",
              boxShadow: "0 5px 0 rgb(200 200 200 / 20%)",
              zIndex: 10,
              position: "relative",
            }}
            className="container mb-5"
          >
            <img className={shake ? `shake` : null} id="imgAimasion"
              src={product.photos}
              alt="randomised!"
            />
            <div className="row">
              <div
                style={{ backgroundColor: "#fff" }}
                className="col-md-6 mt-4 sp"
              >
                <img
                  style={{ width: "400px", height: "400px" }}
                  src={product.photos}
                  alt=""
                />
              </div>
              <div className="col-md-6">
                <div className="titleProduct" style={{ borderBottom: "solid 1px silver" }} >
                  <h4 style={{ fontSize: "34px", fontWeight: "700", color: "black", textTransform: "uppercase" }} className="mt-3 mb-3">{product.name}
                   {
                     product.percentDiscount === 0 ? '' :<span style={{backgroundColor:'orange', color:"white", borderRadius:"13px"}} className="text-right bg-orange m-3 p-1">{product.percentDiscount}%</span>
                   }

                   </h4>

                </div>

                <h3 style={{ fontSize: "34px", color: "black" }} className="gia pt-4 pb-4"> <NumberFormat id="giaSanPham" value={((product.price)-(product.percentDiscount)/100*(product.price))} displayType={'text'} thousandSeparator={true} /> VNĐ</h3>
                {/* <div className="btn-group"> */}
                <div className="soluong tanggiam sl" >
                  <button onClick={() => downValueProductOnclick() } type="button" className="btn sl minus">-</button>
                  {/* <div style={{ width: '35px' }} className="soluong sl" /> */}
                  {/* <p
                      className="soluong sl pl-2 pr-2" type="number" style={{width: "44px"}} id="soLuong"> {count}</p> */}
                  <input
                    className="soluong sl text-center"
                    type="text"
                    id="soLuong"
                    defaultValue={1}
                    min={1}
                  ></input>

                  <button onClick={() =>upValueProductOnclick()

                        } type="button" className="btn sl sum">+</button>
                </div>
                <button
                  type="button"
                  className="btn addcart ml-2"
                  onClick={() => addToCardHandle()}
                >
                  <i className="fa fa-cart-plus" aria-hidden="true" /> THÊM
                    VÀO GIỎ HÀNG
                  </button>
                <>

                </>
                {/* <button onClick={myMove()}>Click Me</button>
                  <div id="myContainer" style={{ bottom: styleTop }}></div> */}
                {/* </div> */}
                <div className="mt-4 mb-4">{product.description}</div>

                <button onClick={LikeProduct} type="button" className="btn btn-danger p-3 btn-icon-split mr-2">
                  <i className="fa fa-heart mr-2" aria-hidden="true" /> Thêm vào
                  danh sách yêu thích
                </button>
                <div className="mt-4 mb-4">
                  <div>
                    <div className="icon_fb icon2 mr-2">
                      <i className="fa fa-facebook p-2" aria-hidden="true" />
                    </div>
                    <div className="icon_tw icon2 mr-2">
                      <i className="fa fa-twitter p-2 " aria-hidden="true" />
                    </div>
                    <div className="icongm icon2 mr-2">
                      <i className="fa fa-youtube p-2" aria-hidden="true" />
                    </div>
                    <div className="iconpinterest icon2 mr-2">
                      <i className="fa fa-pinterest p-2" aria-hidden="true" />
                    </div>
                    <div className="iconinsta icon2">
                      <i className="fa fa-instagram p-2" aria-hidden="true" />
                    </div>
                  </div>
                </div>
                <div className="mt-4">
                  <li>Miễn phí vận chuyển trong vòng 10km </li>
                  <li>Thời gian giao hàng 30-60p kể từ khi đặt hàng</li>
                  <li> Giảm giá thành viên</li>
                  <p>
                    {" "}
                    <b style={{ color: "#80b435" }}>Mã:</b> KJ7SA62{" "}
                    <b style={{ color: "#80b435" }}>Danh mục:</b>{" "}
                    {getTag(product.tag)}
                  </p>
                </div>
              </div>
            </div>
          </div>
          <Comment description={product.description}></Comment>
          <br></br>
          <div
            style={{ backgroundColor: "#fff" }}
            className="container p-4 mb-5"
          >
            <h4>Sản Phẩm Cùng Loại</h4>
            <Sphammuachung product={product}></Sphammuachung>
          </div>
        </div>
      </div>
      <Footer></Footer>
    </>
  );
}

export default Product;