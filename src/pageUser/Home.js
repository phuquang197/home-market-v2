import Danhmucsp from "../component/Danhmucsp";
import Sphamkhuyenmai from "../component/Sphamkhuyenmai";
import Store from "../component/Store";
import Spbanchay from "../component/user/Spbanchay";
import React, { useEffect, useState } from "react";
import axios from "axios";
import Loadpage from "../component/Loadpage";
import { API } from "../config/ConfigENV";
import { getBrowserToken, messaging } from "../firebase/Firebase";
import Slide from "../component/Slide";
import Footer from "../component/Footer";
import Menu from "../component/nav/Menu";
import { NavLink } from "react-router-dom";
import MenuLeftOfUser from "../component/MenuLeftOfUser";

let timeout
function Home() {
  const token = localStorage.getItem("access_token");
  const [product, setProduct] = useState();
  const [isShow, setIsShow] = useState(false);
  const [contentMessage, setContentMessage] = useState({
    title: '',
    body: ''
  })
  const fetchData = async () => {
    const result = await axios.get(`${API}/products`, {});

    return result.data;
  };
  getBrowserToken().then((data) => {
    localStorage.setItem('token_fcm', data);
  });

  useEffect(() => {
    fetchData().then((data) => {
      setProduct(data);
    });

    messaging.onMessage((payload) => {
      if (timeout) clearTimeout(timeout);
      setIsShow(true)
      setContentMessage({
        title: payload.notification.title,
        body: payload.notification.body
      })
      timeout = setTimeout(() => {
        setIsShow(false)
      }, 4000)
      console.log('payload', payload);
    });

  }, []);

  if (!product || product.lengh === 0) {
    return <Loadpage></Loadpage>;
  }

  return (
    <div className="index">
      <Menu token={token}></Menu>
      <div className="container mt-3">
        <div className="row">
          {/* <div className="col-md-3 MenuRow bg-white">
            <ul className="nav flex-column">
              <div className="card-header">
                <h6 className="MenuColumn active"> NGÀNH HÀNG</h6>
              </div>
              <ul style={{ borderBottom: "1px solid #f0f0f0" }} className="list-group list-group-flush mb-2">
                <li className="list-group-item"><NavLink className="MenuColumn" to="/Beat">Thịt</NavLink></li>
                <li className="list-group-item"><NavLink className="MenuColumn" to="/Seafood">Hải Sản</NavLink></li>
                <li className="list-group-item"><NavLink className="MenuColumn" to="/Drinks">Nước Uống</NavLink></li>
                <li className="list-group-item"><NavLink className="MenuColumn" to="/MilkEgg">Trứng, Sữa</NavLink></li>
                <li className="list-group-item"><NavLink className="MenuColumn" to="/Frozenfood">Đồ Đông Lạnh</NavLink></li>
                <li className="list-group-item"><NavLink className="MenuColumn" to="/Fruit">Trái Cây</NavLink></li>
                <li className="list-group-item"><NavLink className="MenuColumn" to="/Bread">Bánh Mì</NavLink></li>
                <li className="list-group-item"><NavLink className="MenuColumn" to="/Vegartable">Rau, Củ</NavLink></li>
              </ul>
              <img className="w-100" src="https://demo.templatetrip.com/Opencart/OPC02/OPC040/OPC05/image/cache/catalog/demo/banners/left-banner-1-274x500.jpg"></img>
             
              <h5 class="text-center mt-3 mb-3 MenuColumn">Đánh Gía Về HomeMarket</h5>
              <div id="carouselExampleIndicators" className="carousel slide mt-3 mb-3" data-ride="carousel">
                <ol className="carousel-indicators">
                  <li data-target="#carouselExampleIndicators" data-slide-to={0} className="active" />
                  <li data-target="#carouselExampleIndicators" data-slide-to={1} />
                  <li data-target="#carouselExampleIndicators" data-slide-to={2} />
                </ol>
                <div className="carousel-inner">
                  <div className="carousel-item active">
                    <div className="w-43 mt-3 mb-4 border-bottom" style={{ width: "43%" }} className="media">

                      <img style={{ border: "solid 1px #eeeeee", borderRadius:"50%", height:"72px" }} className="w-100 mr-2" src="https://by.com.vn/tYkqJW" alt="..." />
                      <div className="media-body MenuColumn mt-2">
                        <h6 className="mt-0" style={{ width: "159px", color: " #666666", fontWeight: "400" }}>Lê Đức Dũng</h6>
                        <small>Sinh Viên Đại Học Duy Tân</small>
                      </div>
                    </div>
                    <div style={{color:"#888", fontFamily: "serif"}} className="w-100">
                    <i>"Nhờ Có HomeMartket việc đi chợ của tôi dễ dàng hơn không còn tốn nhiều thời gian như lúc trước"</i>
                    </div>
                  </div>
                  <div className="carousel-item">
                    <div className="w-43 mt-3 mb-4 border-bottom" style={{ width: "43%" }} className="media">

                      <img style={{ border: "solid 1px #eeeeee", borderRadius:"50%", height:"72px" }} className="w-100 mr-2" src="https://by.com.vn/tYkqJW" alt="..." />
                      <div className="media-body MenuColumn mt-2">
                        <h6 className="mt-0" style={{ width: "159px", color: " #666666", fontWeight: "400" }}>Lê Đức Dũng</h6>
                        <small>Sinh Viên Đại Học Duy Tân</small>
                      </div>

                    </div>
                    <div style={{color:"#888", fontFamily: "serif"}} className="w-100">
                    <i>"Nhờ Có HomeMartket việc đi chợ của tôi dễ dàng hơn không còn tốn nhiều thời gian như lúc trước"</i>
                    </div>
                  </div>
                  <div className="carousel-item">
                    <div className="w-43 mt-3 mb-4 border-bottom" style={{ width: "43%" }} className="media">

                      <img style={{ border: "solid 1px #eeeeee", borderRadius:"50%", height:"72px" }} className="w-100 mr-2" src="https://by.com.vn/tYkqJW" alt="..." />
                      <div className="media-body MenuColumn mt-2">
                        <h6 className="mt-0" style={{ width: "159px", color: " #666666", fontWeight: "400" }}>Lê Đức Dũng</h6>
                        <small>Sinh Viên Đại Học Duy Tân</small>
                      </div>
                    </div>
                    <div style={{color:"#888", fontFamily: "serif"}} className="w-100">
                    <i>Nhờ Có HomeMartket việc đi chợ của tôi dễ dàng hơn không còn tốn nhiều thời gian như lúc trước</i>
                    </div>
                  </div>
                </div>
                <a className="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                  <span className="carousel-control-prev-icon" aria-hidden="true" />
                  <span className="sr-only">Previous</span>
                </a>
                <a className="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                  <span className="carousel-control-next-icon" aria-hidden="true" />
                  <span className="sr-only">Next</span>
                </a>
              </div>


              <div style={{ borderBottom: "solid 1px #666666" }}>
                <h5 className="text-center mt-3 mb-3 MenuColumn">Sản Phẩm Nổi Bật</h5>
                <a href="Product2/609a2078a32e59003bcf187f" className="w-43 mt-3 mb-4 border-bottom" style={{ width: "43%" }} className="media">

                  <img style={{ border: "solid 1px #eeeeee" }} className="w-100 mr-1" src="https://firebasestorage.googleapis.com/v0/b/capstone2-24fd3.appspot.com/o/product%2Fmeat%2Fphi_l%C3%AA_b%C3%B2-removebg-preview.png?alt=media&token=48937fbe-02bb-4463-93b4-f3d88c3d1525" alt="..." />
                  <div className="media-body MenuColumn">
                    <h6 className="mt-0" style={{ width: "159px", color: " #666666", fontWeight: "400" }}>Phi Lê Bò</h6>
                    <small>LotteMart</small>
                    <p style={{ color: "#08c25e" }}>207.000 VNĐ</p>
                  </div>
                </a>

                <a href="Product2/609a2078a32e59003bcf187f" className="w-43 mt-3 mb-4" style={{ width: "43%" }} className="media">

                  <img style={{ border: "solid 1px #eeeeee" }} className="w-100 mr-1" src="https://firebasestorage.googleapis.com/v0/b/capstone2-24fd3.appspot.com/o/product%2Fmeat%2Fphi_l%C3%AA_b%C3%B2-removebg-preview.png?alt=media&token=48937fbe-02bb-4463-93b4-f3d88c3d1525" alt="..." />
                  <div className="media-body MenuColumn">
                    <h6 className="mt-0" style={{ width: "159px", color: " #666666", fontWeight: "400" }}>Phi Lê Bò</h6>
                    <small>LotteMart</small>
                    <p style={{ color: "#08c25e" }}>207.000 VNĐ</p>
                  </div>
                </a>
                <a href="Product2/609a2078a32e59003bcf187f" className="w-43 mt-3 mb-4" style={{ width: "43%" }} className="media">

                  <img style={{ border: "solid 1px #eeeeee" }} className="w-100 mr-1" src="https://firebasestorage.googleapis.com/v0/b/capstone2-24fd3.appspot.com/o/product%2Fmeat%2Fphi_l%C3%AA_b%C3%B2-removebg-preview.png?alt=media&token=48937fbe-02bb-4463-93b4-f3d88c3d1525" alt="..." />
                  <div className="media-body MenuColumn">
                    <h6 className="mt-0" style={{ width: "159px", color: " #666666", fontWeight: "400" }}>Phi Lê Bò</h6>
                    <small>LotteMart</small>
                    <p style={{ color: "#08c25e" }}>207.000 VNĐ</p>
                  </div>
                </a>
              </div>
              <div>
                <h6 className="text-center mt-3 mb-3 MenuColumn"><a href="/KhuyenMai" className="text-center mt-3 mb-3 MenuColumn">Sản Phẩm Khuyến Mãi</a></h6>
                <a href="Product2/609a2078a32e59003bcf187f" className="w-43 mt-3 mb-4 border-bottom" style={{ width: "43%" }} className="media">

                  <img style={{ border: "solid 1px #eeeeee" }} className="w-100 mr-1" src="https://firebasestorage.googleapis.com/v0/b/capstone2-24fd3.appspot.com/o/product%2Fmeat%2Fphi_l%C3%AA_b%C3%B2-removebg-preview.png?alt=media&token=48937fbe-02bb-4463-93b4-f3d88c3d1525" alt="..." />
                  <div className="media-body MenuColumn">
                    <h6 className="mt-0" style={{ width: "159px", color: " #666666", fontWeight: "400" }}>Phi Lê Bò
                  <span className="text-left m-2 bg-orange"> 30%</span>
                    </h6>
                    <small>LotteMart</small>
                    <p style={{ color: "#08c25e", fontWeight: "700", fontSize: "13px" }}> <strike><small style={{ color: "#888" }}>207,000VNĐ</small> </strike> 154.000 VNĐ</p>
                  </div>
                </a>

                <a href="Product2/609a2078a32e59003bcf187f" className="w-43 mt-3 mb-4 border-bottom" style={{ width: "43%" }} className="media">

                  <img style={{ border: "solid 1px #eeeeee" }} className="w-100 mr-1" src="https://firebasestorage.googleapis.com/v0/b/capstone2-24fd3.appspot.com/o/product%2Fmeat%2Fphi_l%C3%AA_b%C3%B2-removebg-preview.png?alt=media&token=48937fbe-02bb-4463-93b4-f3d88c3d1525" alt="..." />
                  <div className="media-body MenuColumn">
                    <h6 className="mt-0" style={{ width: "159px", color: " #666666", fontWeight: "400" }}>Phi Lê Bò
                  <span className="text-left m-2 bg-orange"> 30%</span>
                    </h6>
                    <small>LotteMart</small>
                    <p style={{ color: "#08c25e", fontWeight: "700", fontSize: "13px" }}> <strike><small style={{ color: "#888" }}>207,000VNĐ</small> </strike> 154.000 VNĐ</p>
                  </div>
                </a>
                <a href="Product2/609a2078a32e59003bcf187f" className="w-43 mt-3 mb-4 border-bottom" style={{ width: "43%" }} className="media">

                  <img style={{ border: "solid 1px #eeeeee" }} className="w-100 mr-1" src="https://firebasestorage.googleapis.com/v0/b/capstone2-24fd3.appspot.com/o/product%2Fmeat%2Fphi_l%C3%AA_b%C3%B2-removebg-preview.png?alt=media&token=48937fbe-02bb-4463-93b4-f3d88c3d1525" alt="..." />
                  <div className="media-body MenuColumn">
                    <h6 className="mt-0" style={{ width: "159px", color: " #666666", fontWeight: "400" }}>Phi Lê Bò
                  <span className="text-left m-2 bg-orange"> 30%</span>
                    </h6>
                    <small>LotteMart</small>
                    <p style={{ color: "#08c25e", fontWeight: "700", fontSize: "13px" }}> <strike><small style={{ color: "#888" }}>207,000VNĐ</small> </strike> 154.000 VNĐ</p>
                  </div>
                </a>
              </div>

            </ul>
          </div> */}
          <div className="col-md-3 MenuRow bg-white">
          <MenuLeftOfUser></MenuLeftOfUser>

          </div>
          <div className="col-md-9">
            <div className="Home">
              <div className="cha mb-2">
                <div className="row">
                  <div className="col-md-12 mb-4">
                    <Slide></Slide>
                    {/* <Danhmucsp></Danhmucsp> */}
                  </div>
                  <div className="col-md-6 ">
                    <div>
                      <img className="w-100" src="https://dl.airtable.com/.attachments/0fcb4191a2285e2eb836436198af9257/2fb3a9c8/FS-vi.jpg"></img>
                    </div>
                    {/* <div>
                      <img className="w-100" src="https://dl.airtable.com/.attachments/3ea01387368d6e6cab1d65ae7b4982e8/8a65cfef/FS-vi2x.jpg"></img>
                    </div> */}
                  </div>
                  <div className="col-md-6">
                    {/* <div>
                      <img className="w-100" src="https://dl.airtable.com/.attachments/0fcb4191a2285e2eb836436198af9257/2fb3a9c8/FS-vi.jpg"></img>
                    </div> */}
                    <div>
                      <img className="w-100" src="https://dl.airtable.com/.attachments/3ea01387368d6e6cab1d65ae7b4982e8/8a65cfef/FS-vi2x.jpg"></img>
                    </div>
                  </div>
                </div>
              </div>
              <Spbanchay product={product}></Spbanchay>
              <Sphamkhuyenmai></Sphamkhuyenmai>
              <Store></Store>
            </div>
          </div>
        </div>
      </div>



      <Footer></Footer>
    </div>
  );
}
export default Home;
