import React from 'react';
import BannerPrd from '../component/BannerPrd';
import Menu from '../component/nav/Menu';
import Footer from '../component/Footer';
import Meat from '../component/Meat';
import IndustrySeaFood from '../component/IndustrySeaFood';
import IndustryMikeEgg from '../component/IndustryMikeEgg';
import IndustryDrink from '../component/IndustryDrink';
import IndustryFrozen from '../component/IndustryFrozen';
import IndustryOganic from '../component/IndustryOganic';
import IndustryBread from '../component/IndustryBread';
import MenuLeftOfUser from '../component/MenuLeftOfUser';
import SlideSale from '../component/SlideSale';


function Industry(props) {
    const { product } = props;
    const token = localStorage.getItem('access_token');

    return (
        <>
            <Menu token={token}></Menu>
            <div className="mb-5">
            <div className="container mt-3 ">
                <div className="row">
                   <div className="col-md-3 bg-white">
                   <MenuLeftOfUser></MenuLeftOfUser>
                   </div>
                    <div className="col-md-9">
                        <SlideSale></SlideSale>
                    <h5 className="mt-4">THỊT</h5>
                    <div style={{ backgroundColor: '#fff' }} className="p-4 mb-5">
                       <Meat product={product}></Meat>
                       
                   </div>
                    
                    <h5 className="mt-4">HẢI SẢN</h5>
                    <div style={{ backgroundColor: '#fff' }} className="p-4 mb-5">
                       <IndustrySeaFood></IndustrySeaFood>
                    </div>
                    <h5 className="mt-4">TRỨNG SỮA</h5>
                    <div style={{ backgroundColor: '#fff' }} className="p-4 mb-5">
                        <IndustryMikeEgg></IndustryMikeEgg>
                    </div>
                    <h5 className="mt-4">NƯỚC UỐNG</h5>
                    <div style={{ backgroundColor: '#fff' }} className="p-4 mb-5">
                        <IndustryDrink></IndustryDrink>
                    </div>
                    <h5 className="mt-4">BÁNH</h5>
                    <div style={{ backgroundColor: '#fff' }} className="p-4 mb-5">
                        <IndustryBread></IndustryBread>
                    </div>
                    <h5 className="mt-4">ĐỒ ĐÔNG LẠNH</h5>
                    <div style={{ backgroundColor: '#fff' }} className="p-4 mb-5">
                       <IndustryFrozen></IndustryFrozen>
                        
                    </div>
                    <h5 className="mt-4">RAU CỦ QUẢ</h5>
                    <div style={{ backgroundColor: '#fff' }} className="p-4 mb-5">
                        <IndustryOganic></IndustryOganic>
                        
                    </div> 
                    </div>
                </div>
                </div>
                <div className="container mt-5">
                    
                               
                    </div>
            </div>
            <Footer></Footer>
        </>
    );
}

export default Industry;