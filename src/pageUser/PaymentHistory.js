
import React from 'react';
import NumberFormat from 'react-number-format';
import { getGiaoHangStatus } from "../helpers/getTagProduct";
import { gePaymentStatus } from "../helpers/getTagProduct";
import { getDatHangStatus } from "../helpers/getTagProduct";
import { makeid } from "../helpers/create/create_key_index";
// gePaymentStatus
function PaymentHistory(props) {

    const { payment } = props;
    return (
        <div className="bg-white">
            <div className=" mb-2  " style={{ color: 'black !important' }}>
                {/* {payment.title} <br></br>     */}
                <div className="container p-3">
                    <div className="row">
                        <div className="col-md-3">
                            <img className="mt-2" style={{ height: "100px" }} src={payment.store_Owners?.photos ? 
                            payment.store_Owners?.photos : '' }></img>
                        </div>
                        <div className="col-md-9">
                            {
                                payment.productIDs.map((product) => {
                                    return (
                                        <div key={makeid(10)} style= {{borderBottom:"solid 1px #eeeeee"}}> 
                                            <div style={{display:"inline-block"}} className="productPayment"><img className="mt-2" style={{ height: "100px", width:"150px" }} src={product.photo}></img> </div>
                                            <div style={{display:"inline-block"}} className="productPayment">
                                            Tên Sản Phẩm : {product.name}<br></br>
                                            Số Lượng: {product.amount}<br></br>
                                            </div>
                                            
                                        </div>
                                    )
                                })
                            }
                            

                Mã Thanh Toán:   <div style={{ fontSize: "25px !important", color: "#08c25e " }} className=" MTT"> <b>{payment.paymentCode}</b> </div> <br></br>
                Tên Cửa Hàng: {payment.store_Owners.name} <br></br>
                 Hình Thức Nhận Hàng:  {getGiaoHangStatus(payment.giaoHangStatus)}
                 Hình Thức Thanh Toán: {gePaymentStatus(payment.paymentStatus)}<br></br>
                 Tổng số tiền:<NumberFormat value={payment.totalMoney} displayType={'text'} thousandSeparator={true} />VNĐ
                <br></br>
                 Tình Trạng Đơn Hàng: {getDatHangStatus(payment.order)}
                        </div>
                    </div>
                </div>
                <div style={{ borderBottom: "solid 1px black" }}></div>
            </div>
        </div>
    );
}

export default PaymentHistory;

