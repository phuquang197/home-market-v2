import Menu from '../component/nav/Menu';
import Footer from '../component/Footer';
import React, {useEffect, useState } from 'react';
import axios from 'axios';
import Loadpage from "../component/Loadpage";
import { API } from "../config/ConfigENV";
import InfoStore from "./store/InfoStore";
import {makeid} from '../helpers/create/create_key_index';
function ManyStore() {

    const token = localStorage.getItem('access_token');

    const [storeOwners, setStoreOwners] = useState();
    const fetchData = async () => {
        const result = await axios.get(`${API}/storeOwners?status=ACCEPTED`, {
        })

      return result.data
    }

    useEffect(() => {
      fetchData().then(data => {
        setStoreOwners(data)
      })
    }, [ ])

  if(!storeOwners || storeOwners.lengh === 0 ){
    return <Loadpage></Loadpage>
  }

    return (
        <>
            <Menu token={token}></Menu>
            <div className="container pb-5 pt-5">
              {
                storeOwners.list.map((storeOwner) => {
                  return <InfoStore key={makeid(10)} storeOwner={storeOwner}></InfoStore>
                })
              }
            </div>
            <Footer></Footer>
        </>
    )
}


export default ManyStore;