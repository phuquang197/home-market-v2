

import React, { useEffect, useState } from 'react';
import axios from 'axios';
// import { API } from "../config/ConfigENV";
import Card from '../component/user/combined/Card';
import { makeid } from '../helpers/create/create_key_index';
function Search() {

    const [product, setProduct] = useState([]);
    const token = localStorage.getItem("access_token");


    useEffect(()=>{
       async function getData(){
        let response = await axios.get('http://production-hm-api.eu-4.evennode.com/products' , {
            headers :{
            Authorization: `Bearer ${token}`,
            }
        })
                localStorage.setItem('item' , JSON.stringify(response.data.list))

        }
        getData()

    },[])
    function searchProduct(event) {
        let query = event.target.value.toLowerCase()
        let data = JSON.parse(localStorage.getItem('item'))
        let result = data.filter(i => i.name.toLowerCase().indexOf(query) !== -1)
        setProduct(result)
    }

    return (
        <div className="index bg-white">

            <div className="container-fuild p-0 pt-3 mb-4 bg-white">
                <div className="row text-center">
                    <h6 className="text-center m-3 pt-4">Bạn Đang Tìm Gì</h6>
                    <div className="text-center">
                    <input style={{maxWidth:"500px", margin:"0 auto"}} onChange={searchProduct} type="search"
                        className="form-control search mb-5 " name="" id="" aria-describedby="helpId" placeholder="Search" />
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-2">
                        <img className="w-100" src="https://dl.airtable.com/.attachments/9aa5b7a5d908e2c007826ac280f711a3/7ce1a41b/fs-vi2x.jpg"></img>
                    </div>
                    <div className="col-md-2">
                        <img className="w-100" src="https://dl.airtable.com/.attachments/46cc386371ef59da6de6c2a51b7291f2/3fc95d38/FS-vi2x-.jpg" ></img>

                    </div>
                    <div className="col-md-2">
                        <img className="w-100" src="https://dl.airtable.com/.attachments/1b701bf439a2cc7282774e26e67c43f1/cee7af61/FS-vi2x.jpg" ></img>

                    </div>
                    <div className="col-md-2">
                        <img className="w-100" src="https://dl.airtable.com/.attachments/1b701bf439a2cc7282774e26e67c43f1/cee7af61/FS-vi2x.jpg" ></img>

                    </div>
                    <div className="col-md-2">
                        <img className="w-100" src="https://dl.airtable.com/.attachments/6186e8b776f9912f3a223db36b7bb21e/7ed2585e/FS-vi2x.jpg" ></img>

                    </div>
                    <div className="col-md-2">
                        <img className="w-100" src="https://dl.airtable.com/.attachments/782a0a88c5a93d45fb5d914de8a72f3d/82b066af/FS-vi2x-.jpg" ></img>

                    </div>
                </div>
            </div>
            <div className="container mt-5 bg-white w-100">

                <div className="row">
                    {

                        product.map((product) => {
                            return <Card className="col-md-3" key={makeid(10)} oneProduct={product} > </Card>

                        })

                    }
                </div>
            </div>
        </div>
    );
}
export default Search;
