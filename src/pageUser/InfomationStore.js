import './Spkm.css';
import Menu from '../component/nav/Menu';
import Footer from '../component/Footer';
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import Loadpage from "../component/Loadpage";
import { API } from "../config/ConfigENV";
import {
  useParams
} from "react-router-dom";
import BeatLotteMart from './LotteMart/BeatLotteMart';
import FruitLotteMart from './LotteMart/FruitLotteMart';
import MilkEggLotteMart from './LotteMart/MilkEggLotteMart';
import DrinkLotteMart from './LotteMart/DrinkLotteMart';
import SeaFoodLotteMart from './LotteMart/SeaFoodLotteMart';
import FrozenfoodLotteMart from './LotteMart/FrozenfoodLotteMart';
import VegartableLotteMart from './LotteMart/VegartableLotteMart';
import BreadLotteMart from './LotteMart/BreadLotteMart';
import MenuLeftOfUser from '../component/MenuLeftOfUser';
function InfomationStore(props) {

  const token = localStorage.getItem('access_token');

  let { storeOwnerID } = useParams();

  const [storeOwner, setStoreOwner] = useState();

  const fetchData = async () => {
    const result = await axios.get(`${API}/storeOwners/${storeOwnerID}`, {
    })
    return result.data
  }

  useEffect(() => {
    fetchData().then(data => {
      setStoreOwner(data)
    })
  }, [])




  if (!storeOwner || storeOwner.lengh === 0) {
    return <Loadpage></Loadpage>
  }

 
  return (
    <>
      <Menu token={token}></Menu>

        <div className="container mt-5">
          <div className="row ">
            <div className="col-md-3 bg-white">
              <MenuLeftOfUser></MenuLeftOfUser>
            </div>
            <div className="col-md-9">
            <div style={{ backgroundColor: 'white', boxShadow: ' 0 5px 0 rgb(200 200 200 / 20%)', backgroundColor: "white" }} className="container ">
            <div className="row">
            <div className="col-md-6">
              <img src={storeOwner.photos} className="d-block w-100" alt="..." />
            </div>
            <div className="col-md-6 p-4">
              <h4 className='nameStore mb-4'>{storeOwner.name}</h4>
              {storeOwner.description}
            </div>
            </div>
          </div>
        
        <h5 className="mt-4"> THỊT</h5>
          {/* <BeatLotteMart storeOwnerID={storeOwnerID} ></BeatLotteMart> */}
          {/* <h5 className="mt-4">HẢI SẢN</h5> */}
                    <div style={{ backgroundColor: '#fff' }} className="p-4 mb-5">
                    <BeatLotteMart storeOwnerID={storeOwnerID} ></BeatLotteMart>
                    </div>
        <h5 className="mt-4"> TRÁI CÂY</h5>

        <div style={{ backgroundColor: '#fff' }} className="p-4 mb-5">
                    <FruitLotteMart storeOwnerID={storeOwnerID} ></FruitLotteMart>
                    </div>
      
        <h5 className="mt-4"> TRỨNG & SỮA</h5>
          <div style={{ backgroundColor: '#fff' }} className="p-4 mb-5">
          <MilkEggLotteMart storeOwnerID={storeOwnerID} ></MilkEggLotteMart>
                    </div>
        <h5 className="mt-4"> NƯỚC UỐNG</h5>
          <div style={{ backgroundColor: '#fff' }} className="p-4 mb-5">
          <DrinkLotteMart storeOwnerID={storeOwnerID} ></DrinkLotteMart>
                    </div>
        <h5 className="mt-4"> HẢI SẢN</h5>
          <div style={{ backgroundColor: '#fff' }} className="p-4 mb-5">
          <SeaFoodLotteMart storeOwnerID={storeOwnerID} ></SeaFoodLotteMart>
                    </div>
        <h5 className="mt-4"> THỰC PHẨM ĐÔNG LẠHN</h5>
          <div style={{ backgroundColor: '#fff' }} className="p-4 mb-5">
          <FrozenfoodLotteMart storeOwnerID={storeOwnerID} ></FrozenfoodLotteMart>
                    </div>
        <h5 className="mt-4"> RAU CỦ</h5>
          <div style={{ backgroundColor: '#fff' }} className="p-4 mb-5">
          <VegartableLotteMart storeOwnerID={storeOwnerID} ></VegartableLotteMart>
                    </div>
        <h5 className="mt-4"> BÁNH</h5>
          <div style={{ backgroundColor: '#fff' }} className="p-4 mb-5">
          <BreadLotteMart storeOwnerID={storeOwnerID} ></BreadLotteMart>
                    </div>
            </div>
          </div>
          
          </div>
      <Footer></Footer>
    </>
  );
}

export default InfomationStore;