import { NavLink } from "react-router-dom";
import Menu from "../component/nav/Menu";
import Footer from "../component/Footer";
import React, { useEffect, useState } from "react";
import axios from "axios";
import Loadpage from "../component/Loadpage";
import { API } from "../config/ConfigENV";
import InfoStore from "./store/InfoStore";
import { makeid } from "../helpers/create/create_key_index";

function CartOfStore() {
  const token = localStorage.getItem("access_token");
  const addToCard = localStorage.getItem("addToCard");
  const addToCardOBJ = JSON.parse(addToCard);
  let dem = 0;

  const [storeOwners, setStoreOwners] = useState();
  const fetchData = async () => {
    const result = await axios.get(`${API}/storeOwners?status=ACCEPTED`, {});

    return result.data;
  };

  useEffect(() => {
    fetchData().then((data) => {
      setStoreOwners(data);
    });
  }, []);

  if (!storeOwners || storeOwners.lengh === 0) {
    return <Loadpage></Loadpage>;
  }

  const  addToCardID= addToCardOBJ.map(value => value.storeOwnerID)
  const data = storeOwners.list.filter(value => addToCardID.includes(value._id) )

 
  return (
    <>
      <Menu token={token}></Menu>
      <div className="mb-5 pt-5">
        <div style={{ backgroundColor: "white" }} className="container p-5">
          <table className="table table-hover mb-4">
            <thead>
              <tr className="bg-light">
                <th className="pl-5" scope="col ">
                  STT
                </th>
                <th scope="col">Cửa Hàng</th>
                <th scope="col">Số sản phẩm đã thêm</th>
              </tr>
            </thead>
            <tbody>
              {data.map((storeOwner, index) => {
                dem = 0;
                return (
                  <tr href={`/Giohang/${storeOwner._id}`} key={makeid(10)}>
                    <td className="pl-5" scope="row">
                      <a
                        href={`/Giohang/${storeOwner._id}`}
                        style={{ color: "#858796" }}
                      >
                        {index + 1}
                      </a>
                    </td>
                    <td>
                      <a
                        style={{ color: "#858796" }}
                        href={`/Giohang/${storeOwner._id}`}
                      >
                        <img
                          className="mr-2"
                          width="60px"
                          alt="Generic placeholder image"
                          src={storeOwner.photos}
                        />
                        {storeOwner.name}
                      </a>
                    </td>
                    <td className="pl-5">
                      <a
                        style={{ color: "#858796" }}
                        href={`/Giohang/${storeOwner._id}`}
                      >

                        {addToCardOBJ.map((value) => {
                          if (storeOwner._id === value.storeOwnerID) {
                            dem = dem + 1;
                          }
                        })}
                        {dem}
                      </a>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
      <Footer></Footer>
    </>
  );
}

export default CartOfStore;
