import useForm from "../component/Userform";
import validate from "../helpers/validate/Loginformvalidationrule";
import React, { useState } from "react";
import axios from "axios";
import { API } from "../config/ConfigENV";
import Menu from '../component/nav/Menu';
import Footer from '../component/Footer';
function ResetPassword() {
  const token =  localStorage.getItem('access_token');
  const [dataPass, setDataPass] = useState({});
  const [resultDataPass, setResultDataPass] = useState();
  const [message, setMessage] = useState();
  const { values, errors, handleChange, handleSubmit } = useForm(
    checkChangePass,
    validate
  );

  const fetchData = async () => {
    const result = await axios({
      method: "PUT",
      url: `${API}/comparePassWord`,
      data: {
        passWord: document.getElementById('password').value,
        newPassWord: document.getElementById('newPassWord').value,
        comparePassWord: document.getElementById('comparePassWord').value,


      },
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });

    if (dataPass) {
      setMessage(result.data.message);
    }

    setResultDataPass(result.data);

    if (result.data.access_token && resultDataPass.access_token) {
      localStorage.setItem('access_token', resultDataPass.access_token);
    }
 

    return result.data;
  };

  const submitNewPass = () => {
    setDataPass({
      passWord: values.password,
      newPassWord:values.newPassWord,
      comparePassWord:values.comparePassWord,
    });
    fetchData();
  };

  function checkChangePass() {}




  return (
    <>
    <Menu token={token}></Menu>
    <div className="mb-5" style={{ paddingTop: "5%" }}>
    <div className="container bg-white form">
      <div tyle={{backgroundColor:'white',boxShadow: '0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22)'}} className="row">
        <div style={{ color: "#fff" }} className="col-md-6  pt-5">
        <div className="text-center">
                            <img src="https://firebasestorage.googleapis.com/v0/b/fir-firebase-4d563.appspot.com/o/images%2Flogo.png?alt=media&token=1f57f48d-ef22-46f2-a27e-c6099a6f662a"></img>
                        </div>
        </div>
        <div style={{ color: "#fff" }} className="col-md-6 p-5 bg-white">
        <h6 className="MenuRow">THAY ĐỔI MẬT KHẨU</h6>
          <form onSubmit={handleSubmit} noValidate>
            <div className="field text-left mt-3">
              <label className="label MenuRow">Mật Khẩu</label>
              <div className="control">
                <input
                id="password"
                  className={`input ${
                    errors.password && "is-danger"
                  } w-100 form-control`}
                  type="password"
                  name="password"
                  onChange={handleChange}

                  required
                />
              </div>
              {errors.password && (
                <p className="help is-danger">{errors.password}</p>
              )}

                <p className="help is-danger">{message}</p>
            </div>
            <div className="field text-left mt-3">
              <label className="label MenuRow">Nhập mật khẩu mới</label>
              <div className="control">
                <input
                  id="newPassWord"
                  className={`input ${
                    errors.newPassWord && "is-danger"
                  } w-100 form-control`}
                  type="password"
                  name="password"
                  onChange={handleChange}

                  required
                />
              </div>
              {errors.newPassWord && (
                <p className="help is-danger">{errors.newPassWord}</p>
              )}

                <p className="help is-danger">{message}</p>
            </div>
            <div className="field text-left mt-3">
              <label className="label MenuRow">Nhập Lại Mật Khẩu</label>
              <div className="control">
                <input
                id="comparePassWord"
                  className={`input ${
                    errors.comparePassWord && "is-danger"
                  } w-100 form-control`}
                  type="password"
                  name="password"
                  onChange={handleChange}

                  required
                />
              </div>
              {errors.password && (
                <p className="help is-danger">{errors.comparePassWord}</p>
              )}

                <p className="help is-danger">{message}</p>
            </div>
            <button
              type="submit"
              className="button is-block is-info is-fullwidth btnlogin mt-3 p-2"
              onClick={submitNewPass}
            >
              Login
            </button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <Footer></Footer>
  </>
  );
}

export default ResetPassword;
