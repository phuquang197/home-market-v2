import React from 'react';
import Footer from '../../component/Footer';
import Menu from '../../component/nav/Menu';
import CardDemo2 from '../../component/user/combined/CardDemo2';
function FrozenfoodTopMarket(props) {
    const token = localStorage.getItem('access_token');
    return (
        <div>
            <Menu token={token}></Menu> 
            <div className="text-center mt-5">
            <h2 className="Text-center">Thực Phẩm Đông Lạnh</h2>     
            </div>
            <div className="container pt-4 mb-5">
                <div className="row bg-white p-5 mb-5">
                <CardDemo2></CardDemo2>
                </div>
            </div>
            <Footer></Footer>
        </div>
    );
}

export default FrozenfoodTopMarket;