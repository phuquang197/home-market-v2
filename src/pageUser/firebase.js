
import 'firebase/messaging';
import 'firebase/analytics';
import firebase from "../firebase/Firebase";

export const getBrowserToken = async () => {
  try {
    const messaging = firebase.messaging();
    // tslint:disable-next-line:no-console

      messaging.getToken({ vapidKey: 'BIXQprj8LYKJ--MW1-Ise0e0WgwzMfX4R1FUep9AANLcQudgNkaB_E2rYcOIGyZJDhm1_bAYB6ITtJVVjDgMBhQ' }).then((currentToken) => {
        if (currentToken) {
          console.log(currentToken);
          // Send the token to your server and update the UI if necessary
          // ...
        } else {
          // Show permission request UI
          console.log('No registration token available. Request permission to generate one.');
          // ...
        }
      }).catch((err) => {
        console.log('An error occurred while retrieving token. ', err);
        // ...
      });
  } catch (error) {
    return;
  }

};


export const deleteBrowserToken = async () => {
  try {
    const messaging = firebase.messaging();
    await messaging.deleteToken();
  } catch (error) {
    return;
  }
};