
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { API } from "../config/ConfigENV";
import { makeid } from '../helpers/create/create_key_index';

import NotificaUsers from '../component/NotificaUsers';
import Menu from '../component/nav/Menu';
import Footer from '../component/Footer';
function OptionOfNotifi() {
    const [notications, setNotifications] = useState();
    // let { userID } = useParams();
    const token = localStorage.getItem("access_token");
    const userID = localStorage.getItem("userID");
    const fetchData = async () => {
      if(!token){
        return null
      }

        const result = await axios({

            method: "get",
            url: `${API}/notications?limit=5&createdBy=${userID}`,
            headers: {
                Authorization: `Bearer ${token}`,
            },
        });

        return result.data
    }
    useEffect(() => {
        fetchData().then(data => {
            setNotifications(data)
        })
    }, [])

    if (!notications || notications.lengh === 0) {
        return ("")
    }

    return (
        <>
         <Menu token={token}></Menu>
       <div className="container mt-5 mb-5 bg-white">
           <h4 className="pt-2 text-center">Thông Báo</h4>
           <div className="row">
           <div className="p-3 text-notifi">

      {
          notications.list.map((notication) => {
              return <NotificaUsers key={makeid(10)} notication={notication}></NotificaUsers>

          })
      }

      </div>
           </div>
       </div>
       <Footer></Footer>
        </>
    );
}

export default OptionOfNotifi;