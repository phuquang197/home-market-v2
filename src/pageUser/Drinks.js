import React, { useEffect, useState } from 'react';
import BannerPrd from '../component/BannerPrd';
import Footer from '../component/Footer';
import Menu from '../component/nav/Menu';
import CardDemo from '../component/user/combined/CardDemo';
import axios from 'axios';
import { API } from "../config/ConfigENV";
import { makeid } from "../helpers/create/create_key_index";
import { NavLink } from 'react-router-dom';
import SlideSale from '../component/SlideSale';
function Drinks(props) {

    const token = localStorage.getItem('access_token');
    const [drinkTopmarket, setDrinkTopmarket] = useState([])
    const [drinkAnnam, setDrinkAnnam] = useState([])
    const [drinkNaman, setDrinkNaman] = useState([])
    const [dinkLottel, setDrinkLottel] = useState([])
    const [drinkChoMoi, setDrinkChoMoi] = useState([])
    useEffect(() => {
        async function getData() {
            let response = await axios.get(`${API}/products?`, {
            })
            let product = response.data.list.filter(i => i.storeOwnerID._id === '607f0f76a4521e067da79e2c' && i.tag === 'DRINKS')
            setDrinkTopmarket(product)

        }
        getData()
    }, [])
    useEffect(() => {
        async function getData() {
            let response = await axios.get(`${API}/products?`, {
            })
            let product = response.data.list.filter(i => i.storeOwnerID._id === '607f109fa4521e067da79e32' && i.tag === 'DRINKS')
            setDrinkAnnam(product)

        }
        getData()
    }, [])
    useEffect(() => {
        async function getData() {
            let response = await axios.get(`${API}/products?`, {
            })
            let product = response.data.list.filter(i => i.storeOwnerID._id === '607f111da4521e067da79e38' && i.tag === 'DRINKS')
            setDrinkNaman(product)

        }
        getData()
    }, [])
    useEffect(() => {
        async function getData() {
            let response = await axios.get(`${API}/products?`, {
            })
            let product = response.data.list.filter(i => i.storeOwnerID._id === '60a66b1b756e5f003b250247' && i.tag === 'DRINKS')
            setDrinkLottel(product)

        }
        getData()
    }, [])
    useEffect(() => {
        async function getData() {
            let response = await axios.get(`${API}/products?`, {
            })
            let product = response.data.list.filter(i => i.storeOwnerID._id === '607f1183a4521e067da79e3a' && i.tag === 'DRINKS')
            setDrinkChoMoi(product)

        }
        getData()
    }, [])
    return (
        <>
            <Menu token={token}></Menu>
            <div className="mb-5 ">
                <div className="container ">
                    <div className="row">
                        <div className="col-md-3 bg-white">
                            <BannerPrd></BannerPrd>
                        </div>
                        <div className="col-md-9">
                            <SlideSale></SlideSale>

                            <div>
                                <NavLink to="InformationStore/60802578b18f8b003bfc69f0"><h3 className="pt-4"> TOP MARKET </h3></NavLink>
                            </div>
                            <div className="container bg-white mb-3">
                                <div className="cardDemo">
                                    <div className="row mt-3 p-5">
                                        {
                                            drinkTopmarket.map((drinkTopmarket) => {
                                                return <CardDemo key={makeid(10)} product={drinkTopmarket}></CardDemo>
                                            })
                                        }
                                    </div>
                                </div>
                                <div className="text-center">
                                    <button type="button" className="btn btn-outline-success m-4">Xem thêm</button>
                                </div>
                            </div>

                            <div>
                                <NavLink to="InformationStore/607f111da4521e067da79e38"><h3 className="pt-4"> AN NAM GOURMET </h3></NavLink>
                            </div>
                            <div className="container bg-white mb-3 ">
                                <div className="cardDemo">
                                    <div className="row mt-3 p-5">
                                        {
                                            drinkAnnam.map((drinkAnnam) => {
                                                return <CardDemo key={makeid(10)} product={drinkAnnam}></CardDemo>
                                            })
                                        }
                                    </div>
                                </div>
                                <div className="text-center">
                                    <button type="button" className="btn btn-outline-success m-4">Xem thêm</button>
                                </div>
                            </div>

                            <div>
                                <NavLink to="InformationStore/607f111da4521e067da79e38"><h3 className="pt-4"> AN NAM GOURMET </h3></NavLink>
                            </div>
                            <div className="container bg-white mb-3 ">
                                <h3 className="pt-4"> Nam An Market </h3>

                                <div className="cardDemo">
                                    <div className="row mt-3 p-5">
                                        {
                                            drinkNaman.map((drinkNaman) => {
                                                return <CardDemo key={makeid(10)} product={drinkNaman}></CardDemo>
                                            })
                                        }
                                    </div>
                                </div>
                                <div className="text-center">
                                    <button type="button" className="btn btn-outline-success m-4">Xem thêm</button>
                                </div>
                            </div>
                            <div>
                                <NavLink to="InformationStore/607f111da4521e067da79e38"><h3 className="pt-4"> LOTTE MART </h3></NavLink>
                            </div>
                            <div className="container bg-white mb-3 ">

                                <div className="cardDemo">
                                    <div className="row mt-3 p-5">
                                        {
                                            dinkLottel.map((dinkLottel) => {
                                                return <CardDemo key={makeid(10)} product={dinkLottel}></CardDemo>
                                            })
                                        }
                                    </div>
                                </div>
                                <div className="text-center">
                                    <button type="button" className="btn btn-outline-success m-4">Xem thêm</button>
                                </div>
                            </div>
                            <div>
                                <NavLink to="InformationStore/607f111da4521e067da79e38"><h3 className="pt-4"> CHỢ PHỐ FRESH FOOD </h3></NavLink>
                            </div>
                            <div className="container bg-white mb-3 ">
                                <div className="cardDemo">
                                    <div className="row mt-3 p-5">
                                        {
                                            drinkChoMoi.map((drinkChoMoi) => {
                                                return <CardDemo key={makeid(10)} product={drinkChoMoi}></CardDemo>
                                            })
                                        }
                                    </div>
                                </div>
                                <div className="text-center">
                                    <button type="button" className="btn btn-outline-success m-4">Xem thêm</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Footer></Footer>
        </>
    );
}

export default Drinks;