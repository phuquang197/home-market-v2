import React, { useEffect, useState } from 'react';
import CardDemo from '../component/user/combined/CardDemo';
import axios from 'axios';
import { makeid } from "../helpers/create/create_key_index";
import { API } from '../config/ConfigENV';
function ProductLike() {
    const [product, setProduct] = useState();
    const token = localStorage.getItem("access_token");

    const fetchData = async () => {
        const result = await axios({
            method: "put",
            url: `${API}/likeAndUnLike/{productID}`,
            headers: {
                Authorization: `Bearer ${token}`,
            },
        });

        return result.data
    }

    useEffect(() => {
        fetchData().then(data => {
            setProduct(data)
        })
    }, [])

    if (!product || product.lengh === 0) {
        return ("")
    }
   
    return (

        <div>
            <div className="container ">
                <div className="cardDemo">
                    <div className="row mt-3 pl-3">
                        {
                            product.map((product) => {
                                return <CardDemo key={makeid(10)} product={product}></CardDemo>
                            })
                        }
                    </div>
                </div>
                <div className="text-center m-4">
                    <button type="button" className="btn btn-outline-success">Xem thêm</button>
                </div>
            </div>

        </div>
    );
}

export default ProductLike;