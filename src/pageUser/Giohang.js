import React, { useEffect, useState } from "react";
import Menu from "../component/nav/Menu";
import Footer from "../component/Footer";
import { useParams } from "react-router-dom";
import { makeid } from "../helpers/create/create_key_index";
import Modal_delete from "./modal/Modal_delete";
import Modal_edit from "./modal/Modal_edit";
import { getVND } from "../helpers/getVND";
import axios from "axios";
import { API } from "../config/ConfigENV";
import Loadpage from "../component/Loadpage";

function Giohang() {
  const token = localStorage.getItem("access_token");
  const addToCard = localStorage.getItem("addToCard");
  const [payURL, setPayURL] = useState("");
  const [message, setMessage] = useState("");
  const [storeOwner, setStoreOwner] = useState();
  const { storeOwnerID } = useParams();

  const addToCardOBJ = JSON.parse(addToCard);

  let newAddToCardOBJ = addToCardOBJ.filter((value) =>
    value.storeOwnerID === storeOwnerID ? storeOwnerID : ""
  );

  let totalMoney = 0;
  newAddToCardOBJ.forEach((element) => {
    totalMoney =
      totalMoney + parseInt(element.amount) * parseInt(element.price);
  });

  const pageLoad = async()=> {
    const result = await axios({
      method: "GET",
      url: `${API}/storeOwners/${storeOwnerID}`,
    });
    return result
  }
  const editOneModalGiaoHang = async () => {
    let address = document.getElementById("address").value;
    let phoneNumber = document.getElementById("phoneNumber").value;

    document.getElementById("noiGiaoHang").value = address;
    document.getElementById("DiachiNhanHangg").innerHTML = address;
    document.getElementById("sdtLienHe").value = phoneNumber;
    document.getElementById("sdtLienHe2").innerHTML = phoneNumber;

    document.getElementById("lableDiaChiNhanHang").innerHTML = 'Địa chỉ nhận hàng :';
    document.getElementById("lableSoDienThoaiLienHe").innerHTML = 'Số điện thoại liên hệ :';
    document.getElementById("hinhThucGiaoHang").innerHTML = 'Giao Hàng';
  };

  const layHangTaiCuaHangHandle = async () => {

    document.getElementById("noiGiaoHang").value = 'null';
    document.getElementById("DiachiNhanHangg").innerHTML = '';
    document.getElementById("sdtLienHe").value = 'null';
    document.getElementById("sdtLienHe2").innerHTML = '';
    document.getElementById("NoiNhanHangg").innerHTML = storeOwner.address;

    document.getElementById("lableNoiNhanHang").innerHTML = 'Địa chỉ nhận hàng :';
    document.getElementById("lableDiaChiNhanHang").innerHTML = '';
    document.getElementById("lableSoDienThoaiLienHe").innerHTML = '';
    document.getElementById("hinhThucGiaoHang").innerHTML = 'Lấy hàng tại cửa hàng';
  };


  const payMent = async () => {
    let tt_truc_tiep = document.getElementById("tt_truc_tiep").checked;
    let tt_khi_nhan_hang = document.getElementById("tt_khi_nhan_hang").checked;

    let deliveryAddress = document.getElementById("noiGiaoHang").value;

    let deliveryPhoneNumber = document.getElementById("sdtLienHe").checked;

    const giaoHangStatus = deliveryAddress === 'null' ? 'TU_LAY_HANG' : 'GIAO_HANG'
    const paymentStatus = tt_truc_tiep ? "PENDING" : "DIRECT_PAYMENT";
    let productIDs = newAddToCardOBJ.map((element) => {
      return {
        productID: element.productID,
        amount: parseInt(element.amount),
        name: element.name,
        photo: element.photo,
      };
    });
    let productIDsPayment= productIDs.map(value => value.productID)

    const dataPayment = {
      productIDs,
      userCreatedProductID: newAddToCardOBJ[0].userCreatedProductID,
      totalMoney,
      paymentStatus,
      deliveryAddress,
      deliveryPhoneNumber,
      giaoHangStatus,
    };

    if (paymentStatus === 'PENDING') {
      if (!token) {
        setMessage('Bạn cần đăng nhập để thanh toán số sản phẩm này!');
      }
      const result = await axios({
        method: "POST",
        url: `${API}/payment`,
        data: dataPayment,
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });

      if (result.data.message === "ok") {
        setPayURL(result.data.payment.payUrl);
      }

      if (result.data.message !== "ok") {
        setMessage(result.data.message);
      }
    }


    if (paymentStatus === 'DIRECT_PAYMENT') {

      if (!token) {
        setMessage('Bạn cần đăng nhập để đặt hàng những sản phẩm này!');
      }

      const result = await axios({
        method: "POST",
        url: `${API}/payment/direct_payment`,
        data: dataPayment,
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });

      if (result.data.message === "ok") {

        const newAddToCardOBJData =  addToCardOBJ.filter(value => !productIDsPayment.includes(value.productID)? value :'')
        localStorage.setItem("addToCard", JSON.stringify(newAddToCardOBJData));
        alert('Đặt hàng thành công')
        setPayURL('http://localhost:3000/CartOfStore');
      }

      if (result.data.message !== "ok") {
        setMessage(result.data.message);
      }
    }

  };

  useEffect(() => {
    console.log(payURL);
  }, [payURL]);

  useEffect(() => {
    pageLoad().then(data => {
      setStoreOwner(data.data)
    })
  }, [ ])

if(!storeOwner || storeOwner.lengh === 0 ){
  return <Loadpage></Loadpage>
}

 
  if (payURL) {
    window.location.replace(payURL);
  }
  return (
    <>
      <Menu token={token}></Menu>
      <div className="mb-5 pt-5">
        <div style={{ backgroundColor: "white" }} className="container p-5">
          {message ? (
            <div className="alert alert-danger" role="alert">
              {message}
            </div>
          ) : (
            ""
          )}
          <table className="table table-hover mb-4">
            <thead>
              <tr className="bg-light">
                <th className="pl-5" scope="col ">
                  Sản Phẩm
                </th>
                <th scope="col">Số Lượng</th>
                <th scope="col">Gía Thành</th>
                <th scope="col">Thành Tiền</th>
                <th scope="col">Chỉnh Sửa</th>
              </tr>
            </thead>
            <tbody>
              {newAddToCardOBJ.map((value, index) => {
                return (
                  <tr key={makeid(10)}>
                    <th className="media" scope="row">
                      <img
                        className="mr-4"
                        width="60px"
                        alt="Generic placeholder image"
                        src={value.photo}
                      />
                      {value.name}
                    </th>
                    <td>{value.amount}</td>
                    <td>{getVND(value.price, "VNĐ")}</td>
                    <td>
                      {getVND(parseInt(value.amount) * value.price, "VNĐ")}
                    </td>

                    <td>
                      <button
                        type="button"
                        className="btn btn-danger"
                        aria-hidden="true"
                        data-toggle="modal"
                        data-target="#exampleModal"
                        onClick={() =>
                        (document.getElementById("dataProductID").value =
                          value.productID)
                        }
                      >
                        <i className="fa fa-trash" aria-hidden="true"></i>
                      </button>
                      <button
                        type="button"
                        className="btn btn-danger ml-2"
                        aria-hidden="true"
                        data-toggle="modal"
                        data-target="#exampleModal232"
                        onClick={() =>
                        (document.getElementById("dataProductID").value =
                          value.productID)
                        }
                      >
                        <i className="fa fa-pencil" aria-hidden="true"></i>
                      </button>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
          <div className="text-right pb-4">
            <br></br>
            <h6 style={{ display: "inline-block" }} className="phigh mr-3">
              Phí Giao Hàng :
            </h6>
            <h6 style={{ display: "inline-block" }} className="phigh">
              0
            </h6>{" "}
            <br></br>
            <h6 style={{ display: "inline-block" }} className="kmai mr-3">
              Khuyến Mãi:
            </h6>
            <h6 style={{ display: "inline-block" }} className="kmmai">
              0
            </h6>
            <br></br>
            <h6 style={{ display: "inline-block" }} className="sumSP mr-3">
              Tổng Cộng :
            </h6>
            <h6 style={{ display: "inline-block" }} className="sumSP">
              {getVND(totalMoney, "VNĐ")}
            </h6>
            <br></br>
            <h6 style={{ display: "inline-block" }} className="sumSP"    >
              Hình thức nhận hàng :
            </h6>

            <h6 style={{ display: "inline-block" }} className="sumSP" id='hinhThucGiaoHang' >
              Lấy hàng tại cửa hàng
            </h6>
            <br></br>
            <h6 style={{ display: "inline-block" }} className="sumSP mr-3" id='lableDiaChiNhanHang'>
            </h6>
            <h6 style={{ display: "inline-block" }} className="sumSP" id="DiachiNhanHangg">
            </h6>
            <h6 style={{ display: "inline-block" }} className="sumSP mr-3" id='lableNoiNhanHang'>
              Địa chỉ nhận hàng:
            </h6>
            <h6 style={{ display: "inline-block" }} className="sumSP" id="NoiNhanHangg">
              {storeOwner.address}
            </h6>
            <br></br>
            <h6 style={{ display: "inline-block" }} className="sumSP mr-3" id='lableSoDienThoaiLienHe'>

            </h6>
            <h6 style={{ display: "inline-block" }} className="sumSP" id="sdtLienHe2">

            </h6>
            <br></br>
            {/* <hr></hr> */}
            <div
              style={{ display: "inline-block" }}
              className="btn-group btn-group-toggle"
              data-toggle="buttons"
            ></div>
            <div style={{ display: "inline-block" }}>
              <div
                style={{ display: "inline-block" }}
                className="form-check mr-3"
              >
                <div className="form-check">
                  <input
                    className="form-check-input mt-3"
                    type="radio"
                    name="flexRadioDefault"
                    id="tt_truc_tiep"
                    defaultChecked={false}
                  />
                  <label className="form-check-label" htmlFor="tt_truc_tiep">
                    Thanh Toán qua MoMo{" "}
                    <img
                      width="70px"
                      src="https://www.vaytaichinh.vn/wp-content/uploads/2020/05/nen-su-dung-vi-dien-tu-nao-vi-dien-tu-momo.jpg"
                    />
                  </label>
                </div>
              </div>
              <div style={{ display: "inline-block" }} className="form-check">
                <div className="form-check">
                  <input
                    className="form-check-input"
                    type="radio"
                    name="flexRadioDefault"
                    id="tt_khi_nhan_hang"
                    defaultChecked={true}
                  />
                  <label
                    className="form-check-label"
                    htmlFor="tt_khi_nhan_hang"
                  >
                    Thanh Toán Khi Nhận Hàng
                  </label>
                </div>
              </div>
            </div>
            {/* <hr></hr> */}
            <input
              id="noiGiaoHang"
              defaultValue="null"
              style={{ display: "none" }}
            ></input>
            <input
              id="sdtLienHe"
              defaultValue="null"
              style={{ display: "none" }}
            ></input>
            <div className="mb-3">
              <button
                type="button"
                className="btn btn-primary"
                aria-hidden="true"
                data-toggle="modal"
                data-target="#exampleModalGiaoHang"
              >
                Giao hàng đến vị trí hiện tại của bạn
                      </button>
              <button type="button" className="btn btn-danger ml-2" onClick={layHangTaiCuaHangHandle}>Lấy hàng tại cửa hàng </button>

            </div>


            {/* <div className="text-center mt-3">
              <button type="button" className="btn btn-danger " onClick={layHangTaiCuaHangHandle}>Lấy hàng tại cửa hàng </button>
            </div> */}
            {/* <hr></hr> */}
            <a className="btnlogin p-2 mt-4" role="button" onClick={payMent}>
              Đặt Hàng
            </a>
          </div>
        </div>
      </div>


      {/* modal giao hang */}
      <div
        className="modal fade"
        id="exampleModalGiaoHang"
        tabIndex={-1}
        role="dialog"
        aria-labelledby="exampleModalLabel232"
        aria-hidden="true"
      >
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLabel232">
                Nơi giao hàng
            </h5>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div className="modal-body">
              {/* style={{display: "none"}} */}
              <input
                id="dataProductID"
                defaultValue="null"
                style={{ display: "none" }}
              ></input>
              <strong>
                Nhập địa chỉ bạn muốn giao hàng
            </strong>
              <input style={{ float: 'right' }}
                id="address"
                defaultValue="null"
              ></input>
              <br></br>
              <span>
                <strong style={{ top: '25px', position: 'relative' }} >
                  Số điện thoại liên hệ
            </strong>
                <input style={{ float: 'right' }}
                  id="phoneNumber"
                  defaultValue="null"
                ></input>

              </span>

            </div>
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-secondary"
                data-dismiss="modal"
              >
                Không, Quay lại!
            </button>
              <button
                type="button"
                className="btn btn-primary"
                data-dismiss="modal"
                onClick={() => editOneModalGiaoHang()}
              >
                Xác nhận
            </button>
            </div>
          </div>
        </div>
      </div>
      <Modal_edit></Modal_edit>
      <Modal_delete></Modal_delete>
      <Footer></Footer>
    </>
  );
}

export default Giohang;