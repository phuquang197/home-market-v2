import useForm from "../component/Userform";
import validate from "../helpers/validate/Loginformvalidationrule";
import React, { useEffect, useState } from "react";
import axios from "axios";
import { API } from "../config/ConfigENV";

function LoginAdm() {
  const token = localStorage.getItem("access-token");
  const [token2, setToken2] = useState();
  // const [dataLogin, setDataLogin] = useState();
  const [message, setMessage] = useState();
  const { values, errors, handleChange, handleSubmit } = useForm(
    checkLogin,
    validate
  );



  const submitLogin = async () => {
    const result = await axios({
      method: "post",
      url: `${API}/login`,
      data: {
      userName: values.userName,
      passWord: values.password,
      },
    });
    if(result.data.message){
      setMessage(result.data.message);
    }
    else{
      localStorage.setItem('access-token' ,result.data.access_token )
      window.location.href = '/indexAdm'
    }
    


  };
  function checkLogin() {
    if (token) {
      window.location.href = '/indexAdm';
      // alert("dn thành công")
    }
  }

  // useEffect(() => {
  //   fetchData();
  // }, [dataLogin, message]);



  if (token2) {
    window.location.reload("");
  }
  return (
    <div className="container form p-5">
      <div className="row bg-white p-5">
      <div
                    style={{  color: "#08c25e" }}
                    className="col-md-6  "
                  >
                  <img className="w-100" src="https://firebasestorage.googleapis.com/v0/b/fir-firebase-4d563.appspot.com/o/images%2Flogo.png?alt=media&token=1f57f48d-ef22-46f2-a27e-c6099a6f662a"></img>
                  </div>
        <div className="col-md-6 mt-5 mb-5">
          <form onSubmit={handleSubmit} noValidate>
            <div className="field text-left">
              <label className="label">User name</label>
              <div className="control">
                <input
                  autoComplete="off"
                  className={`input ${
                    errors.userName && "is-danger"
                  } w-100 form-control`}
                  type="userName"
                  name="userName"
                  onChange={handleChange}
                  value={values.userName || ""}
                  required
                />
                {errors.userName && (
                  <p className="help is-danger">{errors.userName}</p>
                )}
                <p className="help is-danger">{message}</p>
              </div>
            </div>
            <div className="field text-left mt-3">
              <label className="label">Password</label>
              <div className="control">
                <input
                  className={`input ${
                    errors.password && "is-danger"
                  } w-100 form-control`}
                  type="text"
                  name="password"
                  onChange={handleChange}
                  value={values.password || ""}
                  required
                />
              </div>
              {errors.password && (
                <p className="help is-danger">{errors.password}</p>
              )}
            </div>
            <button
              type="submit"
              className="button is-block is-info is-fullwidth btnlogin mt-3 p-2"
              onClick={submitLogin}
            >Đăng Nhập
            </button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default LoginAdm;

