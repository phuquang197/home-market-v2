import React from 'react';
import { NavLink } from 'react-router-dom';
import './Admin.css';
function MenuLeftAdmin() {
    return (
        <div className="bg-gradient-primary ">
            <ul className="navbar-nav sidebar sidebar-dark accordion" id="accordionSidebar">
                {/* Sidebar - Brand */}
                <a className="sidebar-brand d-flex align-items-center justify-content-center" href="/#">
                    <div className="sidebar-brand-icon rotate-n-15">
                        <i className="far fa-laugh-wink" />

                    </div>
                    <div className="sidebar-brand-text mx-3">HM</div>
                </a>
                {/* Divider */}
                <hr className="sidebar-divider my-0" />
                {/* Nav Item - Dashboard */}
                <li className="nav-item">           
                    <NavLink className="nav-link" to="/indexAdm">
                        <i className="fa fa-tasks"></i>
                        <span>Thống Kê</span></NavLink>
                </li>
                <li className="nav-item ">
                    <NavLink className="nav-link" to="/Manageaccount">
                        <i className="fa fa-user"></i>
                        <span>Xem Người Dùng</span></NavLink>
                </li>
                <li className="nav-item ">
                    <NavLink className="nav-link" to="/ManageStore">
                        <i className="fa fa-user"></i>
                        <span>Xem Cửa Hàng</span></NavLink>
                </li>
                <li className="nav-item">
                    <NavLink className="nav-link" to="/StoreApprove">
                        <i className="fa fa-user"></i>
                        <span>Xem Cửa Hàng Đăng Kí</span></NavLink>
                </li>
                <li className="nav-item">
                    <NavLink className="nav-link" to="/Managepost">
                        <i className="fa fa-fw fa-book" />
                        <span>Quản Lí Bài Đăng</span></NavLink>
                </li>
                <li className="nav-item">
                    <NavLink className="nav-link" to="/ManagePayment">
                        <i className="fa fa-fw fa-database" />
                        <span>Payment</span></NavLink>
                </li>
                {/* Divider */}
                
            </ul>

        </div>
    );
}

export default MenuLeftAdmin;