import React from 'react';
// import { NavLink } from 'react-router-dom';
import './Admin.css';
import axios from "axios";
import { API } from "../config/ConfigENV";
function MenuAdm(props) {
    const tokenadm = localStorage.getItem('access-token');
    // const token_fcm = localStorage.getItem('token_fcm');

    const logOut = async () => {
        const result1 = await axios({
            method: "post",
            url: `${API}/fcm/unsubscribe`,
            data: {
                token: tokenadm,
            },
            headers: {
                Authorization: `Bearer ${tokenadm}`,
            },
        });

        localStorage.removeItem("access-token");
        localStorage.removeItem("userID");
        localStorage.removeItem("storeOwnerID");
        localStorage.removeItem("nameStore");
        localStorage.removeItem("addToCard");
        localStorage.removeItem("name");

        window.location.href = '/loginadm'
    }
    return (
        <div>
            <nav className="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow ">
                {/* Sidebar Toggle (Topbar) */}
                <button id="sidebarToggleTop " className="btn btn-link d-md-none rounded-circle mr-3 ">
                    <i className="fa fa-bars " />
                </button>

                {/* Topbar Navbar */}
                <ul className="navbar-nav ml-auto ">
                    {/* Nav Item - Search Dropdown (Visible Only XS) */}
                    <li className="nav-item dropdown no-arrow d-sm-none ">
                        <a className="nav-link dropdown-toggle " href="/#" id="searchDropdown " role="button " data-toggle="dropdown " aria-haspopup="true " aria-expanded="false ">
                            <i className="fa fa-search fa-fw " />
                        </a>
                        {/* Dropdown - Messages */}
                        <div className="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in " aria-labelledby="searchDropdown ">
                            <form className="form-inline mr-auto w-100 navbar-search ">
                                <div className="input-group ">
                                    <input type="text " className="form-control bg-light border-0 small " placeholder="Search for... " aria-label="Search " aria-describedby="basic-addon2 " />
                                    <div className="input-group-append ">
                                        <button className="btn btn-primary " type="button ">
                                            <i className="fa fa-search fa-sm " />
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </li>
                    {/* Nav Item - Alerts */}
                    {/* <li className="nav-item dropdown no-arrow mx-1 ">
                        <a className="nav-link dropdown-toggle " id="alertsDropdown " role="button " data-toggle="dropdown " aria-haspopup="true " aria-expanded="false ">

                            <i className="fa fa-bell" aria-hidden="true"></i>
                            <span className="badge badge-danger badge-counter ">3+</span>
                        </a>
                        <div className="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in " aria-labelledby="alertsDropdown ">
                            
                            <a className="dropdown-item d-flex align-items-center " href="/#">
                                <div className="mr-3 ">
                                    <div className="icon-circle bg-primary ">
                                        <i className="fa fa-file-alt    "></i>

                                    </div>
                                </div>
                                
                            </a>
                            
                        </div>
                    </li> */}

                   
                    {/* Nav Item - Messages */}
                    <div className="topbar-divider d-none d-sm-block " />
                    {/* Nav Item - User Information */}
                    <li className="nav-item  dropdown-menu dropdown-menu-right">
                        {/* <NavLink className="dropdown-item  MenuRow" to="/#">
                               Đăng Nhập
                            </NavLink> */}
                        {/* Dropdown - User Information */}

                    </li>
                    <span className="navbar-text pr-3">
                        <div className="dropdown open">
                            <button
                                className="btn dropdown-toggle"
                                type="button"
                                id="triggerId"
                                data-toggle="dropdown"
                                aria-haspopup="true"
                                aria-expanded="false"
                            >
                                <img
                                    className="mr-4 avt"
                                    width="50px"
                                    alt="Generic placeholder image"
                                    src="https://png.pngtree.com/png-vector/20190625/ourlarge/pngtree-business-male-user-avatar-vector-png-image_1511454.jpg"
                                />
                            </button>
                            <div
                                className="dropdown-menu dropdown-menu-right p-3"
                                aria-labelledby="triggerId"
                            >


                                <button onClick={logOut} className="dropdown-item  MenuRow" >
                                    Đăng xuất
                                </button>
                            </div>
                        </div>
                    </span>
                </ul>

            </nav>

        </div>

    );
}

export default MenuAdm;