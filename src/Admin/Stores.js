import React from 'react';
import { NavLink } from 'react-router-dom';

function Stores(props) {
    const { storeOwner } = props;
    return (

                                            <tr role="row" className="odd">
                                                <td>{storeOwner.name}</td>
                                                <td>{storeOwner.email}</td>
                                                <td>{storeOwner.phoneNumbers}</td>
                                                <td>{storeOwner.address}</td>
                                                
                                                <td>
                                                    <NavLink to={`/InforStores/${storeOwner._id}`} className="">
                                                       Xem Thông Tin
                                                    </NavLink>
                                                </td>
                                            </tr>


    
    );
}

export default Stores;
