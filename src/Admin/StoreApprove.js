import React from 'react';
import MenuAdm from './MenuAdm';
import MenuLeftAdmin from './MenuLeftAdmin';
import StoreRegister from './StoreRegister';
function StoreApprove() {
    
    return (
        <div id="wrapper">
            <MenuLeftAdmin></MenuLeftAdmin>
            <div id="content-wrapper" className="d-flex flex-column">
                <MenuAdm></MenuAdm>
                <br></br>
                <div className="text-center mb-3"><h3>Thông Tin Cửa Hàng Đăng Kí</h3></div>
                <div className="p-5 bg-white">
                <StoreRegister></StoreRegister>
                </div>
            </div>
           
        </div>
    );
}

export default StoreApprove;