import './Admin.css';
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { API } from "../config/ConfigENV";


function TotalProduct() {
    const [product, setProduct] = useState();
    const fetchData = async () => {
        const result = await axios.get(`${API}/products`, {
        })

        return result.data
    }

    useEffect(() => {
        fetchData().then(data => {
            setProduct(data)
            localStorage.setItem('totalProduct' , data.totalCount)
        })
    }, [])

    if (!product || product.lengh === 0) {
        return ("")
    }
    return (
        <div>
            {product.totalCount}
        </div>
    );
}

export default TotalProduct;