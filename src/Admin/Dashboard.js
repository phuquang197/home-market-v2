import React from 'react';
import './Admin.css';
import { Bar, Doughnut } from "react-chartjs-2";
import TotalProduct from './TotalProduct';
import TotalStore from './TotalStore';
import TotalUser from './TotalUser';
import { NavLink } from 'react-router-dom';
function Dashboard() {
    const totalProdcuct =localStorage.getItem('totalProduct');
    const totalStore =localStorage.getItem('totalStore');
    const totalUser =localStorage.getItem('totalUser');
    return (
        <div>

            <div className="container-fluid">
                {/* Page Heading */}

                {/* Content Row */}
                <div className="row mt-2">
                    {/* Volunteer Number */}
                    <div className="col-xl-3 col-md-6 mb-4">
                        <div className="card border-left-primary shadow h-100 py-2">
                            <div className="card-body m-4">
                                <div className="row no-gutters align-items-center">
                                    <div className="col mr-2">
                                        <div className="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Số Sản Phẩm</div>
                                        <div className="h5 mb-0 font-weight-bold text-gray-800"><TotalProduct></TotalProduct> </div>
                                    </div>
                                    <div className="col-auto">
                                        <i className="fa fa-user fa-2x text-gray-300" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* NPO Number */}
                    <div className="col-xl-3 col-md-6 mb-4">
                        <div className="card border-left-success shadow h-100 py-2">
                            <div className="card-body m-4">
                                <div className="row no-gutters align-items-center">
                                    <NavLink to="/Manageaccount" className="col mr-2">
                                        <div className="text-xs font-weight-bold text-success text-uppercase mb-1">
                                            Số NGƯỜI DÙNG</div>
                                        <div className="h5 mb-0 font-weight-bold text-gray-800"><TotalUser></TotalUser> </div>
                                    </NavLink>
                                    <div className="col-auto">
                                        <i className="fa fa-users fa-2x text-gray-300" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* Volunteer Activities Number */}
                    <div className="col-xl-3 col-md-6 mb-4">
                        <div className="card border-left-success shadow h-100 py-2">
                            <div className="card-body m-4">
                                <div className="row no-gutters align-items-center">
                                    <NavLink to="/ManageStore" className="col mr-2">
                                        <div className="text-xs font-weight-bold text-success text-uppercase mb-1">
                                            SỐ CỬA HÀNG LIÊN KẾT</div>
                                        <div className="h5 mb-0 font-weight-bold text-gray-800"><TotalStore></TotalStore> </div>
                                    </NavLink>
                                    <div className="col-auto">
                                        <i className="fa fa-tasks fa-2x text-gray-300" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* activities need funding Number */}
                    {/* <div className="col-xl-3 col-md-6 mb-4">
                        <div className="card border-left-warning shadow h-100 py-2">
                            <div className="card-body m-4">
                                <div className="row no-gutters align-items-center">
                                    <div className="col mr-2">
                                        <div className="text-xs font-weight-bold text-warning text-uppercase mb-1">
                                            activities need funding</div>
                                        <div className="h5 mb-0 font-weight-bold text-gray-800">18</div>
                                    </div>
                                    <div className="col-auto">
                                        <i className="fa fa-credit-card fa-2x text-gray-300" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> */}
                </div>
                {/* Content Row */}
                <div className="row">
                    {/* Area Chart */}
                    <div className="col-xl-8 col-lg-7">
                        <div className="card shadow mb-4">
                            {/* Card Header - Dropdown */}
                            <div className="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                <h6 className="m-0 font-weight-bold text-primary">Tổng Quan Hệ Thống</h6>

                            </div>
                            {/* Card Body */}
                            <div className="card-body">
                                <Bar
                                    data={{
                                        labels: [
                                            "Số Khách Hàng",
                                            "Số Cửa Hàng",
                                            "Số Sản Phẩm",
                                            // "Latin America",
                                            // "North America"
                                        ],
                                        datasets: [
                                            {
                                                label: "Số Khách Hàng",
                                               
                                                backgroundColor: [
                                                    "#3e95cd",
                                                    "#8e5ea2",
                                                    "#3cba9f",
                                                    // "#e8c3b9",
                                                    // "#c45850"
                                                ],
                                                data: [ totalUser,
                                                    totalStore, 
                                                    totalProdcuct ]
                                            }
                                        ]
                                    }}
                                    options={{
                                        legend: { display: false },
                                        title: {
                                            display: true,
                                            text: ""
                                        }
                                    }}
                                />

                            </div>
                        </div>
                    </div>
                    {/* Pie Chart */}
                    <div className="col-xl-4 col-lg-5">
                        <div className="card shadow mb-4">
                            {/* Card Header - Dropdown */}
                            <div className="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                <h6 className="m-0 font-weight-bold text-primary">Hoạch Định Tương Lai</h6>
                                
                            </div>
                            {/* Card Body */}
                            <div className="card-body">
                                <Doughnut
                                    data={{
                                        labels: [
                                            "Cửa Hàng",
                                            "Người Dùng",
                                            // "Europe",
                                            // "Latin America",
                                            // "North America"
                                        ],
                                        datasets: [
                                            {
                                                label: "Population (millions)",
                                                backgroundColor: [
                                                    "#3e95cd",
                                                    "#8e5ea2",
                                                    // "#3cba9f",
                                                    // "#e8c3b9",
                                                    // "#c45850"
                                                ],
                                                data: [100, 1000]
                                            }
                                        ]
                                    }}
                                    option={{
                                        title: {
                                            display: true,
                                            text: "Predicted world population (millions) in 2050"
                                        }
                                    }}
                                />


                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    );
}

export default Dashboard;