import MenuAdm from './MenuAdm'
import Footer from "../component/Footer";
import React, { useEffect, useState } from "react";
import axios from "axios";
import Loadpage from "../component/Loadpage";
import { API } from "../config/ConfigENV";
import { useParams } from "react-router-dom";
import MenuLeftAdmin from './MenuLeftAdmin';
import NumberFormat from 'react-number-format';
import { NavLink } from 'react-bootstrap';


function InforProduct() {
  const token = localStorage.getItem("access_token");
  let { productID } = useParams();

  const [product, setProduct] = useState();

  const fetchData = async () => {
    const result = await axios({
      method: "get",
      url: `${API}/products/${productID}`,
      // const result = await axios.get(`${API}/`, 
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });

    return result.data;
  };

  useEffect(() => {
    fetchData().then((data) => {
      setProduct(data);
    });
  }, []);

  async function Lock(){
    let response = await axios.put(`${API}/products/${productID}` ,{
          name : product.name ,
          photos : product.photos,
          email: product.email,
          description:product.description,
          price:product.price,
          nextWeight:product.nextWeight,
          percentDiscount:product.percentDiscount,
          status : 'OUT_OF_STOCK'
    },
    {
      headers:{
          Authorization: `Bearer ${token}`,
        
      }
    }
    )
    // await alert('Đã Khóa Cửa Hàng');
    window.location.href = '/Managepost';

  }
  async function Unlock(){
    let response = await axios.put(`${API}/products/${productID}` ,{
          name : product.name ,
          photos : product.photos,
          email: product.email,
          description:product.description,
          phoneNumbers:product.phoneNumbers,
          address:product.address,
          status : 'STOCKING'
    },
    {
      headers:{
          Authorization: `Bearer ${token}`,
        
      }
    }
    )
    await alert('Đã Mở Khóa');
    // await window.location.reload();
    window.location.href = '/Managepost'

  }

  if (!product || product.lengh === 0) {
    return <Loadpage></Loadpage>;
  }


  return (
    <>

      <div id="wrapper">
        <MenuLeftAdmin></MenuLeftAdmin>
        <div id="content-wrapper" className="d-flex flex-column">
          <MenuAdm></MenuAdm>
          <br></br>
          <div className="container">
            <div className="row text-left bg-white p-5">
              <div className="col-md-6">
                <img className="w-100 d-block " src={product.photos}></img>
              </div>
              <div className="col-md-6 bg-white p-5 text-left">
                Tên Sản Phẩm: {product.name}<br></br>
           Mô Tả: {product.description}<br></br>
           Gía:            <NumberFormat value={product.price} displayType={'text'} thousandSeparator={true} />VNĐ
<br></br>
           Trọng Lượng: {product.nextWeight}<br></br>
           Giảm Gía: {product.percentDiscount}%
           <div className="text-center">

                  {/* <button onClick={Lock} type="button" className="btn btn-primary btn-icon-split mr-2">
                  <span className="icon text-white-50">
                    <i className="fa fa-lock"></i>
                  </span>
                  <span className="text">Lock</span>
                </button> */}
                  <div>
                    {/* Button trigger modal */}
                    <button type="button" className="btn btn-primary btn-icon-split mb-2" data-toggle="modal" data-target="#exampleModal">
                      <span className="icon text-white-50">
                        <i className="fa fa-lock"></i>
                      </span>
                      <span className="text">Lock</span>
                    </button>
                    {/* Modal */}
                    <div className="modal fade" id="exampleModal" tabIndex={-1} role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                      <div className="modal-dialog" role="document">
                        <div className="modal-content">
                          <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLabel">Khóa Tài Khoản</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                          </div>
                          <div className="modal-body">
                            Bạn có Chắc Muốn Xóa Sản Phẩm Này
        </div>
                          <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" data-dismiss="modal">Đóng</button>
                            <NavLink to="/Managepost"><button type="button" onClick={Lock} className="btn btn-primary">Khóa Tài Khoản</button></NavLink>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  {/* Managepost */}
                  <NavLink to="/Managepost">
                    <button onClick={Unlock} type="button" className="btn btn-danger btn-icon-split mr-2">
                      <span className="icon text-white-50">
                        <i className="fa fa-unlock-alt"></i>
                      </span>
                      <span className="text">Unlock</span>
                    </button>
                  </NavLink>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
      <Footer></Footer>
    </>
  );
}

export default InforProduct;
