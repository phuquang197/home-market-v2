import { NavLink } from 'react-router-dom';
import './Admin.css';


function ManyPost(props) {
    const { product } = props;   
    
    
    return (
        <tr role="row" className="odd">
            <td>{product.name}</td>
            <td>{product.description}</td>
            <td>{product.price}</td>
            <td>{product.origin}</td>
            <td>{product.nextWeight}</td>
            <td>{product.amount}</td>
            <td className="text-center" ><img style={{width:"80px",height:"80px"}} src={product.photos}></img></td>
            <td><NavLink to={`/InforProduct/${product._id}`}>Xem Chi tết</NavLink></td>


            {/* <td>
                <NavLink to={`/InforStores/${product._id}`} className="">
                    Xem Thông Tin
                                                </NavLink>
            </td> */}
        </tr>
    );
}

export default ManyPost;
