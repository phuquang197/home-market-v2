import './Admin.css';
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import Loadpage from "../component/Loadpage";
import { API } from "../config/ConfigENV";
import {
  Link,
  useParams
} from "react-router-dom";
import MenuLeftAdmin from './MenuLeftAdmin';
import MenuAdm from './MenuAdm'
import {
  NavLink
} from "react-router-dom";
function InforStores() {


  let { storeOwnerID } = useParams();

  const [storeOwner, setStoreOwner] = useState();
  const token = localStorage.getItem("access-token");
  const fetchData = async () => {
    const result = await axios.get(`${API}/storeOwners/${storeOwnerID}`, {
    })
    return result.data
  }

  useEffect(() => {
    fetchData().then(data => {
      setStoreOwner(data)
    })
  }, [])

  async function Lock(){
    let response = await axios.put(`${API}/storeOwners/${storeOwnerID}` ,{
          name : storeOwner.name ,
          photos : storeOwner.photos,
          email: storeOwner.email,
          description:storeOwner.description,
          phoneNumbers:storeOwner.phoneNumbers,
          address:storeOwner.address,
          status : 'NOSTATUS'
    },
    {
      headers:{
          Authorization: `Bearer ${token}`,
      }
    }
    )
    await alert('Đã Huỷ chức năng Cửa Hàng');
  }
  async function Unlock(){
    let dataRisterStore = {userID: storeOwnerID}
    console.log(token);
    const result = await axios({
      method: "POST",
      url: `${API}/config_storeOwner`,
      data: dataRisterStore,
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    if(result.data.mess === 'ok'){
      alert("PHÊ DUYỆT THÀNH CÔNG ");
      window.location.replace("http://localhost:3000/indexAdm");
    }
    console.log(result);
    if(result.data.mess !== 'ok'){
      alert("CÓ LỖI XẢY RA VUI LÒNG THỬ LẠI");
    }

  }
  if (!storeOwner || storeOwner.lengh === 0) {
    return <Loadpage></Loadpage>
  }
  return (
    <div id="wrapper">
      <MenuLeftAdmin>   </MenuLeftAdmin>
      <div id="content-wrapper" className="d-flex flex-column">
        <MenuAdm> </MenuAdm>
        <div className="container mt-3">
          <div style={{ backgroundColor: 'white', boxShadow: ' 0 5px 0 rgb(200 200 200 / 20%)', backgroundColor: "white" }} className="row p-5">
            <div className="col-md-6">
              <img src={storeOwner.photos} className="d-block w-100" alt="..." />
            </div>
            <div className="col-md-6">
              <h4 className='nameStore mb-4'>TÊN CỬA HÀNG: {storeOwner.name}</h4>
              <h4 className='nameStore mb-4'>EMAIL: {storeOwner.email}</h4>
              <h4 className='nameStore mb-4'>SỐ ĐIỆN THOẠI: {storeOwner.phoneNumbers}</h4>
              <h4 className='nameStore mb-4'>ĐỊA CHỈ: {storeOwner.address}</h4>
              <h4 className='nameStore mb-4'>THÔNG TIN CỬA HÀNG:{storeOwner.description}  </h4>
              <div className="text-center">
              <Link to="/StoreApprove">
                <button onClick={Lock} type="button" className="btn btn-danger btn-icon-split mr-2">
                  <span className="icon text-white-50">
                    <i className="fa fa-lock"></i>
                  </span>
                  <span className="text">Khóa</span>
                </button>
                </Link>
                {/* ManageStore */}

                <button onClick={Unlock} type="button" className="btn btn-primary btn-icon-split mr-2">
                  <span className="icon text-white-50">
                    <i className="fa fa-unlock-alt"></i>
                  </span>
                  <span className="text">Duyệt Cửa Hàng</span>
                </button>

              </div>
            </div>

          </div>

        </div>

      </div>
    </div>
  );
}

export default InforStores;