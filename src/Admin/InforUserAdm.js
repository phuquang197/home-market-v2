import React, { useEffect, useState } from 'react';
import axios from 'axios';
import Loadpage from "../component/Loadpage";
import { API } from "../config/ConfigENV";
import {
    useParams
} from "react-router-dom";
import MenuAdm from './MenuAdm';
import MenuLeftAdmin from './MenuLeftAdmin';
import { NavLink } from 'react-router-dom';

function InforUser() {
    const [user, setUser] = useState();
    let { userID } = useParams();
    const token = localStorage.getItem("access-token");

    const fetchData = async () => {
        const result = await axios({
            method: "get",
            url: `${API}/profile/${userID}`,
            headers: {
                Authorization: `Bearer ${token}`,
            },
        });

        return result.data
    }

    useEffect(() => {
        fetchData().then(data => {
            setUser(data)
        })
    }, [])

    if (!user || user.lengh === 0) {
        return <Loadpage></Loadpage>
    }

    async function Lock() {
        let response = await axios.put(`${API}/profile/${userID}`, {
            name: user.name,
            photos: user.photos,
            email: user.email,
            phone: user.phoneNumbers,
            address: user.address,
            status: 'LOCK'
        },
            {
                headers: {
                    Authorization: `Bearer ${token}`,

                }
            }
        )
        await window.location.reload();
        // await alert('Đã Khóa Cửa Hàng');
    }
    async function Unlock() {
        let response = await axios.put(`${API}/profile/${userID}`, {
            name: user.name,
            photos: user.photos,
            email: user.email,
            phone: user.phoneNumbers,
            address: user.address,
            status: 'UNLOCK'
        },
            {
                headers: {
                    Authorization: `Bearer ${token}`,

                }
            }
        )
        await window.location.reload();
        await alert('Đã Mở Khóa');
    }
    return (
        <div id="wrapper">
            <MenuLeftAdmin>   </MenuLeftAdmin>
            <div id="content-wrapper" className="d-flex flex-column">

                <MenuAdm> </MenuAdm>
                <div className="mb-5 pt-5">
                    <div className="container">
                        <div style={{ backgroundColor: 'white', boxShadow: '0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22)' }} className="row p-4">

                            <div style={{}} className=" editUser w-100 p-5 text-center">
                                <h5 className='MenuRow'>THÔNG TIN CÁ NHÂN</h5>
                                <img className="mb-3" style={{ width: "80px", height: "80px", borderRadius: "50%" }} src={user.profilePicture} />
                                <h6 className="mb-3">Tên: {user.userName}</h6>
                                <h6 className="mb-3">Email: {user.email}</h6>
                                <h6 className="mb-3">Số Điện Thoại: {user.phone}</h6>
                                <h6 className="mb-3">Địa Chỉ: {user.address}</h6>
                                <div>
                                    {/* Button trigger modal */}
                                    <button style={{ width: '150px' }} type="button" className="btnsave p-2 mt-4 mr-2" data-toggle="modal" data-target="#exampleModal">
                                        Khóa Tài Khoản
  </button>
                                    {/* Modal */}
                                    <div className="modal fade" id="exampleModal" tabIndex={-1} role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div className="modal-dialog" role="document">
                                            <div className="modal-content">
                                                <div className="modal-header">
                                                    <h5 className="modal-title" id="exampleModalLabel">Khóa Tài Khoản</h5>
                                                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                    </button>
                                                </div>
                                                <div className="modal-body">
                                                    Bạn có Chắc Muốn Khóa Tài Khoản Này
                                                </div>
                                                <div className="modal-footer">
                                                    <button type="button" className="btn btn-secondary" data-dismiss="modal">Đóng</button>
                                                    <NavLink to="/Manageaccount">
                                                        <button type="button" onClick={Lock} className="btn btn-primary">Khóa Tài Khoản</button>
                                                    </NavLink>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                {/* <button onClick={Lock} style={{ width: '150px' }} type="button" className="btnsave p-2 mt-4 mr-2">Khóa Tài Khoản</button> */}
                                <NavLink to="/Manageaccount">
                                    <button onClick={Unlock} style={{ width: '180px' }} type="button" className="btnsave p-2 mt-4">Mở Khóa Tài Khoản</button>
                                </NavLink>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}


export default InforUser;