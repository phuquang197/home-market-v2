import './Admin.css';
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { API } from "../config/ConfigENV";
import StorePending from './StorePending';
import { makeid } from '../helpers/create/create_key_index';
import Loadpage from "../component/Loadpage";
import {
    useParams
} from "react-router-dom";
function StoreRegister() {
    let { storeOwnerID } = useParams();
    const [storeOwner, setStoreOwners] = useState();
    const [user, setUser] = useState();
    const [page, setPage] = useState();
    const token = localStorage.getItem("access-token");
    const nextPage = () => {
        setPage(page + 1);
    };
    const previousPage = () => {
        setPage(page - 1);
    };
    const fetchData = async () => {
        const result = await axios.get(`${API}/storeOwners?status=PENDING`, {
        })

        const resultuser =   await axios({
          method: "GET",
          url: `${API}/profile/findMany?limit=1000000`,
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });

        return {
          store: result.data,
          user: resultuser.data
        }
    }

    useEffect(() => {
        fetchData().then(data => {
            setStoreOwners(data.store)
            setUser(data.user)
        })
    }, [page])
    if (!storeOwner || storeOwner.lengh === 0 || !user || user.length) {
        return <Loadpage></Loadpage>
    }
    console.log(user.list);
    return (

        <div>
            {
                user.list.map((user) => {
                  if(user?.storeOwnerID?.status === 'PENDING'){
                    return <StorePending key={makeid(10)} user ={user}></StorePending>
                  }

                })
            }

                <div className="text-center">
                    <button className="previousPage btn btn-primary mr-2" onClick={previousPage}>Trang Trước</button>
                    <button className="nextPage btn btn-primary" onClick={nextPage}>Trang Tiếp Theo</button>
                </div>
            </div>
    );
}

export default StoreRegister;