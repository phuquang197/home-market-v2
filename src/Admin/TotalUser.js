import './Admin.css';
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { API } from "../config/ConfigENV";

function TableAcount() {
  const [user, setUser] = useState();
  const token = localStorage.getItem("access_token");

  const fetchData = async () => {
    const result = await axios({
      method: "get",
      url: `${API}/profile/findMany?role=BASIC`,
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });

    return result.data
  }

  useEffect(() => {
    fetchData().then(data => {
      setUser(data)
      localStorage.setItem('totalUser' , data.totalCount)
    })
  }, [])

  if (!user || user.lengh === 0) {
    return ("")
  }
 
  return (
    <div>
      {user.totalCount}

    </div>
  );
}

export default TableAcount;