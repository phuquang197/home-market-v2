import './Admin.css';
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import Loadpage from "../component/Loadpage";
import { API } from "../config/ConfigENV";
import Stores from './Stores';
import { makeid } from '../helpers/create/create_key_index';
function TableStore() {

    const [storeOwners, setStoreOwners] = useState();
    const [page, setPage] = useState();
    const nextPage = () => {
        setPage(page + 1);
      };
      const previousPage = () => {
        setPage(page -1);
      };
    const fetchData = async () => {
        const result = await axios.get(`${API}/storeOwners?status=ACCEPTED`, {
        })

        return result.data
    }

    useEffect(() => {
        fetchData().then(data => {
            setStoreOwners(data)
           
        })
    }, [page])

    if (!storeOwners || storeOwners.lengh === 0) {
        return <Loadpage></Loadpage>
    }
    return (
        <div>
            <div className="card shadow mb-4">
                <div className="card-header py-3">
                    <h6 className="m-0 font-weight-bold text-primary">List Account</h6>
                </div>
                <div className="card-body">
                    <div className="table-responsive p-4">
                        <div id="dataTable_wrapper" className="dataTables_wrapper dt-bootstrap4">
                            <div className="row">
                                <div className="col-sm-12 col-md-6">
                                    <div className="dataTables_length" id="dataTable_length">
                                    </div></div><div className="col-sm-12 col-md-6"><div id="dataTable_filter" className="dataTables_filter"></div></div></div><div className="row"><div className="col-sm-12"><table className="table table-bordered dataTable" id="dataTable" width="100%" cellSpacing={0} role="grid" aria-describedby="dataTable_info" style={{ width: '100%' }}>
                                        <thead>
                                            <tr role="row"><th className="sorting_asc" tabIndex={0} aria-controls="dataTable" rowSpan={1} colSpan={1} aria-sort="ascending" aria-label="Username: activate to sort column descending" style={{ width: '119px' }}>
                                                Tên Cửa Hàng</th>
                                                <th className="sorting" tabIndex={0} aria-controls="dataTable" rowSpan={1} colSpan={1} aria-label="Email: activate to sort column ascending" style={{ width: '357px' }}>
                                                    Email</th>
                                                <th className="sorting" tabIndex={0} aria-controls="dataTable" rowSpan={1} colSpan={1} aria-label="Phone: activate to sort column ascending" style={{ width: '124px' }}>
                                                    Số Điện Thoại</th>
                                                <th className="sorting" tabIndex={0} aria-controls="dataTable" rowSpan={1} colSpan={1} aria-label="Address: activate to sort column ascending" style={{ width: '136px' }}>
                                                    Địa Chỉ</th>
                                                <th className="sorting" tabIndex={0} aria-controls="dataTable" rowSpan={1} colSpan={1} aria-label="Type: activate to sort column ascending" style={{ width: '88px' }}>
                                                    Xem Cửa Hàng</th>
                                                {/* <th className="sorting" tabIndex={0} aria-controls="dataTable" rowSpan={1} colSpan={1} aria-label="Function: activate to sort column ascending" style={{ width: '125px' }}>
                                                    Function</th> */}
                                            </tr>
                                        </thead>
                                        {/* bỏ vào đây */}
                                        <tbody>
                                            {
                                                storeOwners.list.map((storeOwner) => {
                                                    return <Stores key={makeid(10)} storeOwner={storeOwner}></Stores>
                                                })
                                            }
                                        </tbody>

                                    </table>
                                    </div></div>
                            <div className="text-center">
                                <button className="previousPage btn btn-primary mr-2" onClick={previousPage}>Trang Trước</button>
                                <button className="nextPage btn btn-primary" onClick={nextPage}>Trang Tiếp Theo</button>
                            </div>                                    </div>
                    </div>
                </div>
            </div>
        </div>
        // <div>
        //     <div className="container pb-5 pt-5">
        //       {
        //         storeOwners.list.map((storeOwner) => {
        //           return <Stores key={makeid(10)} storeOwner={storeOwner}></Stores>
        //         })
        //       }
        //     </div>

        // </div>
    );
}

export default TableStore;