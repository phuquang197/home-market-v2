import React from 'react';
import Dashboard from './Dashboard';
import MenuAdm from './MenuAdm';
import MenuLeftAdmin from './MenuLeftAdmin';
function IndexAdm() {
    return (
        <div>
            <div id="wrapper">
                <MenuLeftAdmin></MenuLeftAdmin>
                <div id="content-wrapper" className="d-flex flex-column">
                    <MenuAdm></MenuAdm>
                    <br></br>
                    <div className="container-fuild p-3">
                        <Dashboard></Dashboard>
                    </div>
                </div>

            </div>
        </div>
    );
}

export default IndexAdm;