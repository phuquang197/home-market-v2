import React from 'react';
import NumberFormat from 'react-number-format';
import {  getGiaoHangStatus } from "../helpers/getTagProduct";
import { gePaymentStatus } from "../helpers/getTagProduct";
import { getcuahang } from "../helpers/getTagProduct";
function PayMentAdm(props) {
    const { payment } = props;
    return (
        <tr role="row" className="odd">
            <td>{gePaymentStatus(payment.paymentStatus)}</td>
            <td>{getGiaoHangStatus(payment.giaoHangStatus)}</td>
            <td><NumberFormat value={payment.totalMoney} displayType={'text'} thousandSeparator={true}/>VNĐ</td>
            {/* <td>{getcuahang(payment.userCreatedProductID)} </td> */}
            <td>{getcuahang(payment.userCreatedProductID)}</td>
            {/* <td>{payment.photos}</td> */}



            {/* <td>
                <NavLink to={`/InforStores/${payment._id}`} className="">
                    Xem Thông Tin
                                                </NavLink>
            </td> */}
        </tr>
    );
}

export default PayMentAdm;