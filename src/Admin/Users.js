import React from 'react';
import { NavLink } from 'react-router-dom';
function Users(props) {
    const { user } = props;

    return (


        <tr role="row" className="odd">
            <td>{user.userName}</td>
            <td>{user.email}</td>
            <td>{user.phone}</td>
            <td>{user.address}</td>
        
            <td>
                {/* <a href="/InforUserAdm" className="">
                    Xem Thông Tin
                </a> */}
                <NavLink to={`/InforUserAdm/${user._id}`} className="">
                    Xem Thông Tin
                                                    </NavLink>
            </td>
            <td>{user.status}</td>
        </tr>

    );
}

export default Users;