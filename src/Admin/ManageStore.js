import React from 'react';

import MenuAdm from './MenuAdm';
import MenuLeftAdmin from './MenuLeftAdmin';
import TableStore from './TableStore';
function ManageStore() {
    return (
        <div id="wrapper">
        <MenuLeftAdmin></MenuLeftAdmin>
        <div id="content-wrapper" className="d-flex flex-column">
            <MenuAdm></MenuAdm>
            <br></br>
            <div className="container-fuild p-3">
                <TableStore></TableStore>
            </div>
        </div>

    </div>
    );
}

export default ManageStore;