import './Admin.css';
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { API } from "../config/ConfigENV";

function TotalStore() {
    const [storeOwners, setStoreOwners] = useState();
    const fetchData = async () => {
        const result = await axios.get(`${API}/storeOwners?status=ACCEPTED`, {
        })

        return result.data
    }

    useEffect(() => {
        fetchData().then(data => {
            setStoreOwners(data)
            localStorage.setItem('totalStore' , data.totalCount)
        })
    }, [])

    if (!storeOwners || storeOwners.lengh === 0) {
        return ("")
    }
    return (
        <div>
            {storeOwners.totalCount}
        </div>
    );
}

export default TotalStore;