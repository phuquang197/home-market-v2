import './Admin.css';
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { API } from "../config/ConfigENV";
import { makeid } from '../helpers/create/create_key_index';
import PayMentAdm from './PayMentAdm';
import Loadpage from '../component/Loadpage';

function TablePayMent() {
    const [payment, setPayment] = useState();
    const [page, setPage] = useState();
    const nextPage = () => {
        setPage(page + 1);
    };
    const previousPage = () => {
        setPage(page - 1);
    };
    const token = localStorage.getItem("access-token");

    const fetchData = async () => {
        const result = await axios({
            method: "get",
            url: `${API}/payment?paymentStatus=PAYMENTED&limit=8${page}`,
            headers: {
                Authorization: `Bearer ${token}`,
            },
        });

        return result.data
    }

    useEffect(() => {
        fetchData().then(data => {
            setPayment(data)
        })
    }, [page])

    if (!payment || payment.lengh === 0) {
        return <Loadpage></Loadpage>
    }

    return (
        <div>
            <div className="card shadow mb-4">
                <div className="card-header py-3">
                    <h6 className="m-0 font-weight-bold text-primary">Danh Sách Sản Phẩm</h6>
                </div>
                <div className="card-body">
                    <div className="table-responsive p-4">
                        <div id="dataTable_wrapper" className="dataTables_wrapper dt-bootstrap4">
                            <div className="row">
                                <div className="col-sm-12 col-md-6">
                                    <div className="dataTables_length" id="dataTable_length">
                                    </div></div><div className="col-sm-12 col-md-6"><div id="dataTable_filter" className="dataTables_filter"></div></div></div><div className="row"><div className="col-sm-12"><table className="table table-bordered dataTable" id="dataTable" width="100%" cellSpacing={0} role="grid" aria-describedby="dataTable_info" style={{ width: '100%' }}>
                                        <thead>
                                            <tr role="row"><th className="sorting_asc" tabIndex={0} aria-controls="dataTable" rowSpan={1} colSpan={1} aria-sort="ascending" aria-label="Username: activate to sort column descending" style={{ width: '119px' }}>
                                                Trạng Thái Thanh Toán</th>
                                                <th className="sorting" tabIndex={0} aria-controls="dataTable" rowSpan={1} colSpan={1} aria-label="Email: activate to sort column ascending" style={{ width: '357px' }}>
                                                    Hinh Thức Nhận Hàng</th>
                                                <th className="sorting" tabIndex={0} aria-controls="dataTable" rowSpan={1} colSpan={1} aria-label="Phone: activate to sort column ascending" style={{ width: '124px' }}>
                                                    Số Tiền Thanh Toán</th>
                                                <th className="sorting" tabIndex={0} aria-controls="dataTable" rowSpan={1} colSpan={1} aria-label="Address: activate to sort column ascending" style={{ width: '136px' }}>
                                                    Cửa Hàng</th>
                                                {/* <th className="sorting" tabIndex={0} aria-controls="dataTable" rowSpan={1} colSpan={1} aria-label="Function: activate to sort column ascending" style={{ width: '125px' }}>
                                                    Số Lượng</th> */}

                                            </tr>
                                        </thead>
                                        {/* bỏ vào đây */}
                                        <tbody>
                                            {
                                                payment.list.map((payment) => {
                                                    return <PayMentAdm key={makeid(10)} payment={payment}></PayMentAdm>
                                                })
                                            }
                                        </tbody>

                                    </table>
                                    </div>
                            </div>
                            <div className="text-center">
                                <button className="previousPage btn btn-primary mr-2" onClick={previousPage}>Ẩn Bớt</button>
                                <button className="nextPage btn btn-primary" onClick={nextPage}>Xem Thêm</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default TablePayMent;