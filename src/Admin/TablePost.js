// import './Admin.css';
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import Loadpage from "../component/Loadpage";
import { API } from "../config/ConfigENV";
import { makeid } from '../helpers/create/create_key_index';
import ManyPost from './ManyPost';
// import Pagination from '../component/Pagination';

function TablePost() {
    
    const [product, setProduct] = useState();
    const [page, setPage] = useState(0);
    const nextPage = () => {
        setPage(page + 1);
      };
      const previousPage = () => {
        setPage(page -1);
      };
    const fetchData = async () => {
        const result = await axios.get(`${API}/products?status=STOCKING&limit=1${page}`, {
        })

        return result.data
    }

    useEffect(() => {
        fetchData().then(data => {
            setProduct(data)
        })
    }, [page])
    
    if (!product || product.lengh === 0) {
        return <Loadpage></Loadpage>
    }
    
    console.log(product);
    return (
        <div>
        <div className="card shadow mb-4">
            <div className="card-header py-3">
                <h6 className="m-0 font-weight-bold text-primary">Danh Sách Sản Phẩm</h6>
            </div>
            <div className="card-body">
                <div className="table-responsive p-4">
                    <div id="dataTable_wrapper" className="dataTables_wrapper dt-bootstrap4">
                        <div className="row">
                            <div className="col-sm-12 col-md-6">
                                <div className="dataTables_length" id="dataTable_length">
                                </div></div><div className="col-sm-12 col-md-6"><div id="dataTable_filter" className="dataTables_filter"></div></div></div><div className="row"><div className="col-sm-12"><table className="table table-bordered dataTable" id="dataTable" width="100%" cellSpacing={0} role="grid" aria-describedby="dataTable_info" style={{ width: '100%' }}>
                                    <thead>
                                        <tr role="row">
                                            <th className="sorting_asc" tabIndex={0} aria-controls="dataTable" rowSpan={1} colSpan={1} aria-sort="ascending" aria-label="Username: activate to sort column descending" style={{ width: '119px' }}>
                                            Tên Sản Phẩm</th>
                                            <th className="sorting" tabIndex={0} aria-controls="dataTable" rowSpan={1} colSpan={1} aria-label="Email: activate to sort column ascending" style={{ width: '357px' }}>
                                                Mô tả</th>
                                            <th className="sorting" tabIndex={0} aria-controls="dataTable" rowSpan={1} colSpan={1} aria-label="Phone: activate to sort column ascending" style={{ width: '124px' }}>
                                            Gía</th>
                                            <th className="sorting" tabIndex={0} aria-controls="dataTable" rowSpan={1} colSpan={1} aria-label="Address: activate to sort column ascending" style={{ width: '136px' }}>
                                            Xuất Xứ</th>
                                            <th className="sorting" tabIndex={0} aria-controls="dataTable" rowSpan={1} colSpan={1} aria-label="Type: activate to sort column ascending" style={{ width: '88px' }}>
                                            Khối Lượng Tịnh</th>
                                            <th className="sorting" tabIndex={0} aria-controls="dataTable" rowSpan={1} colSpan={1} aria-label="Function: activate to sort column ascending" style={{ width: '125px' }}>
                                                Số Lượng</th>
                                            <th className="sorting" tabIndex={0} aria-controls="dataTable" rowSpan={1} colSpan={1} aria-label="Function: activate to sort column ascending" >
                                                Hình Ảnh</th>
                                        </tr>
                                    </thead>
                                    {/* bỏ vào đây */}
                                    <tbody>
                                        {
                                            product.list.map((product) => {
                                                return <ManyPost key={makeid(10)} product={product}></ManyPost>
                                            })

                                        }
                                    </tbody>

                                </table>
                                </div>
                                </div>
                                {/* <div className="row"><div className="col-sm-12 col-md-5"><div className="dataTables_info" id="dataTable_info" role="status" aria-live="polite">Showing 1 to 3 of 3 entries</div></div><div className="col-sm-12 col-md-7"><div className="dataTables_paginate paging_simple_numbers" id="dataTable_paginate"><ul className="pagination"><li className="paginate_button page-item previous disabled" id="dataTable_previous"><a href="/#" aria-controls="dataTable" data-dt-idx={0} tabIndex={0} className="page-link">Previous</a></li><li className="paginate_button page-item active"><a href="/#" aria-controls="dataTable" data-dt-idx={1} tabIndex={0} className="page-link">1</a></li><li className="paginate_button page-item next disabled" id="dataTable_next"><a href="/#" aria-controls="dataTable" data-dt-idx={2} tabIndex={0} className="page-link">Next</a></li></ul></div></div></div> */}
                                {/* <Pagination></Pagination> */}
                                <div className="text-center">
                                <button className="previousPage btn btn-primary mr-2"onClick={previousPage}>Ẩn Bớt</button>
                                <button className="nextPage btn btn-primary" onClick={nextPage}>Xem Thêm</button>
                                </div>
                                </div>
                </div>
            </div>
        </div>
    </div>
    );
}

export default TablePost;
