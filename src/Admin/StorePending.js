import './Admin.css';
import React  from 'react';


import {
  NavLink
} from "react-router-dom";
function StorePending(props) {

    const { user } = props;
    return (
        <div style={{borderBottom:"solid 1px silver"}} className="mb-2 pb-4">

            <div className="text-left">Tên Cửa Hàng: {user.storeOwnerID.name}</div>
            <div className="text-left"><img className="mb-3" style={{ width: "80px", height: "80px"}} src={user.storeOwnerID.photos}  /></div>
            <div className="text-left">Email: {user.storeOwnerID.email}</div>
            <div className="text-left">Thông Tin: {user.storeOwnerID.description}</div>
            <div className="text-left">Số Điện Thoại: {user.storeOwnerID.phoneNumbers}</div>
            <div className="text-left">Địa Chỉ: {user.storeOwnerID.address}</div>
            <div className="text-left">
                {/* <NavLink to={`/ApprovalStore/${user.storeOwnerID._id} `}><button type="button" className="btn btn-primary">xem chi tiết</button></NavLink> */}
                 <NavLink to={`/ApprovalStore/${user._id}`} className="btn btn-primary">
                                                       Xem Thông Tin
                                                    </NavLink>
                </div>
        </div>
    );
}

export default StorePending;