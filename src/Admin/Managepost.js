import React from 'react';
import MenuAdm from './MenuAdm';
import MenuLeftAdmin from './MenuLeftAdmin';
import TablePost from './TablePost';

function Managepost() {
    return (
        <div>
            <div id="wrapper">
                <MenuLeftAdmin></MenuLeftAdmin>
                <div id="content-wrapper" className="d-flex flex-column">
                    <MenuAdm></MenuAdm>
                    <br></br>
                    <div className="container-fuild p-3">
                        <TablePost></TablePost>
                    </div>
                </div>

            </div>
        </div>
    );
}

export default Managepost;
