import React, { useState } from 'react';
export let context = React.createContext();

export function Provide(props) {
    const [image , setImage] = useState('')

    function setLinkImage(link){
        setImage(link)
    }
    return (
        <div>
            <context.Provider value={{
                linkImage : image ,
                setLinkImage : setLinkImage
            }} >
                {props.children}
            </context.Provider>
        </div>
    );
}

