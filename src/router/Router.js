import React from 'react';
import {
    // BrowserRouter as Router,
    Route,
  }
from "react-router-dom";
// import App from '../App';
import Home from "../pageUser/Home";
import Industry from '../pageUser/Industry';
import InfomationStore from '../pageUser/InfomationStore';
import Introduce from '../pageUser/Introduce';
import KhuyenMai from '../pageUser/KhuyenMai';
import Register from '../pageUser/Register';
import Giohang from '../pageUser/Giohang';
import InfomationUser from '../pageUser/InfomationUser';
import Beat from '../pageUser/Beat';
import MilkEgg from '../pageUser/MilkEgg';
import Drinks from '../pageUser/Drinks';
import Frozenfood from '../pageUser/Frozenfood';
import Seafood from '../pageUser/Seafood';
import Bread from '../pageUser/Bread';
import Vegartable from '../pageUser/Vegartable';
import Fruit from '../pageUser/Fruit';
import ResetPassword from '../pageUser/ResetPassword';
import ManyStore from '../pageUser/ManyStore';
import AddCard from '../pageUser/AddCard';
import RegisterStore from '../pageStore/RegisterStore';
import Addproduct from '../pageStore/Addproduct';
import Product from '../pageUser/Product';
import Productstore from '../pageStore/Productstore';
import Correctentryprice from '../pageStore/Correctentryprice';
import Pricefixes from '../pageStore/Pricefixes';
import DeletehistoryProduct from '../pageStore/DeletehistoryProduct';
import Warehouseentry from '../pageStore/Warehouseentry';
import Billofsale from '../pageStore/Billofsale';
import User from '../pageStore/User';
import Sale from '../pageStore/Sale';
import Search from '../pageUser/Search';
import BeatTopMarket from '../pageUser/TopMarlet/BeatTopMarket';
import SeaFoodTopMarket from '../pageUser/TopMarlet/SeaFoodTopMarket';
import PoultryTopMarket from '../pageUser/TopMarlet/PoultryTopMarket';
import BreadTopMarket from '../pageUser/TopMarlet/BreadTopMarket';
import FrozenfoodTopMarket from '../pageUser/TopMarlet/FrozenfoodTopMarket';
import FruitTopMarket from '../pageUser/TopMarlet/FruitTopMarket';
import MilkEggTopMarket from '../pageUser/TopMarlet/MilkEggTopMarket';
import VegartableTopMarket from '../pageUser/TopMarlet/VegartableTopMarket';
import BeatLotteMart from '../pageUser/LotteMart/BeatLotteMart';
import SeaFoodLotteMart from '../pageUser/LotteMart/SeaFoodLotteMart';
import BreadLotteMart from '../pageUser/LotteMart/BreadLotteMart';
import FrozenfoodLotteMart from '../pageUser/LotteMart/FrozenfoodLotteMart';
import FruitLotteMart from '../pageUser/LotteMart/FruitLotteMart';
import MilkEggLotteMart from '../pageUser/LotteMart/MilkEggLotteMart';
import VegartableLotteMart from '../pageUser/LotteMart/VegartableLotteMart';
import IndexAdm from '../Admin/IndexAdm';
import Manageaccount from '../Admin/Manageaccount';
import Managepost from '../Admin/Managepost';
import Dashboard from '../Admin/Dashboard';
import EditProduct from '../pageStore/EditProduct';
import EmployeeEdit from '../pageStore/EmployeeEdit';
import ChartTopMarket from '../pageStore/ChartofStore';
import PaymentSuccess from '../pageUser/PaymentSuccess';
import Paymentfailed from '../pageUser/Paymentfailed';
import CartOfStore from '../pageUser/CartOfStore';
import Delivery from '../pageUser/Delivery';
import ManageStore from '../Admin/ManageStore';
import InforStores from '../Admin/InforStores';
import LoginAdm from '../Admin/LoginAdm';
import LikeProduct from '../pageUser/LikeProduct';
import StoreApprove from '../Admin/StoreApprove';
import ProductLike from '../pageUser/ProductLike';
import ManagePayment from '../Admin/ManagePayment';
import ApprovalStore from '../Admin/ApprovalStore';
import PaymentOfStore from '../pageStore/PaymentOfStore';
import InforUser from '../Admin/InforUserAdm';
import NotificationsOfUser from '../pageUser/NotificationsOfUser';
import HistoryPaymentOfUser from '../pageUser/HistoryPaymentOfUser';
import IndexNotifi from '../pageUser/IndexNotifi';
import IndexNotifiStore from '../pageStore/IndexNotifiStore';
import NotificationsOfStore from '../pageStore/NotificationsOfStore';
import InforProduct from '../Admin/InforProduct';
import IndexPayment from '../pageUser/IndexPayment';

function Dieuhuong( ) {
  return (
    <div>
      <Route exact path="/"> <Home  ></Home> </Route> {/* index user */}
      <Route path="/Introduce">  <Introduce></Introduce> </Route>  {/* Introduce user */}

      <Route path="/Industry">  <Industry></Industry> </Route> {/* Industry user */}
      <Route path="/KhuyenMai">  <KhuyenMai></KhuyenMai> </Route> {/* KhuyenMai user */}
      <Route path="/Register">  <Register></Register> </Route>{/* Register user */}
      <Route path="/InformationStore/:storeOwnerID">  <InfomationStore></InfomationStore> </Route>{/* InfomationStore user */}
      <Route path="/Giohang/:storeOwnerID">  <Giohang></Giohang> </Route>{/* Giohang user */}
      <Route path="/CartOfStore">  <CartOfStore> </CartOfStore> </Route>{/* Giohang user */}
      <Route path="/InformationUser/:userID">  <InfomationUser></InfomationUser> </Route>{/* InfomationUser user */}
      <Route path="/Beat">  <Beat></Beat> </Route>{/* Beat user */}
      <Route path="/MilkEgg">  <MilkEgg></MilkEgg> </Route>{/* MilkEgg user */}
      <Route path="/Drinks">  <Drinks></Drinks> </Route>{/* Drinks user */}
      <Route path="/Frozenfood">  <Frozenfood></Frozenfood> </Route>{/* Frozenfood user */}
      <Route path="/Seafood">  <Seafood></Seafood> </Route>{/* Seafood user */}
      <Route path="/Bread">  <Bread></Bread> </Route>{/* Bread user */}
      <Route path="/Vegartable">  <Vegartable></Vegartable> </Route>{/* Vegartable user */}
      <Route path="/Fruit">  <Fruit></Fruit></Route>{/* Fruit user */}
      <Route path="/ResetPassword/:useID"> <ResetPassword></ResetPassword> </Route>{/* ResetPassword user */}
      <Route path="/HistoryPaymentOfUser"><HistoryPaymentOfUser></HistoryPaymentOfUser> </Route>
      <Route path="/ManyStore"> <ManyStore></ManyStore> </Route>{/* ManyStore user */}
      <Route path="/AddCard"> <AddCard></AddCard> </Route>{/* AddCard user */}
      <Route path="/RegisterStore"> <RegisterStore></RegisterStore> </Route>{/* RegisterStore user */}
      <Route path="/BeatTopMarket"><BeatTopMarket></BeatTopMarket> </Route>  {/* BeatTopMarket User */}
      <Route path="/SeaFoodTopMarket"><SeaFoodTopMarket></SeaFoodTopMarket> </Route> {/* SeaFoodTopMarket User */}
      <Route path="/PoultryTopMarket"><PoultryTopMarket></PoultryTopMarket> </Route> {/* PoultryTopMarket User */}
      <Route path="/BreadTopMarket"><BreadTopMarket></BreadTopMarket> </Route> {/* BreadTopMarket User */}
      <Route path="/FrozenfoodTopMarket"><FrozenfoodTopMarket></FrozenfoodTopMarket> </Route> {/* FrozenfoodTopMarket User */}
      <Route path="/FruitTopMarket"><FruitTopMarket></FruitTopMarket> </Route> {/* FruitTopMarket User */}
      <Route path="/MilkEggTopMarket"><MilkEggTopMarket></MilkEggTopMarket> </Route> {/* MilkEggTopMarket User */}
      <Route path="/VegartableTopMarket"><VegartableTopMarket></VegartableTopMarket> </Route> {/* VegartableTopMarket User */}
      <Route path="/BeatLotteMart"><BeatLotteMart></BeatLotteMart> </Route> {/* BeatTopMarket User */}
      <Route path="/SeaFoodLotteMart"><SeaFoodLotteMart></SeaFoodLotteMart> </Route> {/* SeaFoodLotteMart User */}
      <Route path="/BreadLotteMart"><BreadLotteMart></BreadLotteMart> </Route> {/* BreadLotteMart User */}
      <Route path="/FrozenfoodLotteMart"><FrozenfoodLotteMart></FrozenfoodLotteMart> </Route> {/* FrozenfoodLotteMart User */}
      <Route path="/FruitLotteMart"><FruitLotteMart></FruitLotteMart> </Route> {/* FruitLotteMart User */}
      <Route path="/MilkEggLotteMart"><MilkEggLotteMart></MilkEggLotteMart> </Route> {/* MilkEggLotteMart User */}
      <Route path="/VegartableLotteMart"><VegartableLotteMart></VegartableLotteMart> </Route> {/* VegartableLotteMart User */}
      <Route path="/Search"><Search></Search> </Route> {/* Search User */}
      <Route path="/PaymentSuccess"><PaymentSuccess></PaymentSuccess> </Route> {/* PaymentSuccess User */}
      <Route path="/Paymentfailed"><Paymentfailed>   </Paymentfailed> </Route>
      <Route path="/Delivery"> <Delivery></Delivery> </Route>
      <Route path="/LikeProduct/useID"><LikeProduct></LikeProduct> </Route>
      <Route path="/ProductLike"><ProductLike> </ProductLike> </Route>
      <Route path="/IndexNotifi/:useID"><IndexNotifi></IndexNotifi> </Route>
      <Route path="/NotificationsOfUser/:notificationID"><NotificationsOfUser></NotificationsOfUser></Route>
      <Route path="/Addproduct"> <Addproduct></Addproduct> </Route>{/* Addproduct STORE */}
      <Route path="/Product"> <Product></Product>  </Route>{/* product STORE */}
      <Route path="/Product2/:productID"> <Product></Product>  </Route> {/* product user */}
      <Route path="/Productstore"> <Productstore></Productstore> </Route> {/* Productstore STORE */}
      <Route path="/EditProduct/:productID"> <EditProduct></EditProduct> </Route> {/* EditProduct STORE */}
      <Route path="/Correctentryprice"> <Correctentryprice></Correctentryprice> </Route>{/* Correctentryprice STORE */}
      <Route path="/Pricefixes"><Pricefixes></Pricefixes> </Route>{/* Pricefixes STORE */}
      <Route path="/DeletehistoryProduct"><DeletehistoryProduct></DeletehistoryProduct> </Route>{/* DeletehistoryProduct STORE */}
      <Route path="/Warehouseentry"><Warehouseentry></Warehouseentry> </Route>{/* Warehouseentry STORE */}
      <Route path="/Billofsale"><Billofsale></Billofsale> </Route>{/* Billofsale STORE */}
      <Route path="/User"><User></User> </Route> {/* User STORE */}
      <Route path="/Sale"><Sale></Sale> </Route> {/* Sale STORE */}
      <Route path="/ChartofStore"><ChartTopMarket></ChartTopMarket> </Route>
      <Route path="/paymentOfStore"><PaymentOfStore></PaymentOfStore> </Route>
      <Route path="/IndexNotifiStore/:storeOwnerID"><IndexNotifiStore></IndexNotifiStore> </Route>
      <Route path="/IndexPayment/:paymentID"><IndexPayment></IndexPayment> </Route>
      <Route path="/NotificationsOfStore/:notificationID"><NotificationsOfStore></NotificationsOfStore> </Route>
      <Route path="/IndexAdm"><IndexAdm></IndexAdm> </Route>{/* index ADMIN */}
      <Route path="/Manageaccount"><Manageaccount></Manageaccount></Route> {/* Manageaccount ADMIN */}
      <Route path="/Managepost"><Managepost></Managepost></Route> {/* Managepost ADMIN */}
      <Route path="/ManageStore"><ManageStore> </ManageStore></Route> {/* Managepost ADMIN */}
      <Route path="/Dashboard"><Dashboard></Dashboard> </Route> {/* Dashboard ADMIN */}
      <Route path="/EmployeeEdit"><EmployeeEdit></EmployeeEdit> </Route> {/* EmployeeEdit ADMIN */}
      <Route path="/InforStores/:storeOwnerID"><InforStores></InforStores> </Route>
      <Route path="/LoginAdm"><LoginAdm></LoginAdm> </Route>
      <Route path="/StoreApprove"> <StoreApprove></StoreApprove></Route>
      <Route path="/ManagePayment"><ManagePayment></ManagePayment> </Route>
      <Route path="/InforUserAdm/:userID"><InforUser></InforUser> </Route>
      <Route path="/ApprovalStore/:storeOwnerID"><ApprovalStore></ApprovalStore> </Route>
      <Route path="/InforProduct/:productID"><InforProduct></InforProduct> </Route>

      

    </div>
  );
}

export default Dieuhuong;