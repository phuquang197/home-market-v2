import React from 'react';
import { NavLink } from 'react-router-dom';
import SlideSale from './SlideSale';

function MenuLeftOfUser(props) {
  return (
    <div>

      <ul className="nav flex-column">
        <h6 style={{ color: "#333333", fontWeight: "500" }} className="pl-4 pt-4"> NGÀNH HÀNG</h6>

        <ul style={{ borderBottom: "1px solid #f0f0f0" }} className="list-group list-group-flush mb-2">
          <li className="list-group-item"><NavLink className="MenuColumn" to="/Beat">Thịt</NavLink></li>
          <li className="list-group-item"><NavLink className="MenuColumn" to="/Seafood">Hải Sản</NavLink></li>
          <li className="list-group-item"><NavLink className="MenuColumn" to="/Drinks">Nước Uống</NavLink></li>
          <li className="list-group-item"><NavLink className="MenuColumn" to="/MilkEgg">Trứng, Sữa</NavLink></li>
          <li className="list-group-item"><NavLink className="MenuColumn" to="/Frozenfood">Đồ Đông Lạnh</NavLink></li>
          <li className="list-group-item"><NavLink className="MenuColumn" to="/Fruit">Trái Cây</NavLink></li>
          <li className="list-group-item"><NavLink className="MenuColumn" to="/Bread">Bánh Mì</NavLink></li>
          <li className="list-group-item"><NavLink className="MenuColumn" to="/Vegartable">Rau, Củ</NavLink></li>
        </ul>
        <img className="w-100" src="https://demo.templatetrip.com/Opencart/OPC02/OPC040/OPC05/image/cache/catalog/demo/banners/left-banner-1-274x500.jpg"></img>
        {/* <SlideSale></SlideSale> */}
        <h5 className="text-center mt-3 mb-3 MenuColumn">Đánh Gía Về HomeMarket</h5>
        <div id="carouselExampleIndicators" className="carousel slide mt-3 mb-3" data-ride="carousel">
          <ol className="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to={0} className="active" />
            <li data-target="#carouselExampleIndicators" data-slide-to={1} />
            <li data-target="#carouselExampleIndicators" data-slide-to={2} />
          </ol>
          <div className="carousel-inner">
            <div className="carousel-item active">
              <div className="w-43 mt-3 mb-4 border-bottom" style={{ width: "43%" }} className="media">

                <img style={{ border: "solid 1px #eeeeee", borderRadius: "50%", height: "72px" }} className="w-100 mr-2" src="https://by.com.vn/tYkqJW" alt="..." />
                <div className="media-body MenuColumn mb-4 mt-2">
                  <h6 className="mt-0" style={{ width: "159px", color: " #666666", fontWeight: "400" }}>Lê Đức Dũng</h6>
                  <small>Sinh Viên Đại Học Duy Tân</small>
                </div>
              </div>
              <div style={{ color: "#888", fontFamily: "serif" }} className="w-100">
                <i>"Nhờ Có HomeMartket việc đi chợ của tôi dễ dàng hơn không còn tốn nhiều thời gian như lúc trước"</i>
              </div>
            </div>
            <div className="carousel-item">
              <div className="w-43 mt-3 mb-4 border-bottom" style={{ width: "43%" }} className="media">

                <img style={{ border: "solid 1px #eeeeee", borderRadius: "50%", height: "72px" }} className="w-100 mr-2" src="https://by.com.vn/tYkqJW" alt="..." />
                <div className="media-body MenuColumn mb-4 mt-2">
                  <h6 className="mt-0" style={{ width: "159px", color: " #666666", fontWeight: "400" }}>Lê Đức Dũng</h6>
                  <small>Sinh Viên Đại Học Duy Tân</small>
                </div>

              </div>
              <div style={{ color: "#888", fontFamily: "serif" }} className="w-100">
                <i>"Nhờ Có HomeMartket việc đi chợ của tôi dễ dàng hơn không còn tốn nhiều thời gian như lúc trước"</i>
              </div>
            </div>
            <div className="carousel-item">
              <div className="w-43 mt-3 mb-4 border-bottom" style={{ width: "43%" }} className="media">

                <img style={{ border: "solid 1px #eeeeee", borderRadius: "50%", height: "72px" }} className="w-100 mr-2" src="https://by.com.vn/tYkqJW" alt="..." />
                <div className="media-body MenuColumn mb-4 mt-2">
                  <h6 className="mt-0" style={{ width: "159px", color: " #666666", fontWeight: "400" }}>Lê Đức Dũng</h6>
                  <small>Sinh Viên Đại Học Duy Tân</small>
                </div>
              </div>
              <div style={{ color: "#888", fontFamily: "serif" }} className="w-100">
                <i>Nhờ Có HomeMartket việc đi chợ của tôi dễ dàng hơn không còn tốn nhiều thời gian như lúc trước</i>
              </div>
            </div>
          </div>
          <a className="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span className="carousel-control-prev-icon" aria-hidden="true" />
            <span className="sr-only">Previous</span>
          </a>
          <a className="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span className="carousel-control-next-icon" aria-hidden="true" />
            <span className="sr-only">Next</span>
          </a>
        </div>


        <div style={{ borderBottom: "solid 1px #666666" }}>
          <h5 className="text-center mt-3 mb-3 MenuColumn">Sản Phẩm Nổi Bật</h5>
          <a href="Product2/609a1d47a32e59003bcf187b" className="w-43 mt-3 mb-4 border-bottom" style={{ width: "43%" }} className="media">

            <img style={{ border: "solid 1px #eeeeee" }} className="w-100 mr-1" src="https://firebasestorage.googleapis.com/v0/b/capstone2-24fd3.appspot.com/o/product%2Fmeat%2FTh%C4%83n_Vai_B%C3%B2_M%E1%BB%B9_Steak-removebg-preview.png?alt=media&token=9d2807bb-6898-4d99-817e-788556307080" alt="..." />
            <div className="media-body MenuColumn mb-4">
              <h6 className="mt-0" style={{ width: "159px", color: " #666666", fontWeight: "400" }}>Thăn Vai Bò Mỹ</h6>
              <small>Annam Gourmet</small>
              <p style={{ color: "#08c25e" }}>258,250 VNĐ</p>
            </div>
          </a>
          <a href="Product2/608d5f20a7d189003b4de63d" className="w-43 mt-3 mb-4 border-bottom" style={{ width: "43%" }} className="media">

            <img style={{ border: "solid 1px #eeeeee" }} className="w-100 mr-1" src="https://firebasestorage.googleapis.com/v0/b/capstone2-24fd3.appspot.com/o/product%2Fseafood%2Ftomhumalaska-removebg-preview%20(1).png?alt=media&token=070d08ff-78c8-4a67-82e9-441ebd6f86b8" alt="..." />
            <div className="media-body MenuColumn mb-4">
              <h6 className="mt-0" style={{ width: "159px", color: " #666666", fontWeight: "400" }}>Tôm hùm alaska</h6>
              <small>LotteMart</small>
              <p style={{ color: "#08c25e" }}>207.000 VNĐ</p>
            </div>
          </a>
          <a href="Product2/609a2078a32e59003bcf187f" className="w-43 mt-3 mb-4 border-bottom" style={{ width: "43%" }} className="media">

            <img style={{ border: "solid 1px #eeeeee" }} className="w-100 mr-1" src="https://firebasestorage.googleapis.com/v0/b/capstone2-24fd3.appspot.com/o/product%2Fmeat%2Fphi_l%C3%AA_b%C3%B2-removebg-preview.png?alt=media&token=48937fbe-02bb-4463-93b4-f3d88c3d1525" alt="..." />
            <div className="media-body MenuColumn mb-4">
              <h6 className="mt-0" style={{ width: "159px", color: " #666666", fontWeight: "400" }}>Phi Lê Bò</h6>
              <small>LotteMart</small>
              <p style={{ color: "#08c25e" }}>207,000 VNĐ</p>
            </div>
          </a>


          <a href="Product2/608d68c3a7d189003b4de645" className="w-43 mt-3 mb-4" style={{ width: "43%" }} className="media">

            <img style={{ border: "solid 1px #eeeeee" }} className="w-100 mr-1" src="https://firebasestorage.googleapis.com/v0/b/capstone2-24fd3.appspot.com/o/product%2Fseafood%2Fsasimi_c%C3%A1_h%E1%BB%93i-removebg-preview.png?alt=media&token=bc094692-8187-403c-a51e-e6fcb507d905" alt="..." />
            <div className="media-body MenuColumn mb-4">
              <h6 className="mt-0" style={{ maxWidth: "159px", color: " #666666", fontWeight: "400" }}>Sashimi cá hồi</h6>
              <small>Cho Pho</small>
              <p style={{ color: "#08c25e" }}>320,000 VNĐ</p>
            </div>
          </a>
          <a href="Product2/60843da1a7d189003b4de500" className="w-43 mt-3 mb-4" style={{ width: "43%" }} className="media">

            <img style={{ border: "solid 1px #eeeeee" }} className="w-100 mr-1" src="https://firebasestorage.googleapis.com/v0/b/capstone2-24fd3.appspot.com/o/product%2Fvegetables%2Ffruit-tao.png?alt=media&token=803c325f-f72e-4b93-a301-1e1aa20a62b1" alt="..." />
            <div className="media-body MenuColumn mb-4">
              <h6 className="mt-0" style={{ width: "159px", color: " #666666", fontWeight: "400" }}>Táo Mỹ</h6>
              <small>LotteMart</small>
              <p style={{ color: "#08c25e" }}>40,000 VNĐ</p>
            </div>
          </a>
        </div>
        <div>
          <h6 className="text-center mt-3 mb-3 MenuColumn"><a href="/KhuyenMai" className="text-center mt-3 mb-3 MenuColumn">Sản Phẩm Khuyến Mãi</a></h6>
          <a href="Product2/609a2078a32e59003bcf187f" className="w-43 mt-3 mb-4 border-bottom" style={{ width: "43%" }} className="media">

            <img style={{ border: "solid 1px #eeeeee" }} className="w-100 mr-1" src="https://firebasestorage.googleapis.com/v0/b/capstone2-24fd3.appspot.com/o/product%2Fmeat%2FTh%C4%83n_%C4%90%C3%B9i_B%C3%B2-removebg-preview.png?alt=media&token=17398124-ac52-49d1-ab79-eefca71cbc7d" alt="..." />
            <div className="media-body MenuColumn mb-4">
              <h6 className="mt-0" style={{ width: "159px", color: " #666666", fontWeight: "400" }}>Thăn Đuồi Bò <br></br>
                <span className="text-left m-2 bg-orange"> 30%</span>
              </h6>
              <small>Annam Gourmet</small>
              <p style={{ color: "#08c25e", fontWeight: "700", fontSize: "13px" }}> <strike><small style={{ color: "#888" }}>207,000VNĐ</small> </strike> 154.000 VNĐ</p>
            </div>
          </a>

          <a href="Product2/608028c2b18f8b003bfc69fe" className="w-43 mt-3 mb-4 border-bottom" style={{ width: "43%" }} className="media">

            <img style={{ border: "solid 1px #eeeeee" }} className="w-100 mr-1" src="https://firebasestorage.googleapis.com/v0/b/capstone2-24fd3.appspot.com/o/product%2Fmeat%2Fchanheo-removebg-preview.png?alt=media&token=289cfbe8-3879-4339-86ee-eeb3326457e7" alt="..." />
            <div className="media-body MenuColumn mb-4">
              <h6 className="mt-0" style={{ width: "159px", color: " #666666", fontWeight: "400" }}>Chân Giò Heo <br></br>
                <span className="text-left m-2 bg-orange"> 20%</span>
              </h6>
              <small>Tops Market</small>
              <p style={{ color: "#08c25e", fontWeight: "700", fontSize: "13px" }}> <strike><small style={{ color: "#888" }}>147,000VNĐ</small> </strike> 117.600 VNĐ</p>
            </div>
          </a>
          <a href="Product2/60803d38a7d189003b4de4a4" className="w-43 mt-3 mb-4 border-bottom" style={{ width: "43%" }} className="media">

            <img style={{ border: "solid 1px #eeeeee" }} className="w-100 mr-1" src="https://firebasestorage.googleapis.com/v0/b/capstone2-24fd3.appspot.com/o/product%2Fmeat%2Fthanngoaibo-removebg-preview.png?alt=media&token=01255d94-8d0d-4943-8fcd-c4c90f3c709c" alt="..." />
            <div className="media-body MenuColumn mb-4">
              <h6 className="mt-0" style={{ width: "159px", color: " #666666", fontWeight: "400" }}>Thăn Ngoại Bò <br></br>
                <span className="text-left m-2 bg-orange"> 30%</span>
              </h6>
              <small>Cho Pho Fresh Food</small>
              <p style={{ color: "#08c25e", fontWeight: "700", fontSize: "13px" }}> <strike><small style={{ color: "#888" }}>199,500VNĐ</small> </strike> 139,650VNĐ</p>
            </div>
          </a>
        </div>

      </ul>
    </div>
  );
}

export default MenuLeftOfUser;