import React from 'react';
import {
  BrowserRouter as Router,
   NavLink
} from "react-router-dom";

export default function MenuTow() {
  return (
    <div className="sticky-top" style={{ padding: '0px', margin: '0px' }}>
      <section className="menu container-fluid ">
        <nav style={{ backgroundColor: 'white' ,boxShadow:'0 1px 8px #555555' }} id="menu" className="navbar navbar-expand-lg navbar-light bg-light text-center ">
          <NavLink className="MenuRow" to="/"> <img width="70" height="70" src="https://scontent-hkg4-2.xx.fbcdn.net/v/t1.15752-9/173230510_1216984905426913_7821080656904254425_n.png?_nc_cat=111&ccb=1-3&_nc_sid=ae9488&_nc_ohc=stKGIs7WU2sAX8IDJvX&_nc_ht=scontent-hkg4-2.xx&oh=d1644442a2ed99440622c9a0ce459a59&oe=60CDBDA5"></img> </NavLink>
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
            <span style={{ color: 'white' }} className="navbar-toggler-icon" />
          </button>
          <div className="collapse navbar-collapse pr-3" id="navbarText">
            <ul className="navbar-nav mr-auto justify-content-center">
              <li className="nav-item active">
                <NavLink className="nav-link js-scroll-trigger MenuRow1" to="/">TRANG CHỦ</NavLink>

              </li>
              <li className="nav-item">
                <NavLink className="nav-link js-scroll-trigger MenuRow" to="/Introduce">GIỚI THIỆU</NavLink>
              </li>
              <li className="nav-item dropdown">
                <NavLink className="nav-link js-scroll-trigger MenuRow" to="/Industry">NGÀNH HÀNG</NavLink>
              </li>
              <li className="nav-item ml-2 mr-2">
                <NavLink className="nav-link js-scroll-trigger MenuRow" to="/KhuyenMai">KHUYẾN MÃI</NavLink>
              </li>
            </ul>
            <span className="navbar-text pr-1">
            <input type="search"
                  className="form-control search " name="" id="" aria-describedby="helpId" placeholder="Search"/>
            </span>
            <span className="navbar-text pr-1">

              <NavLink className="nav-link cartMenu" to="/Giohang"><i className="fa fa-cart-arrow-down" aria-hidden="true"></i></NavLink>
            </span>
            <span className="navbar-text pr-3">
                <div className="dropdown open">
                    <button className="btn dropdown-toggle" type="button" id="triggerId" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                            <img className='mr-4 avt' width='50px' alt="Generic placeholder image" src="https://png.pngtree.com/png-vector/20190625/ourlarge/pngtree-business-male-user-avatar-vector-png-image_1511454.jpg" />
                            </button>
                    <div className="dropdown-menu dropdown-menu-right p-3" aria-labelledby="triggerId">
                        <NavLink className="dropdown-item mb-2 MenuRow" to="/InformationUser">Thông Tin Cá Nhân</NavLink>
                        <NavLink className="dropdown-item  MenuRow" to="/ResetPassword">Đổi MẩT Khẩu</NavLink>
                        <NavLink className="dropdown-item  MenuRow" to="/RegisterStore">Đăng Kí Chủ Cửa Hàng</NavLink>

                    </div>
                </div>
            </span>
            
          </div>
        </nav>

      </section>
    </div >




  )

}
