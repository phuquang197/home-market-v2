import React from 'react';

function SlideSale(props) {
    return (
        <div>
            <div className=" pr-0">
                <div id="carouselExampleControls" className="carousel slide" data-bs-ride="carousel">
                    <div className="carousel-inner">
                        <div className="carousel-item active">
                            <img src="https://dl.airtable.com/.attachments/470b99ae8a22a94a7b7f4b6f6ebadc6d/be65fde8/FS-vi2x.jpg" className="d-block w-100" alt="..." />
                        </div>
                        <div className="carousel-item">
                            <img src="https://dl.airtable.com/.attachments/20e7c5182de672999d587dc48c9c3ee5/b3ce7bd7/FS-vi2x-.jpg" className="d-block w-100" alt="..." />
                        </div>
                        <div className="carousel-item">
                            <img src="https://dl.airtable.com/.attachments/4e17a50e334f3e19b84a612681c66eb0/a052cbf7/FS-Vi2x.jpg" className="d-block w-100" alt="..." />
                        </div>
                    </div>
                    <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
                        <span className="carousel-control-prev-icon" aria-hidden="true" />
                        <span className="visually-hidden">Previous</span>
                    </button>
                    <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
                        <span className="carousel-control-next-icon" aria-hidden="true" />
                        <span className="visually-hidden">Next</span>
                    </button>
                </div>


            </div>
        </div>
    );
}

export default SlideSale;