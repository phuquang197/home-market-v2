
import React from 'react';
function NotificaUsers(props) {

    const { notication } = props;
    return (
        <div>
        {/* {user.userName} */}
        <a className="dropdown-item mb-2 MenuRow Notifi" style={{color:'green'}} href={`/NotificationsOfUser/${notication._id}`}>{notication.description}</a>
        <div className="dropdown-divider"></div>
        </div>
    );
}

export default NotificaUsers;