import CardDemo from '../component/user/combined/CardDemo';
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import Loadpage from "../component/Loadpage";
import { API } from "../config/ConfigENV";
import { makeid } from "../helpers/create/create_key_index";
import CardMuaChung from './user/combined/CardMuaChung';
export default function Sphammuachung(props) {
  const [productsPurchasedCollectively, setProductsPurchasedCollectively] = useState();
  const { product } = props;
  const fetchData = async () => {
    const result = await axios.get(`${API}/products?tag=${product.tag}&limit=4`, {
    })

    return result.data
  }

  useEffect(() => {
    fetchData().then(data => {
        setProductsPurchasedCollectively(data)
    })
  }, [])


  if (!productsPurchasedCollectively || productsPurchasedCollectively.lengh === 0) {
    return <Loadpage></Loadpage>
  }

  return (
    <div>
        
        <div className="container ">
         
        
          <div className="cardDemo">
            <div className="row mt-3 pl-3">
              {
                productsPurchasedCollectively.list.map((product) => {
                  return <CardMuaChung key={makeid(10)} product={product}></CardMuaChung>
                })
              }
            </div>
          </div>
          <div className="text-center mb-4 mt-3">
            <button type="button" className="btn btn-outline-success">Xem thêm</button>
          </div>
        </div>

    </div>
  )
}

