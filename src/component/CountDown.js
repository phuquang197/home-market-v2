

import React, { useEffect, useState } from "react";
import { makeid } from "../helpers/create/create_key_index";

function CountDown() {
    const calculateTimeLeft = () => {
        let year = new Date().getFullYear();
        const difference = +new Date(`10/01/${year}`) - +new Date();
        let timeLeft = {};
        if (difference > 0) {
            timeLeft = {
                days: Math.floor(difference / (10000 * 60 * 60 * 24)),
                hours: Math.floor((difference / (1000 * 60 * 60)) % 24),
                minutes: Math.floor((difference / 1000 / 60) % 60),
                seconds: Math.floor((difference / 1000) % 60)
            };
        }
        return timeLeft;
    }
    const [timeLeft, setTimeLeft] = useState(calculateTimeLeft());
    const [year] = useState(new Date().getFullYear());
    useEffect(() => {
        const timer = setTimeout(() => {
            setTimeLeft(calculateTimeLeft());
        }, 1000);
        // Clear timeout if the component is unmounted
        return () => clearTimeout(timer);
    });
    const timerComponents = [];

    Object.keys(timeLeft).forEach((interval) => {
        if (!timeLeft[interval]) {
            return;
        }

        timerComponents.push(
            <span key={makeid(10)} className="countDouwn pt-3" style={{ display:'inline-block'}}>
                {timeLeft[interval]} <br></br> <div className="title-countdown pt-3">{interval}{" "}</div> <br></br>
            </span>
        );
    });

    return (
        <div className="saleTime">
            
            {timerComponents.length ? timerComponents : <span></span>}
        </div>
    );
}

export default CountDown;