import React from 'react';
import {
    NavLink
  } from "react-router-dom";
import NumberFormat from 'react-number-format';
export default  function Card(props) {
    const { oneProduct } = props
    
     return  (
            <div className="col-md-3 mb-4">
                <div className="card product">
                    <NavLink style={{color:'#747487'}} to={`/Product2/${oneProduct._id}`}  >
                    <div className="khoi">
                        <div className="layer1">
                            <img className="card-img-top mt-4" src={oneProduct.photos} alt="" />
                        </div>
                        <div style={{ width:'100%' }} className="layer2">
                            <div className="text-right">
                            <i className="fa fa-heart-o p-2 t"></i>
                            </div>
                            
                        </div>
                        <div className="card-body text-left ml-2">
                            <small className="sieuthi mb-3"> {oneProduct.storeOwnerID.name}</small> <br></br>
                            
                            <div className="text-left">
                            <div className="text-left card-title tensp" to="/Product">{oneProduct.name}</div>
                            <small className="trongluong">{oneProduct.nextWeight}</small>
                            {/* <p className="gia mt-1">{oneProduct.price} đ</p> */}
                            <p  className=" gia mt-1"><NumberFormat value={oneProduct.price} displayType={'text'} thousandSeparator={true}/> VNĐ</p>

                            </div>
                        </div>
                    </div>
                    </NavLink>
                </div>
            </div>

    )
}

