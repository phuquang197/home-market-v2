import React from "react";

import NumberFormat from 'react-number-format';

function CardDemo(props) {
  const { product } = props;
  
  return (

          <div className="col-md-3  p-0">
            <a className="card-title tensp" href={ `/Product2/${product._id}`} >
            <div className="card product">
              <div className="khoi"  style={{  border: "1px #eaeaea solid" }}>
                <div style={{ border: "none"}} className="layer1">
                  <img className="card-img-top mt-4"
                    src={product.photos}
                    alt=""
                  />
                </div>
                <div className="layer2">
                  <div className="text-right">
                    <i className="fa fa-heart-o p-2 t"></i>
                  </div>
                  
                </div>
                <div className="card-body text-left ml-2">
                  <small className="sieuthi mb-3">{product.storeOwnerID.name}</small> <br></br>

                  <div className="text-center">
                    <div style={{height:"45px"}}>

                        {product.name}


                    </div>
                    <small className="trongluong"> {product.nextWeight}</small>
                    {/* <p className="gia mt-1">{product.price} đ</p> */}
                    <p  className=" gia mt-1"><NumberFormat value={product.price} displayType={'text'} thousandSeparator={true}/> VNĐ</p>

                  </div>
                </div>
              </div>
            </div>
            </a>
          </div>

  );
}

export default CardDemo;
