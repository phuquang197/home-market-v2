import React from "react";
import NumberFormat from 'react-number-format';
import { NavLink } from "react-router-dom";

CardDemo2.propTypes = {};

function CardDemo2(props) {
  const { product } = props;
  
  return (

          <div className="col-md-3  p-0">
            <NavLink   className="card-title tensp" to={`/Product2/${product._id}`} >
            <div className="card product">
              <div className="khoi"  style={{  border: "1px #eaeaea solid" }}>
                <div style={{ border: "none"}} className="layer1">
                <div className="m-2 bg-orange">
                  {product.percentDiscount}%
                  </div>
                  <img 
                    className="card-img-top"
                    src={product.photos}
                    alt=""
                  />
                </div>
                <div  className="layer2 text-right">
                  
                  <div className="text-right">
                    <i className="fa fa-heart-o p-2 "></i>
                  </div>
           
                 
                </div>
                <div className="card-body text-left ml-2">
                  <small className="sieuthi mb-3">{product.storeOwnerID.name}</small> <br></br>

                  <div className="text-left">
                    <div style={{height:"45px"}}>

                        {product.name}


                    </div>
                    <small className="trongluong"> {product.nextWeight}</small>
                   <div className="price">
                   <p style={{display:"inline-block", color:"#747487",fontSize:"12px"}} className=" gia mt-1 mr-2"><strike><NumberFormat value={product.price} displayType={'text'} thousandSeparator={true}/> VNĐ </strike></p><br></br>
                   <p style={{display:"inline-block"}} className=" gia mt-1"><NumberFormat value={((product.price)-(product.percentDiscount)/100*(product.price))} displayType={'text'} thousandSeparator={true}/> VNĐ</p>

                   </div>
                    {/*  */}
                  </div>
                </div>
              </div>
            </div>
            </NavLink>
          </div>

  );
}

export default CardDemo2;
