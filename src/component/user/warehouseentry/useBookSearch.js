import { useEffect, useState } from 'react'
import axios from 'axios'
import { API } from "../../../config/ConfigENV";

export default function useBookSearch(query , Gia,  nganhHang , pageNumber) {
  const [loading, setLoading] = useState(true)
  const [error, setError] = useState(false)
  const [books, setBooks] = useState([])
  const [tag, setTag] = useState()
  const [price, setPrice] = useState()
  const [hasMore, setHasMore] = useState(false)
  useEffect(() => {
    setBooks([])
  }, [query,nganhHang,Gia])

  useEffect(() => {
    if(nganhHang){
      setTag({
        tag:nganhHang
      })
    }else{
      setTag({
      })
    }
  }, [nganhHang])

  useEffect(() => {
    if(Gia){
      setPrice(Gia)
    }else{
      setPrice({})
    }
  }, [Gia])

  useEffect(() => {
    setLoading(true)
    setError(false)
    let cancel
    let params = { name: query, limit: 8 , offset: pageNumber, ...tag , ...price }
      axios({
        method: 'GET',
        url: `${API}/products`,
        params
      }).then(res => {
        setBooks(prevBooks => {
          return [...new Set([...prevBooks, ...res.data.list.map(b => b)])]
        })
        setHasMore(res.data.list.length > 0)
        setLoading(false)
     
    }).catch(e => {
      if (axios.isCancel(e)) return
      setError(true)
    })
    return
  }, [tag,price, query, pageNumber])

  return { loading, error, books, hasMore }
}

  // axios({
  //       method: 'GET',
  //       url: 'http://homemarket-hm.us-3.evennode.com/products',
  //       params: { name: query, limit: 8 , offset: pageNumber },
  //       cancelToken: new axios.CancelToken(c => cancel = c)
  //     }).then(res => {
  //       setBooks(prevBooks => {
  //         return [...new Set([...prevBooks, ...res.data.list.map(b => b.name)])]
  //       })
  //       console.log(query);
  //       console.log(res.data);
  //       setHasMore(res.data.docs.length > 0)
  //       setLoading(false)