import Card from "./combined/Card";
import { makeid } from "../../helpers/create/create_key_index";
import { NavLink } from "react-router-dom";
import NumberFormat from "react-number-format";
import React, { useState, useRef, useCallback } from "react";
import { getTagToVN, getPriceSort} from "../../helpers/getTagProduct";
import useBookSearch from "./warehouseentry/useBookSearch";

// import './App.css';
export default function Spbanchay(props) {
  const { product } = props;

  const [query, setQuery] = useState("");
  const [tag, setTag] = useState("");
  const [price, setPrice] = useState("");
  const [pageNumber, setPageNumber] = useState(0);

  const { books, hasMore, loading, error } = useBookSearch(query,price, tag, pageNumber);

  const observer = useRef();

  const lastBookElementRef = useCallback(
    (node) => {
      if (loading) return;
      if (observer.current) observer.current.disconnect();
      observer.current = new IntersectionObserver((entries) => {
        if (entries[0].isIntersecting && hasMore) {
          setPageNumber((prevPageNumber) => prevPageNumber + 8);
        }
      });
      if (node) observer.current.observe(node);
    },
    [loading, hasMore]
  );

  function handleSearch(e) {
    setQuery(e.target.value);
    setPageNumber(0);
  }
  function handleFilter(e) {
    if(e.target.value === 'Chọn...'){
      setTag();
    }else{
      setTag(getTagToVN(e.target.value));
      setPageNumber(0);
    }

  }

  function handleFilterPrice(e) {
    if(e.target.value === 'Chọn...'){
      setPrice();
    }else{
      setPrice(getPriceSort(e.target.value));
      setPageNumber(0);
    }
  }

  return (
    <div>
      <section className="spbanchay mb-5 ">
        <div className="container pb-4">
          <h4
            style={{ fontFamily: '"Oswald", sans-serif', fontWeight: "bold" }}
            className="mb-4 pt-4 text-center"
          >
            SẢN PHẨM BÁN CHẠY
          </h4>
          {/* <div className="input-group mb-3">
  <div className="input-group-prepend">
    <span className="input-group-text" id="basic-addon1">@</span>
  </div>
  <input type="text" className="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1" />
</div> */}

          <div className="row">
            <div className="input-group mb-3">
            <input
              className="inputSearch"
              type="text"
              defaultValue={query}
              onChange={handleSearch}
              placeholder="Tìm Kiếm"
            />

            {/* <div style={{ height: "10px" }}> </div> */}
            <div className="input-group-prepend">
              {/* <div className="col-auto my-1" > */}
      <label className="" htmlFor="inlineFormCustomSelect" >
      <select style={{borderRadius: "0", border: "solid 2px #eeeeee"}} className="custom-select mr-sm-2" id="inlineFormCustomSelect" onChange={handleFilter}>
        <option defaultValue="Chọn...">Ngành hàng</option>
        <option defaultValue="1">Trái Cây</option>
        <option defaultValue="2">Đồ Uống</option>
        <option defaultValue="2">Hải sản</option>
        <option defaultValue="3">Sản phẩm hữ cơ</option>
        <option defaultValue="3">Đồ Đông Lạnh</option>
        <option defaultValue="3">Thịt</option>
        <option defaultValue="3">Bánh</option>
        <option defaultValue="1">Trứng Sữa</option>
      </select>
      </label>
    {/* </div> */}
    {/* <div className="col-auto my-1"> */}
      <label className="mr-sm-2" htmlFor="inlineFormCustomSelect">
      <select  style={{borderRadius: "0",border: "solid 3px #eeeeee"}} className="custom-select mr-sm-2" id="inlineFormCustomSelect2" onChange={handleFilterPrice}>
        <option defaultValue="Chọn...">Giá</option>
        <option defaultValue="1">Tăng dần</option>
        <option defaultValue="2">Giảm dần</option>
      </select>
      </label>
    {/* </div> */}
              
            </div>
            </div>
            {books.map((book, index) => {
              if (books.length === index + 1) {
                return (
                  <div
                    key={makeid(10)}
                    ref={lastBookElementRef}
                    className="col-md-3 mb-4"
                  >
                    <div className="card product">
                      <NavLink
                        style={{ color: "#747487" }}
                        to={`/Product2/${book._id}`}
                      >
                        <div className="khoi">
                          <div className="layer1">
                            <img
                              className="card-img-top mt-4"
                              src={book.photos}
                              alt=""
                            />
                          </div>
                          <div style={{ width: "100%" }} className="layer2">
                            <div className="text-right">
                              <i className="fa fa-heart-o p-2 t"></i>
                            </div>
                            {/* <div className="text-center">
                              <div className="icon-card-plus text-center">
                                <i
                                  className="fa fa-plus pt-3"
                                  aria-hidden="true"
                                />
                              </div>
                            </div> */}
                          </div>
                          <div className="card-body text-left ml-2">
                            <small className="sieuthi mb-3">
                              {" "}
                              {book.storeOwnerID.name}
                            </small>{" "}
                            <br></br>
                            <div className="text-left">
                              <div className="text-left card-title tensp m-0 p-0" to="/Product">
                                {book.name}
                              </div>
                              <small className="trongluong">
                                {book.nextWeight}
                              </small>
                              {/* <p className="gia mt-1">{book.price} đ</p> */}
                              <p className=" gia mt-1">
                                <NumberFormat
                                  defaultValue={book.price}
                                  displayType={"text"}
                                  thousandSeparator={true}
                                />{" "}
                                VNĐ
                              </p>
                            </div>
                          </div>
                        </div>
                      </NavLink>
                    </div>
                  </div>
                );
              } else {
                return (
                  <div key={makeid(10)} className="col-md-3 mb-4">
                    <div className="card product">
                      <NavLink
                        style={{ color: "#747487" }}
                        to={`/Product2/${book._id}`}
                      >
                        <div className="khoi">
                          <div className="layer1">
                            <img
                              className="card-img-top mt-4"
                              src={book.photos}
                              alt=""
                            />
                          </div>
                          <div style={{ width: "100%" }} className="layer2">
                            <div className="text-right">
                              <i className="fa fa-heart-o p-2 t"></i>
                            </div>
                            <div className="text-left">
                              {/* <div className="icon-card-plus text-center">
                                <i
                                  className="fa fa-plus pt-3"
                                  aria-hidden="true"
                                />
                              </div> */}
                            </div>
                          </div>
                          <div className="card-body text-left ml-2">
                            <small className="sieuthi mb-3">
                              {" "}
                              {book.storeOwnerID.name}
                            </small>{" "}
                            <br></br>
                            <div className="text-left">
                              <div className="card-title tensp p-0 m-0" to="/Product">
                                {book.name}
                              </div>
                              <small className="trongluong">
                                {book.nextWeight}
                              </small>
                              {/* <p className="gia mt-1">{book.price} đ</p> */}
                              <p className=" gia mt-1">
                                <NumberFormat
                                  defaultValue={book.price}
                                  displayType={"text"}
                                  thousandSeparator={true}
                                />{" "}
                                VNĐ
                              </p>
                            </div>
                          </div>
                        </div>
                      </NavLink>
                    </div>
                  </div>
                );
              }
            })}
            <div>{loading && "Loading..."}</div>
            <div>{error && "Error"}</div>
          </div>
          {/* <div className="text-center mt-4 mb-4">
            <button type="button" className="btn btn-outline-success">
              Xem thêm
            </button>
          </div> */}
        </div>
      </section>
    </div>
  );
}
