import React,{useContext, useState} from 'react';
import { storage } from "../firebase/Firebase"
import {context} from '../ContextApi/Provide'
import { makeid } from "../helpers/create/create_key_index";
function InputFile(props) {

    const [file, setFile] = useState(null);
    const [url, setUrl] = useState("");
    let consumer = useContext(context);
    function handleChange(e) {
        setFile(e.target.files[0]);
    }

    function handleUpload(e) {
        e.preventDefault();
        const uploadTask = storage.ref(`/images/${makeid(10)}-${file.name}`).put(file);
        uploadTask.then((res) => {

            storage.ref("images").child(res.ref.name).getDownloadURL()
            .then((url) => {
                consumer.setLinkImage(url);
                setUrl(url);
              })
        })

    }

 
    return (
        <div className="">
            <form onSubmit={handleUpload}>
        <input type="file" onChange={handleChange} />
        <button  disabled={!file} >upload to firebase</button>
      </form>
        </div>
    );
}

export default InputFile;

