import React from 'react';
import {NavLink} from "react-router-dom";
// import MenuLeftOfUser from './MenuLeftOfUser';
function BannerPrd() {
    return (

        <div className="row">
            <div style={{ boxShadow: ' 0 5px 0 rgb(200 200 200 / 20%)', backgroundColor: "white" }} className="">
                <ul className="nav flex-column">
                <h6 style={{color:"#333333", fontWeight:"500"}} className="pl-4 pt-4"> NGÀNH HÀNG</h6>
             
              <ul style={{ borderBottom: "1px solid #f0f0f0" }} className="list-group list-group-flush mb-2">
                <li className="list-group-item"><NavLink className="MenuColumn" to="/Beat">Thịt</NavLink></li>
                <li className="list-group-item"><NavLink className="MenuColumn" to="/Seafood">Hải Sản</NavLink></li>
                <li className="list-group-item"><NavLink className="MenuColumn" to="/Drinks">Nước Uống</NavLink></li>
                <li className="list-group-item"><NavLink className="MenuColumn" to="/MilkEgg">Trứng, Sữa</NavLink></li>
                <li className="list-group-item"><NavLink className="MenuColumn" to="/Frozenfood">Đồ Đông Lạnh</NavLink></li>
                <li className="list-group-item"><NavLink className="MenuColumn" to="/Fruit">Trái Cây</NavLink></li>
                <li className="list-group-item"><NavLink className="MenuColumn" to="/Bread">Bánh Mì</NavLink></li>
                <li className="list-group-item"><NavLink className="MenuColumn" to="/Vegartable">Rau, Củ</NavLink></li>
              </ul>
              <img className="w-100" src="https://demo.templatetrip.com/Opencart/OPC02/OPC040/OPC05/image/cache/catalog/demo/banners/left-banner-1-274x500.jpg"></img>
                {/* <SlideSale></SlideSale> */}
              


              <div style={{ borderBottom: "solid 1px #666666" }}>
                <h5 className="text-center mt-3 mb-3 MenuColumn">Sản Phẩm Nổi Bật</h5>
                <a href="Product2/609a2078a32e59003bcf187f" className="w-43 mt-3 mb-4 border-bottom" style={{ width: "43%" }} className="media">

                  <img style={{ border: "solid 1px #eeeeee" }} className="w-100 mr-1" src="https://firebasestorage.googleapis.com/v0/b/capstone2-24fd3.appspot.com/o/product%2Fmeat%2Fphi_l%C3%AA_b%C3%B2-removebg-preview.png?alt=media&token=48937fbe-02bb-4463-93b4-f3d88c3d1525" alt="..." />
                  <div className="media-body MenuColumn mb-4">
                    <h6 className="mt-0" style={{ width: "159px", color: " #666666", fontWeight: "400" }}>Phi Lê Bò</h6>
                    <small>LotteMart</small>
                    <p style={{ color: "#08c25e" }}>207.000 VNĐ</p>
                  </div>
                </a>

                <a href="Product2/609a2078a32e59003bcf187f" className="w-43 mt-3 mb-4" style={{ width: "43%" }} className="media">

                  <img style={{ border: "solid 1px #eeeeee" }} className="w-100 mr-1" src="https://firebasestorage.googleapis.com/v0/b/capstone2-24fd3.appspot.com/o/product%2Fmeat%2Fphi_l%C3%AA_b%C3%B2-removebg-preview.png?alt=media&token=48937fbe-02bb-4463-93b4-f3d88c3d1525" alt="..." />
                  <div className="media-body MenuColumn mb-4">
                    <h6 className="mt-0" style={{ width: "159px", color: " #666666", fontWeight: "400" }}>Phi Lê Bò</h6>
                    <small>LotteMart</small>
                    <p style={{ color: "#08c25e" }}>207.000 VNĐ</p>
                  </div>
                </a>
                <a href="Product2/609a2078a32e59003bcf187f" className="w-43 mt-3 mb-5" style={{ width: "43%" }} className="media">

                  <img style={{ border: "solid 1px #eeeeee" }} className="w-100 mr-1" src="https://firebasestorage.googleapis.com/v0/b/capstone2-24fd3.appspot.com/o/product%2Fmeat%2Fphi_l%C3%AA_b%C3%B2-removebg-preview.png?alt=media&token=48937fbe-02bb-4463-93b4-f3d88c3d1525" alt="..." />
                  <div className="media-body MenuColumn mb-4 mb-4">
                    <h6 className="mt-0" style={{ width: "159px", color: " #666666", fontWeight: "400" }}>Phi Lê Bò</h6>
                    <small>LotteMart</small>
                    <p style={{ color: "#08c25e" }}>207.000 VNĐ</p>
                  </div>
                </a>
                <a href="Product2/609a2078a32e59003bcf187f" className="w-43 mt-3 mb-4" style={{ width: "43%" }} className="media">

                  <img style={{ border: "solid 1px #eeeeee" }} className="w-100 mr-1" src="https://firebasestorage.googleapis.com/v0/b/capstone2-24fd3.appspot.com/o/product%2Fmeat%2Fphi_l%C3%AA_b%C3%B2-removebg-preview.png?alt=media&token=48937fbe-02bb-4463-93b4-f3d88c3d1525" alt="..." />
                  <div className="media-body MenuColumn mb-4">
                    <h6 className="mt-0" style={{ width: "159px", color: " #666666", fontWeight: "400" }}>Phi Lê Bò</h6>
                    <small>LotteMart</small>
                    <p style={{ color: "#08c25e" }}>207.000 VNĐ</p>
                  </div>
                </a>
                <a href="Product2/609a2078a32e59003bcf187f" className="w-43 mt-3 mb-4" style={{ width: "43%" }} className="media">

                  <img style={{ border: "solid 1px #eeeeee" }} className="w-100 mr-1" src="https://firebasestorage.googleapis.com/v0/b/capstone2-24fd3.appspot.com/o/product%2Fmeat%2Fphi_l%C3%AA_b%C3%B2-removebg-preview.png?alt=media&token=48937fbe-02bb-4463-93b4-f3d88c3d1525" alt="..." />
                  <div className="media-body MenuColumn mb-4">
                    <h6 className="mt-0" style={{ width: "159px", color: " #666666", fontWeight: "400" }}>Phi Lê Bò</h6>
                    <small>LotteMart</small>
                    <p style={{ color: "#08c25e" }}>207.000 VNĐ</p>
                  </div>
                </a>
              </div>
              <div>
                <h6 className="text-center mt-3 mb-3 MenuColumn"><a href="/KhuyenMai" className="text-center mt-3 mb-3 MenuColumn">Sản Phẩm Khuyến Mãi</a></h6>
                <a href="Product2/609a2078a32e59003bcf187f" className="w-43 mt-3 mb-4 border-bottom" style={{ width: "43%" }} className="media">

                  <img style={{ border: "solid 1px #eeeeee" }} className="w-100 mr-1" src="https://firebasestorage.googleapis.com/v0/b/capstone2-24fd3.appspot.com/o/product%2Fmeat%2FTh%C4%83n_%C4%90%C3%B9i_B%C3%B2-removebg-preview.png?alt=media&token=17398124-ac52-49d1-ab79-eefca71cbc7d" alt="..." />
                  <div className="media-body MenuColumn mb-4">
                    <h6 className="mt-0" style={{ width: "159px", color: " #666666", fontWeight: "400" }}>Thăn Đuồi Bò <br></br>
                  <span className="text-left m-2 bg-orange"> 30%</span>
                    </h6>
                    <small>Annam Gourmet</small>
                    <p style={{ color: "#08c25e", fontWeight: "700", fontSize: "13px" }}> <strike><small style={{ color: "#888" }}>207,000VNĐ</small> </strike> 154.000 VNĐ</p>
                  </div>
                </a>

                <a href="Product2/608028c2b18f8b003bfc69fe" className="w-43 mt-3 mb-4 border-bottom" style={{ width: "43%" }} className="media">

                  <img style={{ border: "solid 1px #eeeeee" }} className="w-100 mr-1" src="https://firebasestorage.googleapis.com/v0/b/capstone2-24fd3.appspot.com/o/product%2Fmeat%2Fchanheo-removebg-preview.png?alt=media&token=289cfbe8-3879-4339-86ee-eeb3326457e7" alt="..." />
                  <div className="media-body MenuColumn mb-4">
                    <h6 className="mt-0" style={{ width: "159px", color: " #666666", fontWeight: "400" }}>Chân Giò Heo <br></br>
                  <span className="text-left m-2 bg-orange"> 20%</span>
                    </h6>
                    <small>Tops Market</small>
                    <p style={{ color: "#08c25e", fontWeight: "700", fontSize: "13px" }}> <strike><small style={{ color: "#888" }}>147,000VNĐ</small> </strike> 117.600 VNĐ</p>
                  </div>
                </a>    
                <a href="Product2/60803d38a7d189003b4de4a4" className="w-43 mt-3 mb-4 border-bottom" style={{ width: "43%" }} className="media">

                  <img style={{ border: "solid 1px #eeeeee" }} className="w-100 mr-1" src="https://firebasestorage.googleapis.com/v0/b/capstone2-24fd3.appspot.com/o/product%2Fmeat%2Fthanngoaibo-removebg-preview.png?alt=media&token=01255d94-8d0d-4943-8fcd-c4c90f3c709c" alt="..." />
                  <div className="media-body MenuColumn mb-4">
                    <h6 className="mt-0" style={{ width: "159px", color: " #666666", fontWeight: "400" }}>Thăn Ngoại Bò <br></br>     
                  <span className="text-left m-2 bg-orange"> 30%</span>
                    </h6>
                    <small>Cho Pho Fresh Food</small>
                    <p style={{ color: "#08c25e", fontWeight: "700", fontSize: "13px" }}> <strike><small style={{ color: "#888" }}>199,500VNĐ</small> </strike> 139,650VNĐ</p>
                  </div>
                </a>
              </div>

            </ul>
            </div>
            
        </div>
    );
}

export default BannerPrd;