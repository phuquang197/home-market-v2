import React from 'react';
import { NavLink } from 'react-router-dom';
export default function Danhmucsp (){
    
        return (
            <div className="danhmucsanpham shadow">
                    <div  className="container ">
                        <div className="card-deck">
                            <div className="card pt-3 text-center">
                            <NavLink className="MenuColumn" to="/Fruit">
                            <img className="card-img-top0" src="https://firebasestorage.googleapis.com/v0/b/capstone2-24fd3.appspot.com/o/product%2FlogoProduct%2Ftraicaylogo.webp?alt=media&token=5bbbb90a-1f9c-4e83-9732-e6f4c6d3302f" alt="" />
                                <div className="card-body">
                                    <h4 className="card-title">Trái Cây</h4>
                                </div>
                            </NavLink>
                            </div>
                          
                            
                            <div className="card pt-3 text-center">
                            <NavLink className="MenuColumn" to="/Drinks">
                            <img className="card-img-top0" src="https://product.hstatic.net/1000119621/product/4902102092951_439e06f37db14ab3bb9215ba28ec8b5f_large.jpg" alt="" />
                                <div className="card-body">
                                    <h4 className="card-title">Nước Uống</h4>
                                </div>
                                </NavLink>
                            </div>
                            
                           
                            <div className="card pt-3 text-center">
                            <NavLink className="MenuColumn" to="/Seafood">
                            <img className="card-img-top0" src="https://firebasestorage.googleapis.com/v0/b/capstone2-24fd3.appspot.com/o/product%2FlogoProduct%2Fc%C3%A1_logo-removebg-preview.png?alt=media&token=cfa7d4f9-b135-4337-bbd4-a2388bf72393" alt="" />
                                <div className="card-body">
                                    <h4 className="card-title">Hải Sản</h4>
                                </div>
                                </NavLink>
                            </div>
                            
                            
                            <div className="card pt-3 text-center">
                            <NavLink className="MenuColumn" to="/Vegartable">
                            <img className="card-img-top0" src="https://firebasestorage.googleapis.com/v0/b/capstone2-24fd3.appspot.com/o/product%2FlogoProduct%2Flogo_rau_cu-removebg-preview.png?alt=media&token=599f7382-bc06-4c92-b212-b8f0a284ece7" alt="" />
                                <div className="card-body">
                                    <h4 className="card-title">Rau Củ</h4>
                                </div>
                                </NavLink>
                            </div>
                            <div className="card pt-3 text-center">
                            <NavLink className="MenuColumn" to="/Frozenfood">
                            <img className="card-img-top0" src="https://firebasestorage.googleapis.com/v0/b/capstone2-24fd3.appspot.com/o/product%2Ffrozen%2Fca-dong-lanh-removebg-preview.png?alt=media&token=345c1975-eb32-4166-b341-f353934a80b0" alt="" />
                                <div className="card-body">
                                    <h4 className="card-title">Đồ Đông Lạnh</h4>
                                </div>
                                </NavLink>
                            </div>
                            
                            <div className="card pt-3 text-center">
                            <NavLink className="MenuColumn" to="/Beat">
                            <img className="card-img-top0" src="https://firebasestorage.googleapis.com/v0/b/capstone2-24fd3.appspot.com/o/product%2FlogoProduct%2Fth%E1%BB%8Bt_logo-removebg-preview.png?alt=media&token=7eadd946-4f1b-439f-b592-31a96e6d78ac" alt="" />
                                <div className="card-body">
                                    <h4 className="card-title">Thịt</h4>
                                </div>
                                </NavLink>
                            </div>
                           
                            
                            <div className="card pt-3 text-center">
                            <NavLink className="MenuColumn" to="/Bread">
                            <img className="card-img-top0" src="https://firebasestorage.googleapis.com/v0/b/capstone2-24fd3.appspot.com/o/product%2FlogoProduct%2Fb%C3%A1nh_logo-removebg-preview.png?alt=media&token=111e933a-a6bf-40f3-aaca-dab57879603f" alt="" />
                                <div className="card-body">
                                    <h4 className="card-title">Bánh</h4>
                                </div>
                                </NavLink>
                            </div>
                           
                           
                            <div className="card pt-3 text-center">
                            <NavLink className="MenuColumn" to="/MilkEgg">
                                <img className="card-img-top0" src="https://firebasestorage.googleapis.com/v0/b/capstone2-24fd3.appspot.com/o/product%2FlogoProduct%2Fs%E1%BB%AFa_logo-removebg-preview.png?alt=media&token=e6be0c65-6e3e-42de-84a3-0b922b3d89ec" alt="" />
                                <div className="card-body">
                                    <h4 className="card-title">Trứng Sữa</h4>
                                </div>
                                </NavLink>
                            </div>
                           
                        </div>
                    </div>

            </div>
        )
    }

    