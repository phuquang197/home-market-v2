
import React from 'react';
function NotificaStore(props) {

    const { notication } = props;
    return (
        <div>
        {/* {user.userName} */}
        <a className="dropdown-item mb-2 MenuRow Notifi" href={`/NotificationsOfStore/${notication._id}`}>{notication.description}</a>
        <div className="dropdown-divider"></div>
        </div>
    );
}

export default NotificaStore;
