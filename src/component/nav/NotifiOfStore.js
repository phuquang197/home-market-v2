
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { API } from "../../config/ConfigENV";
import { makeid } from '../../helpers/create/create_key_index';
// import {
//     useParams
// } from "react-router-dom";
import NotificaStore from '../NotificaStore';
// import {
//     NavLink
// } from "react-router-dom";
// import NotificaUsers from '../NotificaUsers';
function NotifiOfStore() {
    const [notications, setNotifications] = useState();
    // let { userID } = useParams();
    const token = localStorage.getItem("access_token");
    const storeOwnerID = localStorage.getItem("storeOwnerID");
    const fetchData = async () => {
      if(!token){
        return null
      }
      
        const result = await axios({
            method: "get",
            url: `${API}/notications?limit=5&receiverUStoreID=${storeOwnerID}`,
            headers: {
                Authorization: `Bearer ${token}`,
            },
        });

        return result.data
    }
    useEffect(() => {
        fetchData().then(data => {
            setNotifications(data)
        })
    }, [])

    if (!notications || notications.lengh === 0) {
        return ("BẠN KHÔNG CÓ THÔNG BÁO NÀO")
    }
    console.log(notications);
    return (
       <div className="p-3">

        {
            notications.list.map((notication) => {
                return <NotificaStore key={makeid(10)} notication={notication}></NotificaStore>

            })
        }
        <a className="dropdown-item MenuRow text-center" href="/IndexNotifiStore/{}">Xem Thêm</a>

        </div>
    );
}

export default NotifiOfStore;

