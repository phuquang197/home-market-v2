import React from 'react';
import { NavLink } from "react-router-dom";

function DangKiOrGoChuCuaHang(props) {
    const { user } = props;

    if(user.storeOwnerID.status === 'ACCEPTED'){
      return (<div>
        <NavLink className="dropdown-item  MenuRow" to={`/Productstore`}>
        Đi Đến Cửa Hàng
      </NavLink>
    </div>)
    }

    if(user.storeOwnerID.status === 'PENDING'){
      return (<div>
        <span className="dropdown-item  MenuRow" to={`/Productstore`}>
        Đang chờ phê duyệt làm chủ cửa hàng, home market sẽ sớm liên lạc với bạn
      </span>
    </div>)
    }
    return (
        <div>
            <NavLink className="dropdown-item  MenuRow" to={`/RegisterStore`}>
            Đăng kí chủ cửa hàng
          </NavLink>
        </div>
    );
}

export default DangKiOrGoChuCuaHang;