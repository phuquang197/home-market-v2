
import { NavLink } from "react-router-dom";
import Login from "../../pageUser/Login";
import InforUser from "../../pageUser/user/InforUser";
import DangKiOrGoChuCuaHang from './DangKiOrGoChuCuaHang';
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { API } from "../../config/ConfigENV";
// import PaymentHistory from "../../pageUser/PaymentHistory";
export default function Menu(props) {
  const token =  localStorage.getItem('access_token');
  const token_fcm =  localStorage.getItem('token_fcm');

  const logOut = async () => {
    const result1 = await axios({
      method: "post",
      url: `${API}/fcm/unsubscribe`,
      data: {
        token :token_fcm,
      },
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });

    localStorage.removeItem("access_token");

    localStorage.removeItem("userID");
    localStorage.removeItem("storeOwnerID");
    localStorage.removeItem("nameStore");
    localStorage.removeItem("addToCard");
    localStorage.setItem('addToCard','[{"amount":"1111","productID":"111111111111111","price":3570750,"userCreatedProductID":"60802578b18f8b003bfc69f1","name":"noanme","photo":"noPhoto","storeOwnerID":"storeOwnerID"}]');
    localStorage.removeItem("name");

    window.location.reload();
  }

  const userID =  localStorage.getItem('userID');

  const [user, setUser] = useState();

  const fetchData = async () => {

    const result = await axios.get(`${API}/profile/${userID}`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    },
    )
    return result.data
  }

  useEffect(() => {
    if(token) {
      fetchData().then(data => {
        setUser(data)
        localStorage.setItem('nameStore', data.storeOwnerID.name);
      })
    }
  }, [])

  if(token){
    if (!user || user.lengh === 0 ) {
      return ("")
    }

  }
  if (!token) {
    return (
      <span className="navbar-text pr-1">
        <button
          style={{ border: "none" }}
          className="btnlogin p-2"
          data-toggle="modal"
          data-target="#exampleModal"
          data-whatever="@mdo"
        >
          Đăng Nhập
        </button>
        <div
          className="modal fade"
          id="exampleModal"
          tabIndex={-1}
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog text-center">
            <div style={{ width: "890px" }} className="modal-content">
              <div className="modal-header">
                <h5  style={{  color: "#08c25e" }} className="modal-title" id="exampleModalLabel">
                ĐĂNG NHẬP
                </h5>
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div style={{ color: "#08c25e" }} className="container form">
                <div className="row bg-white">
                  <div
                    style={{  color: "#08c25e" }}
                    className="col-md-6 imglogin "
                  >
                    <div className="text-center pt-5">
                      {/* <h1 className="login-title">ĐĂNG NHẬP</h1>
                      <h4 className="login-title mb-5">Chào mừng trở lại!</h4> */}
                      <h6 style={{paddingTop:"350px"}}>
                        Bạn mới sử dụng Home Martket?
                        <a className="MenuRow" href="/Register">Đăng kí miễn phí</a>
                      </h6>
                    </div>
                  </div>
                  <div className="col-md-6 pt-5 pr-5 pl-5">
                    <Login></Login>
                    <form className="signwith pb-4">
                      <div className="row">
                        <div
                          className="col-md-4"
                          style={{ borderBottom: "solid 0.5px silver" }}
                        />
                        <div className="col-md-4 text-center">
                          <small>Hoặc đăng nhập bằng</small>
                        </div>
                        <div
                          className="col-md-4"
                          style={{ borderBottom: "solid 0.5px silver" }}
                        />
                      </div>
                      <div className="d-grid gap-2 d-md-block text-center">
                        <a className="btn fb" href="#" role="button">
                          <div className="icon_fb icon2">
                            <i className="fa fa-facebook" aria-hidden="true" />
                          </div>{" "}
                          <br />
                          <h6>Facebook</h6>
                        </a>
                        <a className="btn gm" href="#" role="button">
                          <div className="icongm icon2">
                            <i className="fa fa-google" aria-hidden="true" />
                          </div>
                          <br />
                          <h6>Gmail</h6>
                        </a>
                      </div>
                      <small className="pdt-md pt-4" id="agree-terms">
                        <font style={{ verticalAlign: "inherit" }}>
                          <font style={{ verticalAlign: "inherit" }}>
                            Bằng cách đăng nhập, tôi đồng ý với{" "}
                          </font>
                        </font>
                        <a className="rule MenuRow" target="_blank" href="/privacy">
                          <font style={{ verticalAlign: "inherit" }}>
                            <font style={{ verticalAlign: "inherit" }}>
                              Chính sách Bảo mật
                            </font>
                          </font>
                        </a>
                        <font style={{ verticalAlign: "inherit" }}>
                          <font style={{ verticalAlign: "inherit" }}> và </font>
                        </font>
                        <a className="rule MenuRow" target="_blank" href="/privacy">
                          <font style={{ verticalAlign: "inherit" }}>
                            <font style={{ verticalAlign: "inherit" }}>
                              Điều Khoản Dịch Vụ
                            </font>
                          </font>
                        </a>
                        <font style={{ verticalAlign: "inherit" }}>
                          <font style={{ verticalAlign: "inherit" }}> .</font>
                        </font>
                      </small>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </span>
    );
  }

  return (
    <span className="navbar-text pr-3">
      <div className="dropdown open">
        <button
          className="btn dropdown-toggle"
          type="button"
          id="triggerId"
          data-toggle="dropdown"
          aria-haspopup="true"
          aria-expanded="false"
        >
          <img
            className="mr-4 avt"
            width="50px"
            alt="Generic placeholder image"
            src={user.profilePicture? user.profilePicture :"https://png.pngtree.com/png-vector/20190625/ourlarge/pngtree-business-male-user-avatar-vector-png-image_1511454.jpg"}
          />
        </button>
        <div
          className="dropdown-menu dropdown-menu-right p-3"
          aria-labelledby="triggerId"
        >
            <InforUser user={user}></InforUser>
            <NavLink className="dropdown-item  MenuRow" to={`/ResetPassword/${user._id}`}>
            Đổi Mật Khẩu
          </NavLink>
          <DangKiOrGoChuCuaHang user={user}></DangKiOrGoChuCuaHang>
          {/* <NavLink className="dropdown-item  MenuRow" to="/RegisterStore">
            Đăng Kí Chủ Cửa Hàng
          </NavLink> */}
          <NavLink className="dropdown-item  MenuRow" to={`/HistoryPaymentOfUser/${user._id}`}>
            Xem Lịch Sử Mua Hàng
          </NavLink>
          {/* <NavLink className="dropdown-item  MenuRow" to={`/LikeProduct/${user._id}`}>
            Xem San Pham Yeu Thich
          </NavLink> */}
          <button  onClick={logOut} className="dropdown-item  MenuRow" >
            Đăng xuất
          </button>

        </div>
      </div>
    </span>
  );
}
