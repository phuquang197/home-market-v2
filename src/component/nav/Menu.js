
import {
  NavLink
} from "react-router-dom";
import Option from './Option';

import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { API } from "../../config/ConfigENV";
import OptionOfNotifi from "./OptionOfNotifi";
import NotificaUsers from "../NotificaUsers";

export default function Menu(props) {

  const addToCard = localStorage.getItem('addToCard');

  if(!addToCard){
    localStorage.setItem('addToCard','[{"amount":"1111","productID":"111111111111111","price":3570750,"userCreatedProductID":"60802578b18f8b003bfc69f1","name":"noanme","photo":"noPhoto","storeOwnerID":"storeOwnerID"}]');
  }
  const token = localStorage.getItem("access_token");

  const [user, setUser] = useState();

const fetchData = async () => {
  if(!token){
    return null
  }

  const result = await axios.get(`${API}/profile`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  },
  )
  return result.data
}

  useEffect(() => {

      fetchData().then(data => {
        setUser(data)
      })
  }, [])

  // console.log(user)
if(token){
  if (!user || user.lengh === 0 ) {
    return <> <h1>.....</h1> </>
  }

  localStorage.setItem('userID', user.resultUser._id);
  localStorage.setItem('storeOwnerID', user.resultUser.storeOwnerID._id);
}
    return (
      <div className="sticky-top" style={{ padding: '0px', margin: '0px',zIndex:'101' }}>
        <section className="menu container-fluid p-0 m-0">
          <nav style={{ backgroundColor: 'white' ,boxShadow:'0 1px 8px #555555' }} id="menu" className="navbar navbar-expand-lg navbar-light bg-light text-center">
          <NavLink className="MenuRow" to="/"> <img width="70" height="70" src="https://scontent-hkg4-2.xx.fbcdn.net/v/t1.15752-9/173230510_1216984905426913_7821080656904254425_n.png?_nc_cat=111&ccb=1-3&_nc_sid=ae9488&_nc_ohc=stKGIs7WU2sAX8IDJvX&_nc_ht=scontent-hkg4-2.xx&oh=d1644442a2ed99440622c9a0ce459a59&oe=60CDBDA5"></img> </NavLink>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
              <span style={{ color: 'white' }} className="navbar-toggler-icon" />
            </button>
            <div className="collapse navbar-collapse pr-3" id="navbarText">
              <ul className="navbar-nav mr-auto justify-content-center">
                <li className="nav-item active">
                  <NavLink className="nav-link js-scroll-trigger MenuRow1" to="/">TRANG CHỦ</NavLink>

                </li>
                <li className="nav-item">
                  <NavLink className="nav-link js-scroll-trigger MenuRow" to="/Introduce">GIỚI THIỆU</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link js-scroll-trigger MenuRow" to="/ManyStore">CỬA HÀNG</NavLink>
                </li>
                <li className="nav-item dropdown">
                  <NavLink className="nav-link js-scroll-trigger MenuRow" to="/Industry">NGÀNH HÀNG</NavLink>
                </li>
                <li className="nav-item ml-2 mr-2">
                  <NavLink className="nav-link js-scroll-trigger MenuRow" to="/KhuyenMai">KHUYẾN MÃI</NavLink>
                </li>
              </ul>
              <span className="navbar-text pr-3">
              <NavLink to="/Search">
              <input type="search"
                    className="form-control search " name="" id="" aria-describedby="helpId" placeholder="Tìm Kiếm"/>
              </NavLink>
              </span>
              <span className="navbar-text">
                        <div className="dropdown open">
                        <span style={{ left: '51px', position: 'relative',top:'-8px' }} className="badge badge-danger badge-counter">3+</span>
                            <button style={{boxShadow:"none"}}
                                className="btn"
                                type="button"
                                id="triggerId"
                                data-toggle="dropdown"
                                aria-haspopup="true"
                                aria-expanded="false"
                            >
                                <i style={{color:"#ed6663", fontSize:"24px"}} className="fa fa-bell-o" aria-hidden="true"></i>
                            </button>

                            <div
                                className="dropdown-menu dropdown-menu-right p-2"
                                aria-labelledby="triggerId"
                            >
                              <OptionOfNotifi ></OptionOfNotifi>

                                {/* <NavLink className="dropdown-item  MenuRow" to="/">
                                    Thông Báo I
                                </NavLink>
                                <NavLink className="dropdown-item  MenuRow" to="/">
                                    Thông Báo II
                                </NavLink>
                                <NavLink className="dropdown-item  MenuRow" to="/">
                                    Thông Báo III
                                </NavLink> */}
                            </div>
                        </div>
                    </span>
              <span className="navbar-text pr-3">

                <NavLink className="nav-link cartMenu" to="/CartOfStore"><i className="fa fa-cart-arrow-down" aria-hidden="true"></i></NavLink>
              </span>
              <Option token={token}></Option>
            </div>
          </nav>

        </section>
      </div >
    )

}
