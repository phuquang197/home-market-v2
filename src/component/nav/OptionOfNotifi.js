
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import Loadpage from "../Loadpage";
import { API } from "../../config/ConfigENV";
import { makeid } from '../../helpers/create/create_key_index';
// import {
//     useParams
// } from "react-router-dom";
import NotificaUsers from '../NotificaUsers';
// import {
//     NavLink
// } from "react-router-dom";
// import NotificaUsers from '../NotificaUsers';
function OptionOfNotifi() {
    const [notications, setNotifications] = useState();
    // let { userID } = useParams();
    const token = localStorage.getItem("access_token");
    const userID = localStorage.getItem("userID");
    const fetchData = async () => {
      if(!token){
        return null
      }
      
        const result = await axios({
            method: "get",
            url: `${API}/notications?limit=5&createdBy=${userID}`,
            headers: {
                Authorization: `Bearer ${token}`,
            },
        });

        return result.data
    }
    useEffect(() => {
        fetchData().then(data => {
            setNotifications(data)
        })
    }, [])

    if (!notications || notications.lengh === 0) {
        return ("BẠN KHÔNG CÓ THÔNG BÁO NÀO")
    }

    return (
       <div className="p-3">

        {
            notications.list.map((notication) => {
                return <NotificaUsers key={makeid(10)} notication={notication}></NotificaUsers>

            })
        }
        <a className="dropdown-item MenuRow text-center" href="/IndexNotifi/{}">Xem Thêm</a>

        </div>
    );
}

export default OptionOfNotifi;