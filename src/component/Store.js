
import React from 'react';
import {
 NavLink
} from "react-router-dom";

export default function Store () { 
        return (
            <div className="mb-5 ">
                <div className="store">
                    <div className="slide pb-4">
                        <div className="container p-4 ">
                            <h3 style={{fontFamily:"inherit"}} className="text-center title mt-5">Các Siêu Thị Liên Kết Với Home Martket</h3>
                            <div className="row ">
                                <div id="carouselExampleSlidesOnly" className="carousel slide" data-ride="carousel">
                                    <div className="carousel-inner">
                                        <div className="carousel-item active">
                                            <p className="comment p-4">"Tops Market là thương hiệu của tập đoàn phân phối bán lẻ Groupe Casino (Pháp) tại Thái
                                            Lan và Việt Nam. Tops Market được thành lập vào năm 1993 và mở cửa hàng đầu tiên của mình tại ngã tư Wong
                                            Sawang, Bangkok, Thái Lan. ... Hiện tại, mỗi siêu thị Tops Market có khoảng hơn 40.000 mặt hàng để đáp ứng
                                            cho nhu cầu của Khách hàng.
                                            "</p>
                                            <img className="logost mt-3 ml-4" src="https://firebasestorage.googleapis.com/v0/b/capstone2-24fd3.appspot.com/o/product%2FlogoProduct%2FSTORE-COVER2x-removebg-preview%20(1).png?alt=media&token=2dfa3510-53c0-447f-a373-dc2af6062c03" alt="" />
                                            <strong >Siêu Thị Tops Market</strong>
                                        </div>
                                        <div className="carousel-item">
                                            <p className="comment p-4">"Lotte Mart luôn luôn ưu tiên phát triển các loại hàng hóa nội địa, 
                                            chủ động hợp tác để đưa các sản phẩm Việt Nam chất lượng cao vào danh mục nhãn hàng riêng Choice. 
                                            Tiên phong đầu tư trong ngành bán lẻ tại Việt Nam từ 2008 với mô hình bán lẻ hiện đại."</p>
                                            <img className="logost mt-3 ml-4" src="https://dl.airtable.com/.attachments/54b438a3d1b6006921c19b6353ac41c9/b33c9e07/lotte-20200418-vi2x.png" alt="" />
                                            <strong >Lotte Mart</strong>
                                        </div>
                                        <div className="carousel-item">
                                            <p className="comment p-4">"Annam Gourmet có trên 20 năm kinh nghiệm trong việc cung cấp những sản phẩm chất 
                                            lượng cao cùng các thương hiệu cao cấp tới người tiêu dùng tại Việt Nam.
                                            "</p>
                                            <img className="logost mt-3 ml-4" src="https://dl.airtable.com/gHI31HCSELYU8QuBRNwQ_AnNam%402x.png" alt="" />
                                            <strong >Annam Gourmet</strong>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
         
                <div className="spkm logostore mb-4 bg-white p-4">
                        <div className="card-deck mt-4 mb-4">
                        <NavLink className="card text-center pt-3" to="/InformationStore/60a66b1b756e5f003b250247">
                                <img className="card-img-top1 W-100%" src="https://dl.airtable.com/.attachments/54b438a3d1b6006921c19b6353ac41c9/b33c9e07/lotte-20200418-vi2x.png" alt="" />
                                <div className="card-body">
                                    <h4 className="card-title">LOTTE MART</h4>
                                </div>
                                </NavLink>
                            <NavLink className="card text-center pt-3" to="/InformationStore/607f111da4521e067da79e38">
                            
                                <img className="card-img-top1 W-100%" src="https://dl.airtable.com/4GEHCEMSH6S1VGinxdju_Nam%20an-large%402x.jpg" alt="" />
                                <div className="card-body">
                                    <h4 className="card-title">NAM AN MARKET</h4>
                                </div>
                                
                            </NavLink>
                            <NavLink className="card text-center pt-3" to="/InformationStore/607f109fa4521e067da79e32">
                                <img className="card-img-top1 W-100%" src="https://dl.airtable.com/gHI31HCSELYU8QuBRNwQ_AnNam%402x.png" alt="" />
                                <div className="card-body">
                                    <h4 className="card-title">ANNAM GOURMET</h4>
                                </div>
                                </NavLink>
                                <NavLink className="card text-center pt-3" to="/InformationStore/607f1183a4521e067da79e3a">
                                <img className="card-img-top1 W-100%" src="https://firebasestorage.googleapis.com/v0/b/capstone2-24fd3.appspot.com/o/product%2FlogoProduct%2F8VUy8qPmS8iNxGu4egBj_logo_2x-removebg-preview.png?alt=media&token=0b268be6-d566-4420-9a36-ad72371917bd" alt="" />
                                <div className="card-body">
                                    <h4 className="card-title">Chợ Phố Fresh Food</h4>
                                </div>
                                </NavLink>
                                <NavLink className="card text-center pt-3" to="/InformationStore/607f0f76a4521e067da79e2c">
                                <img className="card-img-top1 W-100%" src="https://firebasestorage.googleapis.com/v0/b/capstone2-24fd3.appspot.com/o/product%2FlogoProduct%2FSTORE-COVER2x-removebg-preview%20(1).png?alt=media&token=2dfa3510-53c0-447f-a373-dc2af6062c03" alt="" />
                                <div className="card-body">
                                    <h4 className="card-title">Top Martket</h4>
                                </div>
                                </NavLink>
                            
                        </div>
                    </div>
            </div>
        )
    }

