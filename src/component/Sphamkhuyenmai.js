
import CardDemo2 from '../component/user/combined/CardDemo2';
import CountDown from './CountDown';
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import Loadpage from "../component/Loadpage";
import { API } from "../config/ConfigENV";
import { makeid } from "../helpers/create/create_key_index";
export default function Sphamkhuyenmai() {
  const [productPercentDiscount, setProductPercentDiscount] = useState();

  const fetchData = async () => {
    const result = await axios.get(`${API}/products?filterPercentDiscount=2&limit=4`, {
    })

    return result.data
  }

  useEffect(() => {
    fetchData().then(data => {
      setProductPercentDiscount(data)
    })
  }, [])


  if (!productPercentDiscount || productPercentDiscount.lengh === 0) {
    return <Loadpage></Loadpage>
  }

  return (
    <div>
      <section style={{ margin: '0 auto', position: 'relative', height: '763px' }} className="spkhuyenmai mt-4">
        <div className="bgimg">
          <h1 className="text-center pt-5">SẢN PHẨM KHUYẾN MÃI</h1>
          <div className="middle">
            <p className="p-5 mb-3" id="timer" />
          </div>
          <div className="bottomleft">
          </div>
        </div>
        <div className="container mt-5 spkm pb-4">
          <CountDown></CountDown>
          <h4 style={{ fontFamily: '"Oswald", sans-serif', fontWeight: 'bold' }} className="mb-4 pt-4 text-center">
          </h4>
          <div className="cardDemo">
            <div className="row mt-3 p-5">
              {
                productPercentDiscount.list.map((product) => {
                  return <CardDemo2 key={makeid(10)} product={product}></CardDemo2>
                })
              }
            </div>
          </div>
          <div className="text-center m-4">
            <a type="button" href="/KhuyenMai" className="btnlogin p-2 mt-4">Xem thêm</a>
          </div>
        </div>
      </section>

    </div>
  )
}

