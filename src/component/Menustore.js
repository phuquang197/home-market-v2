import React from 'react';
import {

  NavLink
} from "react-router-dom";
import NotifiOfStore from './nav/NotifiOfStore';
function Menustore() {
  return (
    <div style={{width:"1519px"}} className="sticky-top" className="menustore">
      <div className="sticky-top" style={{ padding: '0px', margin: '0px', zIndex: '101' }}>
        <section className="menu container-fluid m-0 p-0">
          <nav style={{ backgroundColor: 'white', boxShadow: '0 1px 8px #555555' }} id="menu" className="navbar navbar-expand-lg navbar-light bg-light text-center">
            <NavLink className="MenuRow" to="/"> <img width="70" height="70" src="https://scontent-hkg4-2.xx.fbcdn.net/v/t1.15752-9/173230510_1216984905426913_7821080656904254425_n.png?_nc_cat=111&ccb=1-3&_nc_sid=ae9488&_nc_ohc=stKGIs7WU2sAX8IDJvX&_nc_ht=scontent-hkg4-2.xx&oh=d1644442a2ed99440622c9a0ce459a59&oe=60CDBDA5"></img> </NavLink>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
              <span style={{ color: 'white' }} className="navbar-toggler-icon" />
            </button>
            <div className="collapse navbar-collapse pr-3" id="navbarText">
              <ul className="navbar-nav mr-auto justify-content-center">
                <li className="nav-item active">
                  <NavLink className="nav-link js-scroll-trigger MenuRow1" to="/Productstore">Sản phẩm </NavLink>

                </li>
                <li className="nav-item">
                  <NavLink className="nav-link js-scroll-trigger MenuRow" to="/ChartofStore">Thống Kê</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link js-scroll-trigger MenuRow" to="/Warehouseentry">Kho hàng</NavLink>
                </li>
                <li className="nav-item dropdown">
                  <NavLink className="nav-link js-scroll-trigger MenuRow" to="/Billofsale">Bán hàng</NavLink>
                </li>
                {/* <li className="nav-item ml-2 mr-2">
                  <NavLink className="nav-link js-scroll-trigger MenuRow" to="/User">Khách hàng</NavLink>
                </li>
                <li className="nav-item ml-2 mr-2">
                  <NavLink className="nav-link js-scroll-trigger MenuRow" to="/Sale">Khuyến mãi</NavLink>
                </li> */}
                <li className="nav-item ml-2 mr-2">
                  <NavLink className="nav-link js-scroll-trigger MenuRow" to="/paymentOfStore">Thanh toán</NavLink>
                </li>
                <li className="nav-item ml-2 mr-2">
                  <NavLink className="nav-link js-scroll-trigger MenuRow" to="/EmployeeEdit"> Xem thông tin nhân viên</NavLink>
                </li>
              </ul>
              <span className="navbar-text mt-3">
                <div className="dropdown open">
                  <span style={{ left: '51px', position: 'relative' }} className="badge badge-danger badge-counter">3+</span>
                  <button style={{ boxShadow: "none" }}
                    className="btn"
                    type="button"
                    id="triggerId"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    <img
                      className="mr-1 mt-2"
                      width="30px"
                      height="30px"
                      alt="Generic placeholder image"
                      src="https://cdn.iconscout.com/icon/free/png-256/bell-479-458061.png"
                    />
                  </button>
                  <div
                    className="dropdown-menu dropdown-menu-right p-3"
                    aria-labelledby="triggerId"
                  >
                    {/* <NavLink className="dropdown-item  MenuRow" to="/">
                      Thông Báo I
                                </NavLink>
                    <NavLink className="dropdown-item  MenuRow" to="/">
                      Thông Báo II
                                </NavLink>
                    <NavLink className="dropdown-item  MenuRow" to="/">
                      Thông Báo III
                                </NavLink>
                    <div className="dropdown-divider"></div>
                    <NavLink className="dropdown-item  MenuRow" to="/">
                      Xem Thêm
                    </NavLink> */}

                      <NotifiOfStore></NotifiOfStore>
                  </div>
                </div>
              </span>
            </div>
          </nav>

        </section>
      </div >
    </div>

  );
}

export default Menustore;