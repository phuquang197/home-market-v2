
import CardDemo from '../component/user/combined/CardDemo';
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import Loadpage from "../component/Loadpage";
import { API } from "../config/ConfigENV";
import { makeid } from "../helpers/create/create_key_index";
import { NavLink } from 'react-router-dom';
export default function Meat() {
  const [productMeat, setProductMeat] = useState();

  const fetchData = async () => {
    const result = await axios.get(`${API}/products?tag=MEAT&limit=8`, {
    })

    return result.data
  }

  useEffect(() => {
    fetchData().then(data => {
        setProductMeat(data)
    })
  }, [])


  if (!productMeat || productMeat.lengh === 0) {
    return <Loadpage></Loadpage>
  }

  return (
    <div className="container">
         
          <div className="cardDemo">
            <div className="row mt-3 ">
              {
                productMeat.list.map((product) => {
                  return <CardDemo key={makeid(10)} product={product}></CardDemo>
                })
              }
            </div>
          </div>
          <div className="text-center m-4">
            <NavLink to="/Beat">
            <button type="button" className="btn btn-outline-success">Xem thêm</button>
            </NavLink>
          </div>
        </div>
  )
}

