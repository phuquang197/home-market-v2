import Footer from "../component/Footer";
import Menustore from "../component/Menustore";
import React, { useState, useRef, useCallback } from "react";
import useBookSearch from "./employee/useBookSearch";
import { makeid } from "../helpers/create/create_key_index";
import { getTag } from "../helpers/getTagProduct";
import Modal_delete from "./modal/Modal_delete";
import axios from "axios";
import { API } from "../config/ConfigENV";
import ModalDeleteEmploy from "./modal/ModelDeleteEmploy";

function EmployeeEdit() {
  const [query, setQuery] = useState("");
  const [pageNumber, setPageNumber] = useState(0);
  // let { EmployeeID } = useParams();
    const { books, hasMore, loading, error } = useBookSearch(query, pageNumber);
  const [fullNameEmployee] = useState();
  const token = localStorage.getItem("access_token");
  const storeOwnerID = localStorage.getItem("storeOwnerID");
  const observer = useRef();

  const lastBookElementRef = useCallback(
    (node) => {
      if (loading) return;
      if (observer.current) observer.current.disconnect();
      observer.current = new IntersectionObserver((entries) => {
        if (entries[0].isIntersecting && hasMore) {
          setPageNumber((prevPageNumber) => prevPageNumber + 8);
        }
      });
      if (node) observer.current.observe(node);
    },
    [loading, hasMore]
  );

  async function  addEmployee() {
   const fullName =  document.getElementById("fullNameEmployee").value ;
      console.log(fullName);
      const result = await axios({
        method:"POST",
        url:`${API}/storeOwners/${storeOwnerID}/createEmployee`,
        data: {
          fullName
        },
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })

     if (result) {
      window.location.reload();
      alert("Thêm nhân viên thành công")
     }else{
      alert("Thêm nhân Viên thất bại")
     }
  }
  async function Delete(id){
    let response = await axios.delete(`${API}/storeOwners/${id}}`,  
    {
      headers:{
          Authorization: `Bearer ${token}`,
        
      }
    }
    )
    console.log(response);
    
   window.location.reload();

  }
  return (
    <div>
      <Menustore></Menustore>
      <div>
        <div className="container mt-3 mb-5 text-right">
          <div className="navbar-text text-right pr-3 pt-0">
            <input
              type="Text"
              className="form-control search "
              name=""
              id="fullNameEmployee"
              aria-describedby="helpId"
              placeholder="Nhập tên nhân viên"
            />
          </div>
          <button
            type="button"
            onClick={addEmployee}
            className="btn btn-primary mr-3"
          >
            Thêm Nhân Viên
          </button>
        </div>
        <div className="container mt-5">
          <div className="container">
            <div className="table-responsive-sm mb-4 pb-5">
              <table className="table table-hover table-responsive-sm">
                <thead className="thead text-center">
                  <tr>
                    <th scope="col">Id</th>
                    <th scope="col">Tên Đăng nhập</th>
                    <th scope="col">Tên Nhân Viên</th>
                   
                  </tr>
                </thead>
                <tbody>
                  <>
                    {books.map((book, index) => {
                      if (books.length === index + 1) {
                        return (
                          <tr key={makeid(10)} className="text-center">
                            <th scope="row">{index + 1}</th>
                            <td>
                              <strong>{book.userName}</strong>
                            </td>
                            <td>
                            <strong style={{ color: "green" }}>
                                {book.fullName}
                              </strong>
                            </td>
                          
                            {/* <td className="text-center">
                              <div className="btn-group">
                                <div className="btn btn-danger">

                                  Xoá
                                </div>
                              </div>
                            </td> */}
                          </tr>
                        );
                      } else {
                        return (
                          <tr key={makeid(10)} className="text-center">
                            <th scope="row">{index + 1}</th>
                            <td>
                              <strong>{book.userName}</strong>
                            </td>
                            <td>
                              <strong style={{ color: "green" }}>
                                {book.fullName}
                              </strong>
                            </td>
                          
                            
                          </tr>
                        );
                      }
                    })}
                    <div>{loading && "Loading..."}</div>
                    <div>{error && "Error"}</div>
                  </>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        {/* Modal */}
        <ModalDeleteEmploy fullNameEmployee={fullNameEmployee}></ModalDeleteEmploy>
        {/* Modal */}
        <Footer></Footer>
      </div>
    </div>
  );
}

export default EmployeeEdit;
