import { Bar } from "react-chartjs-2";
import Footer from "../component/Footer";
import Menustore from "../component/Menustore";
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import Loadpage from "../component/Loadpage";
import { API } from "../config/ConfigENV";

function ChartTopMarket() {
  const token = localStorage.getItem("access_token");
  const userID = localStorage.getItem("userID");
  const [paymenteds, setPaymenteds] = useState();
  const [directPayment, setDirectPayment] = useState();
  const [giaoHang, setGiaoHang] = useState();
  const [tuLayHang, setTuLayHang] = useState();

  const fetchDataPaymentEd = async () => {
    const result = await axios({
      method: "get",
      url: `${API}/payment/aggregate/data/log?paymentStatus=PAYMENTED&userCreatedProductID=${userID}`,
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });

    return result.data
  }

  const fetchDataDIRECT_PAYMENT = async () => {
    const result = await axios({
      method: "get",
      url: `${API}/payment/aggregate/data/log?paymentStatus=DIRECT_PAYMENT&userCreatedProductID=${userID}`,
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });

    return result.data
  }

  const fetchDataGiaoHang = async () => {
    const result = await axios({
      method: "get",
      url: `${API}/payment/aggregate/data/log?giaoHangStatus=GIAO_HANG&userCreatedProductID=${userID}`,
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });

    return result.data
  }

  const fetchDataTuLayHang = async () => {
    const result = await axios({
      method: "get",
      url: `${API}/payment/aggregate/data/log?giaoHangStatus=TU_LAY_HANG&userCreatedProductID=${userID}`,
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });

    return result.data
  }

  useEffect(() => {
    fetchDataPaymentEd().then(data => {
      setPaymenteds(data)
    })
    fetchDataDIRECT_PAYMENT().then(data => {
      setDirectPayment(data)
    })
    fetchDataGiaoHang().then(data => {
      setGiaoHang(data)
    })
    fetchDataTuLayHang().then(data => {
      setTuLayHang(data)
    })
  }, [])

  if (!paymenteds || paymenteds.lengh === 0 || !directPayment || directPayment.length === 0
    || !giaoHang || giaoHang.length === 0
    || !tuLayHang || tuLayHang.length === 0
    ) {
    return <Loadpage></Loadpage>
  }

  // console.log(paymenteds);
  const newPaymenteds = paymenteds.list.slice((paymenteds.list.length-30), paymenteds.list.length);
  const newDirectPayment = directPayment.list.slice((directPayment.list.length-30), directPayment.list.length);
  const newGiaoHang = giaoHang.list.slice((giaoHang.list.length-30), giaoHang.list.length);
  const newTuLayHang = tuLayHang.list.slice((tuLayHang.list.length-30), tuLayHang.list.length);

  // console.log(paymenteds);
  const labelsPaymented =  newPaymenteds.map(element => {
    var day = element.date.slice(0, 10)
    let data = `${day.slice(8, 10)}-${day.slice(5, 7)}-${day.slice(0, 4)}`
    return data
  });

  const dataPaymented =  newPaymenteds.map(element => element.count);
  const dataDirectPayment = newDirectPayment.map(element => element.count);
  const dataNewGiaoHang = newGiaoHang.map(element => element.count);

  console.log(tuLayHang);
  const dataNewTuLayHang = newTuLayHang.map(element => element.count);
  // console.log(labels);
  return (
    <>
      <Menustore></Menustore>

      <div className="container mt-5 mb-5 p-5 ">

        <div className="row bg-white p-4">
        <h3 className="text-center"> <strong>Biểu đồ các đơn hàng đã được thanh toán và chưa thanh toán trong 30 ngày gần nhất </strong></h3>
          <Bar
            data={{
              labels: labelsPaymented,
              datasets: [
                {
                  label: "Số đơn hàng đã thanh toán thành công",
                  backgroundColor: [
                    "#33CC66	"
                  ],
                  data: dataPaymented,
                },
                {
                  label: "Số đơn hàng không được thanh toán ",
                  backgroundColor: [
                    "#EE0000",
                  ],
                  data: dataDirectPayment,
                },
              ],
            }}
            options={{
              legend: { display: false },
              title: {
                display: true,
                text: "Doanh thu trong ngày",
              },
            }}
          />
        </div>
      </div>

      <div className="container mt-5 mb-5 p-5 ">
      <div className="row bg-white p-4">
        <h3 className="text-center"> <strong>Biểu đồ các đơn người dùng muốn giao hàng tận nhà và tự đến lấy trong 30 ngày gần nhất </strong></h3>
          <Bar
            data={{
              labels: labelsPaymented,
              datasets: [
                {
                  label: "Thống kê doanh thu người dùng chọn phương pháp giao hàng",
                  backgroundColor: [
                    "#3e95cd"
                  ],
                  data: dataNewGiaoHang,
                },
                {
                  label: "Thống kê doanh thu người dùng chọn phương pháp đến quầy lấy hàng ",
                  backgroundColor: [
                    "#e84393",
                  ],
                  data: dataNewTuLayHang,
                },
              ],
            }}
            options={{
              legend: { display: false },
              title: {
                display: true,
                text: "Doanh thu trong ngày",
              },
            }}
          />
        </div>
      </div>

      <Footer></Footer>
    </>
  );
}

export default ChartTopMarket;
