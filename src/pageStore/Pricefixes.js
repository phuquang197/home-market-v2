import React from 'react';
import Footer from '../component/Footer';
import Menustore from '../component/Menustore';
import { NavLink } from 'react-router-dom';
function Pricefixes() {
    return (
        <div>
            <Menustore></Menustore>
            <div className="container-fuild ">
                <div className="row">
                    <div className="col-md-2 c-12 menu-trai">
                        <div className="sanpham MenuRow1 pt-2 pb-2">
                            <i className="fa fa-cube" />  Sản Phẩm
      </div>
                        <div className="danhmuc-thuonghieu">
                            <div className="dropdown">
                                <a className="btn MenuRow dropdown-toggle"  href="/#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Danh mục và thương hiệu
          </a>
                                <div className="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                    <a className="dropdown-item" href="/#">Action</a>
                                    <a className="dropdown-item" href="/#">Another action</a>
                                    <a className="dropdown-item" href="/#">Something else here</a>
                                </div>
                            </div>
                        </div>
                        <div className="sanpham MenuRow pt-2 pb-2">
                            Thuộc tính
      </div>
                        <div className="danhmuc-thuonghieu">
                            <div className="dropdown">
                                <a className="btn MenuRow dropdown-toggle"  href="/#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Nhà cung cấp
          </a>
                                <div className="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                    <a className="dropdown-item" href="/#">Action</a>
                                    <a className="dropdown-item" href="/#">Another action</a>
                                    <a className="dropdown-item" href="/#">Something else here</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-10 c-12 menu_phai">
                        {/* thanhcongcu */}
                        <ul className="nav nav-tabs mt-2">
                            <li className="nav-item">
                                <a className="nav-link MenuRow" href="/Productstore"><i className="fa fa-bars pr-2" />Tất cả</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link active MenuRow1" href="/Pricefixes"> <i className="fa fa-edit pr-2" />Thông Tin Chỉnh Sưa</a>
                            </li>
                            
                            <li className="nav-item">
                                <a className="nav-link MenuRow" href="/DeletehistoryProduct" tabIndex={-1} aria-disabled="true"><i className="fa fa-trash pr-2" />Lịch sử xóa</a>
                            </li>
                        </ul>
                        {/* themsanpham */}
                        <div className="themmoi mt-3 mb-3 ml-2 pr-4">
                        <NavLink to="/addProduct" className="btn btn-outline-danger">THÊM MỚI<i className="fa fa-plus pl-1" /></NavLink>
                        <div style={{ float: 'right' }} className="btn-group pr-5" role="group" aria-label="Basic mixed styles example">
                        <div className="dropdown mr-2">
                                <button className="btn btn-primary dropdown-toggle" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Lọc 1
                                </button>
                                <div className="dropdown-menu bg-dark" aria-labelledby="dropdownMenuLink">
                                    <a className="dropdown-item" href="/#">Action</a>
                                    <a className="dropdown-item" href="/#">Another action</a>
                                    <a className="dropdown-item" href="/#">Something else here</a>
                                </div>
                            </div>
                            <div className="dropdown mr-2">
                                <button className="btn btn-primary dropdown-toggle" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Lọc 1
                                </button>
                                <div className="dropdown-menu bg-dark" aria-labelledby="dropdownMenuLink">
                                    <a className="dropdown-item" href="/#">Action</a>
                                    <a className="dropdown-item" href="/#">Another action</a>
                                    <a className="dropdown-item" href="/#">Something else here</a>
                                </div>
                            </div>
                        </div>
                        <div style={{ float: 'right' }} className="navbar-text text-right pr-3 pt-0">
                            <input type="search"
                                className="form-control search " name="" id="" aria-describedby="helpId" placeholder="Search" />
                        </div>

                    </div>
                        {/* bangsanpham */}
                        <div className="table-responsive-sm mb-4 pb-5">
                        <table className="table table-hover table-responsive-sm">
                                <thead className="thead">
                                    <tr>
                                        <th scope="col">Mã sản phẩm</th>
                                        <th scope="col">Tên sản phẩm</th>
                                        <th scope="col">Ngành Hàng</th>
                                        <th scope="col">Gía </th>
                                        <th scope="col">Người sửa</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row">1</th>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">2</th>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">3</th>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>


            <Footer></Footer>
        </div>
    );
}

export default Pricefixes;