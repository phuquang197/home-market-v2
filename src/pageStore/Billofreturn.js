import React from 'react';
import Footer from '../component/Footer';
import Menustore from '../component/Menustore';
import { NavLink } from 'react-router-dom';
function Billofreturn(props) {
    return (
        <div>
            <Menustore></Menustore>
            <div className="container-fuild ">
                <div className="row">
                    <div className="col-md-2 c-12 menu-trai">
                        <div className="sanpham pt-2 pb-2">
                            <a className="MenuRow" href="/Billofsale"> <i className="fa fa-cube" />  Hóa đơn bán</a>
                        </div>
                        <div style={{ color: '#eb4d4b' }} className="sanpham pt-2 pb-2">
                            <a className="MenuRow1" href="/Bilofreturn"><i className="fa fa-cube" />  Hóa đơn trả hàng</a>
                        </div>
                    </div>
                    <div className="col-md-10 c-12 menu_phai">
                        {/* thanhcongcu */}
                        {/* themsanpham */}
                        <div className="themmoi mt-3 mb-3 ml-2 pr-4">
                        <NavLink to="/addProduct" className="btn btn-outline-danger">THÊM MỚI<i className="fa fa-plus pl-1" /></NavLink>
                        <div style={{ float: 'right' }} className="btn-group pr-5" role="group" aria-label="Basic mixed styles example">
                        <div className="dropdown mr-2">
                                <button className="btn btn-primary dropdown-toggle" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Lọc 1
                                </button>
                                <div className="dropdown-menu bg-dark" aria-labelledby="dropdownMenuLink">
                                    <a className="dropdown-item" href="#">Action</a>
                                    <a className="dropdown-item" href="#">Another action</a>
                                    <a className="dropdown-item" href="#">Something else here</a>
                                </div>
                            </div>
                            <div className="dropdown mr-2">
                                <button className="btn btn-primary dropdown-toggle" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Lọc 1
                                </button>
                                <div className="dropdown-menu bg-dark" aria-labelledby="dropdownMenuLink">
                                    <a className="dropdown-item" href="#">Action</a>
                                    <a className="dropdown-item" href="#">Another action</a>
                                    <a className="dropdown-item" href="#">Something else here</a>
                                </div>
                            </div>
                        </div>
                        <div style={{ float: 'right' }} className="navbar-text text-right pr-3 pt-0">
                            <input type="search"
                                className="form-control search " name="" id="" aria-describedby="helpId" placeholder="Search" />
                        </div>

                    </div>
                        {/* bangsanpham */}
                        <div className="table-responsive-sm mb-4 pb-5">
                        <table className="table table-hover table-responsive-sm">
                                <thead className="thead">
                                    <tr>
                                        <th scope="col">Ngày trả</th>
                                        <th scope="col">Khách hàng</th>
                                        <th scope="col">Địa chỉ</th>
                                        <th scope="col">Sản phẩm</th>
                                        <th scope="col">Gía</th>
                                        <th scope="col">Số lượng</th>
                                        <th scope="col">Trả lại</th>
                                        <th scope="col">Phí phải trả</th>
                                        <th scope="col">Tổng tiền</th>
                                        <th scope="col">Người lập biểu</th>
                                        <th scope="col">Ghi chú</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row">1</th>
                                        <td>Mark</td>
                                        <td>Otto</td>
                                        <td>@mdo</td>
                                        <td>Mark</td>
                                        <td>Otto</td>
                                        <td>@mdo</td>
                                        <td>Mark</td>
                                        <td>Otto</td>
                                        <td />
                                        <td />
                                    </tr>
                                    <tr>
                                        <th scope="row">2</th>
                                        <td>Jacob</td>
                                        <td>Thornton</td>
                                        <td>@fat</td>
                                        <td>Mark</td>
                                        <td>Otto</td>
                                        <td>@mdo</td>
                                        <td>Mark</td>
                                        <td>Otto</td>
                                        <td />
                                        <td />
                                    </tr>
                                    <tr>
                                        <th scope="row">3</th>
                                        <td>Larry</td>
                                        <td>the Bird</td>
                                        <td>@twitter</td>
                                        <td>Mark</td>
                                        <td>Otto</td>
                                        <td>@mdo</td>
                                        <td>Mark</td>
                                        <td>Otto</td>
                                        <td />
                                        <td />
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <Footer></Footer>
        </div>
    );
}

export default Billofreturn;