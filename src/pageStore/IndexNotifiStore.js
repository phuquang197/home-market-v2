import Footer from "../component/Footer";
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { API } from "../config/ConfigENV";
import { makeid } from '../helpers/create/create_key_index';
import NotificaStore from '../component/NotificaStore';
import Menustore from "../component/Menustore";
function IndexNotifiStore() {
    const [notications, setNotifications] = useState();
    // let { userID } = useParams();
    const token = localStorage.getItem("access_token");
    const storeOwnerID = localStorage.getItem("storeOwnerID");
    const fetchData = async () => {
        const result = await axios({
            method: "get",
            url: `${API}/notications?limit=10&receiverUStoreID=${storeOwnerID}`,
            headers: {
                Authorization: `Bearer ${token}`,
            },
        });

        return result.data
    }
    useEffect(() => {
        fetchData().then(data => {
            setNotifications(data)
        })
    }, [])

    if (!notications || notications.lengh === 0) {
        return ("BẠN KHÔNG CÓ THÔNG BÁO NÀO")
    }
    console.log(notications);
    return (
        <div >
            <Menustore></Menustore>

            <div className="container mt-5 mb-5 bg-white">
                <h4 className="pt-2 text-center">Thông Báo</h4>
                <div className="row">
                    <div className="p-3 text-notifi">
                        {
                            notications.list.map((notication) => {
                                return <NotificaStore key={makeid(10)} notication={notication}></NotificaStore>
                            })
                        }

                    </div>
                </div>
            </div>
            <Footer></Footer>

        </div>
    );
}

export default IndexNotifiStore;


