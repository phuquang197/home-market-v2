import React, { useContext, useEffect, useState } from "react";
import axios from "axios";
import Menustore from "../component/Menustore";
import Footer from "../component/Footer";
// import InputFile from "../component/InputFile";
import { useParams } from "react-router-dom";
import { context } from "../ContextApi/Provide";
import { API } from "../config/ConfigENV";
// import Error from "./error/error";
import { makeid } from "../helpers/create/create_key_index";
import Loadpage from "../component/Loadpage";

import { storage } from "../firebase/Firebase";

function EditProduct() {
  let { productID } = useParams();
  const token = localStorage.getItem("access_token");

  let consumer = useContext(context);

  const [file, setFile] = useState(null);
  const [url, setUrl] = useState("");
  const [error1, setError1] = useState("true1");
  const [productEdit, setProductEdit] = useState();

  function handleChange(e) {
    setFile(e.target.files[0]);
  }

  const productHandle = async () =>{

    const result = await axios({
      method: "get",
      url: `${API}/products/${productID}`
    });


    setProductEdit(result.data)
    setUrl(result.data.photos)
  }

  const handleUpload = () => {
    if (!file) return;
    const uploadTask = storage
      .ref(`/images/${makeid(10)}-${file.name}`)
      .put(file);
    uploadTask.then((res) => {

      storage
        .ref("images")
        .child(res.ref.name)
        .getDownloadURL()
        .then((url) => {
          consumer.setLinkImage(url);
          setUrl(url);
        });
    });
  };

  async function uploadData(e) {
    let data = await {
      name: document.getElementById("name").value,
      description: document.getElementById("description").value,
      photos: url,
      price: document.getElementById("price").value,
      nextWeight: document.getElementById("nextWeight").value,
      tag: document.getElementById("tag").value,
      percentDiscount: document.getElementById("percentDiscount").value,
      amount: document.getElementById("amount").value,
      quantitySold: 0,
      calories: "string",
      origin: "string",
      statusAccount: "UNLOCK",
      status: document.getElementById("status").value,
      tradeMark: document.getElementById("tradeMark").value,
      inCard: "false",
      calories: "1",
    };

    for (const property in data) {

      if (data[property] !== "") {
        setError1("true");
      }

      if (data[property] === "") {
        setError1("false");
        break;
      }
    }
  }

  useEffect(() => {
    productHandle();
  }, []);



  useEffect(() => {
    handleUpload();

  }, [file]);

  const PostAPI = async () => {
    let data = await {
      name: document.getElementById("name").value,
      description: document.getElementById("description").value,
      photos: url,
      price: document.getElementById("price").value,
      nextWeight: document.getElementById("nextWeight").value,
      tag: document.getElementById("tag").value,
      percentDiscount: document.getElementById("percentDiscount").value,
      amount: document.getElementById("amount").value,
      quantitySold: 0,
      calories: "string",
      origin: "string",
      statusAccount: "UNLOCK",
      status: document.getElementById("status").value,
      tradeMark: document.getElementById("tradeMark").value,
      inCard: "false",
      calories: "1",
    };

    const result = await axios({
      method: "put",
      url: `${API}/products/${productID}`,
      data: {
        ...data,
      },
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });


    await window.location.reload();
    await alert("Chỉnh sửa thành công ");
  };

  useEffect(() => {
    if (error1 === "true") {
      PostAPI();
    }
  }, [error1]);

  if(!productEdit || productEdit.length === 0){
    return <Loadpage></Loadpage>
  }

  return (
    <div className="Addproduct">
      <Menustore></Menustore>
      <div className="container-fuild ">
        <div className="row">
          <div className="col-md-2 c-12 menu-trai">
            <div className="sanpham MenuRow1 pt-2 pb-2">
              <i className="fa fa-cube" /> Sản Phẩm
            </div>
            
           
          
          </div>

          <div className="col-md-10 c-12 menu_phai ">
            <div className="row m-5 p-5 bg-white">
              <div
                style={{ display: error1 === "false" ? "block" : "none" }}
                className="alert alert-danger"
              >
                <strong>!</strong> Không được để trống trường nào
              </div>

              <div className="col-md-2 c-12">
                Ảnh
                <div className="input-group mb-3 mt-4 c-12">
                  <div className="">
                    <form>
                      <input type="file" onChange={handleChange} />
                    </form>
                    <img style={{width:'150px'}} src={url} alt="Logo" />
                  </div>
                </div>
              </div>
              <div className="col-md-5">
                <form>
                  <div className="form-group">
                    <label htmlFor="exampleInputEmail1">Tên sản phẩm</label>
                    <input id="name" type="text" defaultValue={`${productEdit.name}`} className="form-control" />
                  </div>
                  <div className="form-group">
                    <label htmlFor="exampleInputText">Mô Tả</label>
                    <div className="mb-3">
                      <textarea
                        id="description"
                        className="form-control"
                        rows={3}
                        defaultValue={`${productEdit.description}`}
                      />
                    </div>
                  </div>

                  <div className="form-group">
                    <label htmlFor="exampleInputText">Giảm giá</label>
                    <input
                      type="text"
                      id="percentDiscount"
                      className="form-control"
                      defaultValue={`${productEdit.percentDiscount}`}
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="exampleInputText">Số lượng</label>
                    <input
                      type="number"
                      id="amount"
                      className="form-control"
                      defaultValue={`${productEdit.amount}`}
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="exampleInputText">Xuất Xứ</label>
                    <input
                      type="origin"
                      className="form-control"
                      id="tradeMark"
                      defaultValue={`${productEdit.tradeMark}`}
                    />
                  </div>
                </form>
              </div>
              <div className="col-md-5">
                <form>
                  <div className="form-group">
                    <label htmlFor="exampleInputText">Gía </label>
                    <input id="price" type="number" defaultValue={`${productEdit.price}`} className="form-control" />
                  </div>
                  <div className="form-group">
                    <label htmlFor="exampleInputText">Khối Lượng</label>
                    <input
                      id="nextWeight"
                      type="text"
                      className="form-control"
                      defaultValue={`${productEdit.nextWeight}`}
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="exampleInputText">Ngành Hàng</label>
                    {/* <input type="tag" className="form-control" id="tag" /> */}
                    <select
                      id="tag"
                      className="form-select"
                      aria-label="Default select example"
                      defaultValue={`${productEdit.tag}`}
                    >
                      <option value="FRUIT">Trái Cây</option>
                      <option value="DRINKS">Đồ Uống</option>
                      <option value="MEAT">Thịt</option>
                      <option value="SEA_FOOD">Hải sản</option>
                      <option value="MILK_AND_AGE">Trứng & Sữa</option>
                      <option value="BREAD">Bánh</option>
                      <option value="FROZREN">Đồ Đông Lạnh</option>
                      <option value="ORGANIC">Sản phẩm hữ cơ</option>
                    </select>
                  </div>

                  <div className="form-group">
                    <label htmlFor="exampleInputText">Tình trạng</label>
                    {/* <input type="tag" className="form-control" id="tag" /> */}
                    <select
                      id="status"
                      className="form-select"
                      aria-label="Default select example"
                      defaultValue={`${productEdit.status}`}
                    >
                      <option value="STOCKING">Còn hàng</option>
                      <option value="OUT_OF_STOCK">Hết hàng</option>
                    </select>
                  </div>
                </form>
              </div>
              <div className="nut text-center">
                <button
                  style={{ width: "150px" }}
                  onClick={uploadData}
                  className="btnlogin p-2"
                >
                  {" "}
                  Lưu sản phẩm
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>

      <Footer></Footer>
    </div>
  );
}

export default EditProduct;
