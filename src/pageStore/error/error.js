import React, { useContext, useEffect, useState } from "react";

export default function Error(props) {
  const {error} =props
    useEffect(() => {
    console.log(error);

  }, [error]);
    return (
      <div style={{display: error ? 'block' : 'none'}} className="alert alert-danger">
      <strong>{error}!</strong> Indicates a dangerous or potentially
      negative action.
    </div>
    )
}
