// import Footer from "../component/Footer";
import Menustore from "../component/Menustore";
// import { NavLink } from "react-router-dom";
import React, { useState, useRef, useCallback } from "react";
import useBookSearch from "./paymentOfStore/useBookSearch";
// import { makeid } from "../helpers/create/create_key_index";
import Loadpage from "../component/Loadpage";
import {
  getTag,
  getGiaoHangStatus,
  gePaymentStatus,
} from "../helpers/getTagProduct";
// import Modal_delete from "./modal/Modal_delete";
import Modal_paymentStatus from "./modal/Modal_paymentStatus";

function User() {
  const [query, setQuery] = useState("");
  const [pageNumber, setPageNumber] = useState(0);

  const { books, hasMore, loading, error } = useBookSearch(query, pageNumber);
  const [productID] = useState();

  const observer = useRef();

  const lastBookElementRef = useCallback(
    (node) => {
      // console.log(loading);
      if (loading) return;
      if (observer.current) observer.current.disconnect();
      observer.current = new IntersectionObserver((entries) => {
        if (entries[0].isIntersecting && hasMore) {
          setPageNumber((prevPageNumber) => prevPageNumber + 8);
        }
      });
      if (node) observer.current.observe(node);
    },
    [loading, hasMore]
  );

  function handleSearch() {
    const paymentCode =  document.getElementById("paymentCode").value ;
    setQuery(paymentCode);
    setPageNumber(0);
  }

  // console.log(books);
  return (
    <div>
      <Menustore></Menustore>

      <div className="row">

        <div className="col-md-2 c-12 menu-trai">
          <div className="sanpham MenuRow1 pt-2 pb-2">
            <i className="fa fa-cube" /> Tổng Thanh Toán
          </div>
        </div>

        <div className="col-md-10 c-12 menu_phai">
          <ul className="nav nav-tabs mt-2">
            <li className="nav-item">
              <a
                style={{ color: "#343a40" }}
                className="nav-link MenuRow1 active"
                href="/Productstore"
              >
                <i className="fa fa-bars pr-2" />
                Tất cả
              </a>
            </li>
          </ul>
          <div className="table-responsive-sm pl-2">

          <div className="container mt-3 mb-5 text-right">
          <div className="navbar-text text-right pr-3 pt-0">
            <input
              type="Text"
              className="form-control search "
              name=""
              id="paymentCode"
              aria-describedby="helpId"
              placeholder="Mã thanh toán "
            />
          </div>
          <button
            type="button"
            onClick={handleSearch}
            className="btn btn-primary mr-3"
          >
            Tìm kiếm
          </button>
        </div>
            <table className="table table-hover table-responsive-sm m-0">
              <thead className="thead">
                <tr className="text-center">
                  <th style={{width:'230px'}} scope="col">STT</th>
                  {/* <th style={{width:'230px'}} scope="col">Tên sản phẩm</th> */}
                  <th style={{width:'230px'}} scope="col">Mã thanh toán</th>
                  <th style={{width:'230px'}} scope="col">Hình thức nhận hàng</th>
                  <th style={{width:'230px'}} scope="col">Trạng thái giao dịch</th>
                  <th style={{width:'230px'}} scope="col">Tổng tiền</th>
                </tr>
              </thead>
            </table>
          </div>
          <div id="accordion">
            {books.map((book, index) => {
              if (books.length === index + 1) {
                return (
                  <div ref={lastBookElementRef} key={index}>
                    <div className="p-0 bg-white" id={`AS${book._id}1`}>
                      <div
                        style={{ boxShadow: "none" }}
                        className=" accordion btn w-100 p-0 collapsed m-0"
                        data-toggle="collapse"
                        data-target={`#A${book._id}`}
                        aria-expanded="false"
                        aria-controls={`A${book._id}`}
                      >
                        <table className="table table-hover table-responsive-sm">
                          <tr className="text-center">
                            <th style={{width: '230px'}} scope="col">{index + 1}</th>
                            <th style={{width: '230px'}} scope="col">{book.paymentCode}</th>
                            <th style={{width: '230px'}} scope="col">
                              {getGiaoHangStatus(book.giaoHangStatus)}
                            </th>
                            <th style={{width: '230px'}} scope="col">
                              {gePaymentStatus(book.paymentStatus)}
                            </th>
                            <th style={{width: '230px'}} scope="col">{book.totalMoney}</th>
                          </tr>
                          <tbody></tbody>
                        </table>
                      </div>
                    </div>
                    <div
                      id={`A${book._id}`}
                      className="collapse"
                      aria-labelledby={`AS${book._id}1`}
                      data-parent="#accordion"
                    >
                      <div className="card-body">
                        <table className="table table-hover table-responsive-sm">
                        <tbody>
                        <tr class="toggle text-center">
                              <td>
                                <label for="child1"></label>Chi tiết mặt hàng
                              </td>
                              <td>ảnh</td>
                              <td>Tên</td>
                              <td>số lượng</td>
                            </tr>
                            <tr class="toggle text-center">
                              <td>
                                <label for="child1"></label>Chi tiết mặt hàng
                              </td>
                              <td>ảnh</td>
                              <td>Tên</td>
                              <td>số lượng</td>
                            </tr>
                            <tr class="toggle text-center">
                              <td>
                                <label for="child1"></label>Chi tiết mặt hàng
                              </td>
                              <td>ảnh</td>
                              <td>Tên</td>
                              <td>số lượng</td>
                            </tr>
                            {book.productIDs.map((product) => {
                              return (
                                <tr class="toggle text-center">
                                  <td></td>
                                  <td><img
                              style={{ width: "45px" }}
                              src={product.photo}
                              arl="anh"
                            ></img></td>
                                  <td>{product.name}</td>
                                  <td>{product.amount}</td>
                                </tr>
                              );
                            })}
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                );
              } else {
                return (
                  <div key={index}>
                    <div className="p-0 bg-white" id={`AS${book._id}1`}>
                      <div
                        style={{ boxShadow: "none" }}
                        className=" accordion btn w-100 p-0 collapsed m-0"
                        data-toggle="collapse"
                        data-target={`#B${book._id}`}
                        aria-expanded="false"
                        aria-controls={`B${book._id}`}
                        data-bs-toggle="collapse"
                      >
                        <table className="table table-hover table-responsive-sm">
                          <tr className="text-center">
                            <th style={{width: '230px'}} scope="col">{index + 1}</th>
                            <th style={{width: '230px', color:'#08c25e '}} scope="col">{book.paymentCode}</th>
                            <th style={{width: '230px'}} scope="col">
                              {getGiaoHangStatus(book.giaoHangStatus)}
                            </th>
                            <th style={{width: '230px'}} scope="col">
                              {gePaymentStatus(book.paymentStatus)}
                            </th>
                            <th style={{width: '230px'}} scope="col">{book.totalMoney}</th>
                          </tr>
                          <tbody></tbody>
                        </table>

                      </div>
                    </div>
                    <div
                      id={`B${book._id}`}
                      className="collapse"
                      aria-labelledby={`AS${book._id}1`}
                      data-parent="#accordion"
                    >
                      <div className="card-body">
                        <table className="table table-hover table-responsive-sm">
                          <tbody>
                            {
                              book.paymentStatus === 'DIRECT_PAYMENT' ?
                              <button
                                type="button"
                                className="btn btn-danger" aria-hidden="true"
                                data-toggle="modal"
                                data-target="#exampleModal"
                                onClick={() =>
                                  (document.getElementById(
                                    "dataPaymentID"
                                  ).value = book._id)
                                }
                              >
                                Thanh toán cho khách hàng
                              </button> :
                              <span

                              className="btn btn-success"
                            >
                              Thanh toán thành công
                            </span>


                            }


                          {
                            book.deliveryAddress && book.deliveryAddress !== 'null'?
                            <tr class="toggle text-center">
                            <td>
                              <strong>Địa chỉ nhận hàng</strong>
                            </td>
                            <td>{book.deliveryAddress}</td>
                            <td> </td>
                            <td> </td>
                          </tr> : ''
                          }

                          {
                             book.deliveryPhoneNumber && book.deliveryPhoneNumber !== 'null' && book.deliveryPhoneNumber !== 'false' ?
                             <tr class="toggle text-center">
                              <td>
                              <strong>Số điện thoại liên hệ</strong>

                              </td>
                              <td>{book.deliveryPhoneNumber}</td>
                              <td> </td>
                              <td> </td>
                            </tr>:''
                          }

                            <tr class="toggle text-center">
                              <td>
                                <label for="child1"></label>Chi tiết mặt hàng
                              </td>
                              <td>ảnh</td>
                              <td>Tên</td>
                              <td>số lượng</td>
                            </tr>
                            {book.productIDs.map((product) => {
                              return (
                                <tr class="toggle text-center">
                                  <td></td>
                                  <td><img
                              style={{ width: "45px" }}
                              src={product.photo}
                              arl="anh"
                            ></img></td>
                                  <td>{product.name}</td>
                                  <td>{product.amount}</td>
                                </tr>
                              );
                            })}
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                );
              }
            })}
            {
              loading? <Loadpage></Loadpage> : <></>
            }
            <div>{error && "Error"}</div>
            {/* hàng 2 */}
          </div>
          <Modal_paymentStatus></Modal_paymentStatus>
        </div>
      </div>
    </div>

  );
}

export default User;
