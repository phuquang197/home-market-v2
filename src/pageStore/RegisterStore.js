// import useForm from "../component/Userform";
// import validate from "../helpers/validate/RegisterfFormValidationRule";
import React, { useEffect, useState } from "react";
import axios from "axios";
import Menu from "../component/nav/Menu";
import Footer from "../component/Footer";
import { API } from "../config/ConfigENV";
// import { Redirect } from 'react-router';
import Loadpage from "../component/Loadpage";
import InputFileRgt from "../component/InputFileRgt";

function RegisterStore() {
  const token = localStorage.getItem("access_token");
  const [registerData, setRegisterData] = useState("");
  const [mess, setMess] = useState("");
  //   const [isFindingDataOk, setIsFindingDataOk] = useState('false');
  async function submitRegister() {
    const dataRisterStore = {
      name: document.getElementById("name").value,
      email: document.getElementById("email").value,
      description: document.getElementById("description").value,
      phoneNumbers: document.getElementById("phoneNumbers").value,
      address: document.getElementById("address").value,
      photos: "https://firebasestorage.googleapis.com/v0/b/fir-firebase-4d563.appspot.com/o/logo-removebg-preview.png?alt=media&token=530ac8ce-5944-41c9-80e0-d5d96767f720"
    };

    const result = await axios({
      method: "POST",
      url: `${API}/register_storeOwner`,
      data: dataRisterStore,
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });

    if (result.data.mess === "ok") {
      alert("ĐK Cửa Hàng THÀNH CÔNG ");
      window.location.replace("http://localhost:3000");
    }

    if (result.data.mess !== "ok") {
      setMess(result.data.mess);
    }
  }

  useEffect(() => {}, [mess]);

  return (
    <>
      <Menu token={token}></Menu>
      <div className="mb-5" style={{ paddingTop: "5%" }}>
        <div style={{ color: "#747487" }} className="container form">
          <div className="row">
            <div className="col-md-6 bg-white pt-5">
              <img src="https://firebasestorage.googleapis.com/v0/b/fir-firebase-4d563.appspot.com/o/logo-removebg-preview.png?alt=media&token=530ac8ce-5944-41c9-80e0-d5d96767f720"></img>
            </div>
            <div
              style={{ backgroundColor: "white" }}
              className="col-md-6 pt-5 pr-5 pl-5 "
            >
              {
                mess? <div class="alert alert-danger" role="alert">
                không được đẻ trống mục nào
              </div> : ''
              }
              <div className="mb-3">
                <label className="form-label">Tên Cửa Hàng</label>

                <input className="w-100 form-control" id="name"></input>
              </div>
              <div className="mb-3">
                <label className="form-label">Địa Chỉ</label>

                <input className="w-100 form-control" id="address"></input>
              </div>
              <div className="mb-3">
                <label className="form-label">Số Điện ThoẠI</label>

                <input className="w-100 form-control" id="phoneNumbers"></input>
              </div>
              <div className="mb-3">
                <label className="form-label">Email</label>
                <input id="email" className="w-100 form-control"></input>

                {/* {errors.email && (
                    <p className="help is-danger">{errors.email}</p>
                  )} */}
              </div>
              {/* <FormFileInput></FormFileInput> */}
              <InputFileRgt></InputFileRgt>
              <div className="mb-3">
                <label htmlFor="exampleInputPassword1" className="form-label">
                  Mô Tả
                </label>
                <textarea
                  className="form-control"
                  id="description"
                  rows="3"
                ></textarea>
              </div>
              <button
                type="submit"
                className="button is-block is-info is-fullwidth btnlogin mt-3 p-2"
                onClick={submitRegister}
              >
                Đăng kí
              </button>
            </div>
          </div>
        </div>
      </div>
      <Footer></Footer>
    </>
  );
}

export default RegisterStore;
