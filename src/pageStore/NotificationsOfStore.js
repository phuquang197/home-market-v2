import Footer from "../component/Footer";
import Menustore from "../component/Menustore";
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { API } from "../config/ConfigENV";
import Loadpage from '../component/Loadpage';
import {
    useParams
} from "react-router-dom";
import NumberFormat from 'react-number-format';


function NotificationsOfStore() {
    const [notification, setNotification] = useState();
    const token = localStorage.getItem("access_token");
    let { notificationID } = useParams();
    const fetchData = async () => {
        const result = await axios({
            method: "get",
            url: `${API}/notications/${notificationID}`,
            headers: {
                Authorization: `Bearer ${token}`,
            },
        });

        return result.data
    }
    useEffect(() => {
        fetchData().then(data => {
            setNotification(data)
        })
    }, [])

    if (!notification || notification.lengh === 0) {
        return (<Loadpage></Loadpage>)
    }
    return (
        <div>
                        <Menustore></Menustore>
            <div className="container mt-5 mb-5 bg-white">
                <div className="row p-5">
                    <h4 className="text-center MT mb-5">{notification.description}</h4>
                    {/* <img src= {notification.targetID.productIDs.map( i => i.photo)}>  */}
                    <div className="col-md-4">
                    <img className="mt-2" style={{ width: "200px" }} src= {notification.targetID.productIDs.map( i => i.photo)}></img><br></br>
                    </div>
                    <div className="col-md-8">
                    <h6> Tên Sản Phẩm: {notification.targetID.productIDs.map( i => i.name)}
                      </h6>
                    <h6> Mã Đơn Hàng:  {notification._id}</h6>
                    <h6> Người Mua: {notification.receiverUID.fullName}</h6>
                    <div style={{display:"inline-block"}}><div className="MTT mr-1">Mã Thanh Toán:   </div><div  style={{fontSize:"25px !important",color:"#08c25e",fontWeight:"700"}} className=" MTT"> <b>{notification.targetID.paymentCode}</b> </div></div> <br></br>
                    
                      <h6> Số Lượng: {notification.targetID.productIDs.map( i => i.amount)}
                      </h6>
                    <h6> Địa Chỉ Giao Hàng: {notification.targetID.deliveryAddress}</h6>
                    <h6 className="gia">Tổng tiền thanh toán: <NumberFormat id="giaSanPham" value={notification.targetID.totalMoney} displayType={'text'} thousandSeparator={true} /> VNĐ</h6>

                    </div>
                    

                   


                </div>
            </div>
            <Footer></Footer>
        </div>
    );
}

export default NotificationsOfStore;
