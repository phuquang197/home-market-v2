import React, { useState, useRef, useCallback } from "react";
import useBookSearch from "./useBookSearch";
import { makeid } from '../../helpers/create/create_key_index';

export default function Data() {
  const [query, setQuery] = useState("");
  const [pageNumber, setPageNumber] = useState(1);

  const { books, hasMore, loading, error } = useBookSearch(query, pageNumber);

  const observer = useRef();
  const lastBookElementRef = useCallback(
    (node) => {
      if (loading) return;
      if (observer.current) observer.current.disconnect();
      observer.current = new IntersectionObserver((entries) => {
        if (entries[0].isIntersecting && hasMore) {
          setPageNumber((prevPageNumber) => prevPageNumber + 8);
        }
      });
      if (node) observer.current.observe(node);
    },
    [loading, hasMore]
  );

  function handleSearch(e) {
    setQuery(e.target.value);
    setPageNumber(1);
  }

  return (
    <>
      <input type="text" value={query} onChange={handleSearch}></input>
      {books.map((book, index) => {
        if (books.length === index + 1) {
          return (
              <tr key={makeid(10)} className="text-center">
                <th scope="row">1</th>
                <td className="text-center">
                  <img
                    style={{ width: "45px" }}
                    src="https://firebasestorage.googleapis.com/v0/b/capstone2-24fd3.appspot.com/o/product%2Fdrinks%2F7_up.png?alt=media&token=e142a301-0a1d-479e-9b89-bbf88424bbc9"
                    arl="anh"
                  ></img>
                </td>
                <td ref={lastBookElementRef} key={book}>
                  {book}
                </td>
                <td></td>
                <td></td>
                <td></td>
                <td className="text-center">
                  <div className="btn-group">
                    <div className="btn btn-warning">
                      <i className="fa fa-pencil" aria-hidden="true" />
                      Sửa
                    </div>
                    <div className="btn btn-danger">
                      <i className="fa fa-minus" aria-hidden="true" />
                      Xoá
                    </div>
                  </div>
                </td>
              </tr>
          );
        } else {
          return  <tr key={makeid(10)} className="text-center">
            <th scope="row">1</th>
            <td className="text-center">
              <img
                style={{ width: "45px" }}
                src="https://firebasestorage.googleapis.com/v0/b/capstone2-24fd3.appspot.com/o/product%2Fdrinks%2F7_up.png?alt=media&token=e142a301-0a1d-479e-9b89-bbf88424bbc9"
                arl="anh"
              ></img>
            </td>
            <td ref={lastBookElementRef} key={book}>
              {book}
            </td>
            <td></td>
            <td></td>
            <td></td>
            <td className="text-center">
              <div className="btn-group">
                <div className="btn btn-warning">
                  <i className="fa fa-pencil" aria-hidden="true" />
                  Sửa
                </div>
                <div className="btn btn-danger">
                  <i className="fa fa-minus" aria-hidden="true" />
                  Xoá
                </div>
              </div>
            </td>
          </tr>
        }
      })}
      <div>{loading && "Loading..."}</div>
      <div>{error && "Error"}</div>
    </>
  );
}
