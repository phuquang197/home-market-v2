import React, { useContext, useEffect, useState } from "react";
import axios from 'axios';
import { API } from "../../config/ConfigENV";
export default function Modal_paymentStatus(props) {
  const token =  localStorage.getItem('access_token');

  const changeHandle = async () => {
    let paymentID = document.getElementById("dataPaymentID").value

      await axios({
      method: "put",
      url: `${API}/payment/${paymentID}`,
      data: {
        paymentStatus: 'PAYMENTED',
      },
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });

    await window.location.reload();
  }

    return (
      <div className="modal fade" id="exampleModal" tabIndex={-1} role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div className="modal-dialog" role="document">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title" id="exampleModalLabel">Thanh toán!</h5>
            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div className="modal-body">
            <input id='dataPaymentID' defaultValue='d' style={{display: "none"}}></input>
             <strong>Bạn xác nhận người dùng này đã thanh toán ?</strong>
          </div>
          <div className="modal-footer">
            <button type="button" className="btn btn-secondary" data-dismiss="modal">Không, Quay lại!</button>
            <button type="button" className="btn btn-primary" onClick={changeHandle}>Có, đã thanh toán hoá đơn cho người dùng này</button>
          </div>
        </div>
      </div>
    </div>
    )
}
