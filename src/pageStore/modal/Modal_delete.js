import React, { useContext, useEffect, useState } from "react";
import axios from 'axios';
import { API } from "../../config/ConfigENV";
export default function Modal_delete(props) {
  const token =  localStorage.getItem('access_token');

  const deleteOne = async () => {
    let productId = document.getElementById("dataProductID").value

    const result = await axios({
      method: "delete",
      url: `${API}/products/${productId}`,
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
   
    await window.location.reload();
  }

    return (
      <div className="modal fade" id="exampleModal" tabIndex={-1} role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div className="modal-dialog" role="document">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title" id="exampleModalLabel">Cảnh báo!</h5>
            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div className="modal-body">
            <input id='dataProductID' value='d' style={{display: "none"}}></input>
             <strong>Bạn chắc chắn muốn xoá sản phẩm này?</strong>
          </div>
          <div className="modal-footer">
            <button type="button" className="btn btn-secondary" data-dismiss="modal">Không, Quay lại!</button>
            <button type="button" className="btn btn-primary" onClick={deleteOne}>Có, tôi muốn xoá</button>
          </div>
        </div>
      </div>
    </div>
    )
}
