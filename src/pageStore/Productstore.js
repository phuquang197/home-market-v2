import Footer from "../component/Footer";
import Menustore from "../component/Menustore";
import { NavLink } from "react-router-dom";
import React, { useState, useRef, useCallback } from "react";
import useBookSearch from "./test/useBookSearch";
import { makeid } from "../helpers/create/create_key_index";
import { getTag } from "../helpers/getTagProduct";
import Modal_delete from "./modal/Modal_delete";
import NumberFormat from "react-number-format";

function Productstore() {
  const [query, setQuery] = useState("");
  const [pageNumber, setPageNumber] = useState(0);

  const { books, hasMore, loading, error } = useBookSearch(query, pageNumber);
  const [productID] = useState();

  const observer = useRef();

  function handleClick(id) {
    console.log(id);
  }
  const lastBookElementRef = useCallback(
    (node) => {
      if (loading) return;
      if (observer.current) observer.current.disconnect();
      observer.current = new IntersectionObserver((entries) => {
        if (entries[0].isIntersecting && hasMore) {
          setPageNumber((prevPageNumber) => prevPageNumber + 8);
        }
      });
      if (node) observer.current.observe(node);
    },
    [loading, hasMore]
  );

  function handleSearch(e) {
    setQuery(e.target.value);
    setPageNumber(0);
  }

  return (
    <div className="container-fuild ">
      <Menustore></Menustore>
      <div className="row">
        <div className="col-md-2 c-12 menu-trai">
          <div className="sanpham MenuRow1 pt-2 pb-2">
            <i className="fa fa-cube" /> Sản Phẩm
          </div>
          {/* <div className="danhmuc-thuonghieu">
            <div className="dropdown">
              <a
                className="btn MenuRow dropdown-toggle"
                href="#"
                role="button"
                id="dropdownMenuLink"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                Danh mục và thương hiệu
              </a>
              <div
                className="dropdown-menu bg-dark"
                aria-labelledby="dropdownMenuLink"
              >
                <a className="dropdown-item" href="#">
                  Action
                </a>
                <a className="dropdown-item" href="#">
                  Another action
                </a>
                <a className="dropdown-item" href="#">
                  Something else here
                </a>
              </div>
            </div>
          </div>
          <a href="#" className="MenuRow sanpham pt-2 pb-2">
            Thuộc tính
          </a>
          <div className="danhmuc-thuonghieu">
            <div className="dropdown">
              <a
                className="btn MenuRow dropdown-toggle"
                href="#"
                role="button"
                id="dropdownMenuLink"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                Nhà cung cấp
              </a>
              <div
                className="dropdown-menu bg-dark"
                aria-labelledby="dropdownMenuLink"
              >
                <a className="dropdown-item" href="#">
                  Action
                </a>
                <a className="dropdown-item" href="#">
                  Another action
                </a>
                <a className="dropdown-item" href="#">
                  Something else here
                </a>
              </div>
            </div>
          </div> */}
        </div>
        <div className="col-md-10 c-12 menu_phai">
          <ul className="nav nav-tabs mt-2">
            <li className="nav-item">
              <a
                style={{ color: "#343a40" }}
                className="nav-link MenuRow1 active"
                href="/Productstore"
              >
                <i className="fa fa-bars pr-2" />
                Tất cả
              </a>
            </li>
            {/* <li className="nav-item">
              <a
                style={{ color: "#343a40" }}
                className="nav-link MenuRow"
                href="/Pricefixes"
              >
                {" "}
                <i className="fa fa-edit pr-2" />
                Thông Tin Chỉnh Sửa
              </a>
            </li>

            <li className="nav-item">
              <a
                style={{ color: "#343a40" }}
                className="nav-link MenuRow"
                href="/DeletehistoryProduct"
                tabIndex={-1}
                aria-disabled="true"
              >
                <i className="fa fa-trash pr-2" />
                Lịch sử xóa
              </a>
            </li> */}
          </ul>
          <div className="themmoi mt-3 mb-3 ml-2 pr-4">
            <NavLink to="/addProduct" className="btn btn-outline-danger">
              THÊM MỚI
              <i className="fa fa-plus pl-1" />
            </NavLink>

            <div
              style={{ float: "right" }}
              className="btn-group pr-5"
              role="group"
              aria-label="Basic mixed styles example"
            >
              
             
            </div>
            <div
              style={{ float: "right" }}
              className="navbar-text text-right pr-3 pt-0"
            >
              <input
                type="text"
                value={query}
                onChange={handleSearch}
                className="form-control search "
                name=""
                id=""
                aria-describedby="helpId"
                placeholder="Search"
              />
            </div>
          </div>

          <div className="table-responsive-sm mb-4 pb-5">
            <table className="table table-hover table-responsive-sm">
              <thead className="thead">
                <tr className="text-center">
                  <th scope="col">ID</th>
                  <th scope="col">Ảnh</th>
                  <th scope="col">Tên sản phẩm</th>
                  <th scope="col">Ngành Hàng</th>
                  <th scope="col">Giá bán</th>
                  <th scope="col">Tổng tồn</th>
                  <th scope="col">Chỉnh sửa</th>
                </tr>
              </thead>
              <tbody>
                <>
                  {books.map((book, index) => {
                    if (books.length === index + 1) {
                      return (
                        <tr key={makeid(10)} className="text-center">
                          <th scope="row">{index+1}</th>
                          <td className="text-center">
                            <img
                              style={{ width: "45px" }}
                              src={book.photos}
                              arl="anh"
                            ></img>
                          </td>
                          <td ref={lastBookElementRef} key={book._id}>
                            {book.name}
                          </td>
                          <td>{getTag(book.tag)}</td>
                          {/* <td>{book.price}</td> */}
                          <td><NumberFormat value={book.price} displayType={'text'} thousandSeparator={true}/>VNĐ</td>

                          
                          <td>{book.quantitySold}</td>
                          <td className="text-center">
                            <div className="btn-group">
                              <div className="btn btn-warning">
                                <NavLink to={`/EditProduct/${book._id}`}>
                                  <i
                                    className="fa fa-pencil"
                                    aria-hidden="true"
                                  />
                                  Sửa
                                </NavLink>
                              </div>
                              <button
                                type="button"
                                className="btn btn-danger" aria-hidden="true"
                                data-toggle="modal"
                                data-target="#exampleModal"
                                onClick={() =>
                                  (document.getElementById(
                                    "dataProductID"
                                  ).value = book._id)
                                }
                              >
                                Xoá
                              </button>
                            </div>
                          </td>
                        </tr>
                      );
                    } else {
                      return (
                        <tr key={makeid(10)} className="text-center">
                          <th scope="row">{index+1}</th>
                          <td className="text-center">
                            <img
                              style={{ width: "45px" }}
                              src={book.photos}
                              arl="anh"
                            ></img>
                          </td>
                          <td ref={lastBookElementRef} key={book._id}>
                            {book.name}
                          </td>
                          <td>{getTag(book.tag)}</td>
                          <td><NumberFormat value={book.price} displayType={'text'} thousandSeparator={true}/>VNĐ</td>
                          <td>{book.amount}</td>
                          <td className="text-center">
                            <div className="btn-group">
                              <div className="btn btn-warning">
                                <NavLink to={`/EditProduct/${book._id}`}>
                                  <i
                                    className="fa fa-pencil"
                                    aria-hidden="true"
                                  />
                                  Sửa
                                </NavLink>
                              </div>
                              <div></div>
                              <button
                                type="button"
                                className="btn btn-danger" aria-hidden="true"
                                data-toggle="modal"
                                data-target="#exampleModal"
                                onClick={() =>
                                  (document.getElementById(
                                    "dataProductID"
                                  ).value = book._id)
                                }
                              >
                                Xoá
                              </button>
                            </div>
                          </td>
                        </tr>
                      );
                    }
                  })}
                  <div>{loading && "Loading..."}</div>
                  <div>{error && "Error"}</div>
                </>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      {/* Modal */}
      <Modal_delete productID={productID}></Modal_delete>
      {/* Modal */}
      <Footer></Footer>
    </div>
  );
}

export default Productstore;
