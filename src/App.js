import './App.css';
import React, { useEffect, useState } from "react";
import axios from "axios";
import { API } from "./config/ConfigENV";
import { getBrowserToken, messaging } from "./firebase/Firebase";
import Dieuhuong from "./router/Router";
import { Provide } from './ContextApi/Provide'
import {
  BrowserRouter as Router,
} from "react-router-dom";

let timeout

function App() {
  const token = localStorage.getItem("access_token");
  const [isShow, setIsShow] = useState(false);
  const [contentMessage, setContentMessage] = useState({
    title: '',
    body: ''
  })

  getBrowserToken().then((data) => {
    localStorage.setItem('token_fcm', data);
  });

  useEffect(() => {

    messaging.onMessage((payload) => {
      if (timeout) clearTimeout(timeout);
      setIsShow(true)
      setContentMessage({
        title: payload.notification.title,
        body: payload.notification.body
      })
      timeout = setTimeout(() => {
        setIsShow(false)
      }, 4000)
    console.log('payload', payload);
    });

  }, []);

  return (
   <>
    <div className="alert-notify bg-white MenuRow p-5 shadow p-5 mb-5 rounded" style={isShow ? { display: 'block' } : { display: 'none'}}>
    <div className="msg">{contentMessage.title}</div>
    <div>{contentMessage.body}</div>
  </div>
    <Router>
      <div className="App">
       <Provide>
       <Dieuhuong/>
       </Provide>
      </div>
    </Router>

 </>
  );
}

export default App;
