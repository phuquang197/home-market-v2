import firebase from "firebase";
import 'firebase/storage'

const firebaseConfig =  {
  apiKey: "AIzaSyCR2FEiueWED6JIdrnSneAKE21an33oGVM",
  authDomain: "capstone2-24fd3.firebaseapp.com",
  databaseURL: "https://capstone2-24fd3-default-rtdb.firebaseio.com",
  projectId: "capstone2-24fd3",
  storageBucket: "capstone2-24fd3.appspot.com",
  messagingSenderId: "569887834310",
  appId: "1:569887834310:web:28a25710c09ba7688557c7",
  measurementId: "G-YVV7JJ7J9T"
};

firebase.initializeApp(firebaseConfig);

const storage = firebase.storage()
const messaging = firebase.messaging();

export const getBrowserToken = async () => {
  try {
    // tslint:disable-next-line:no-console

     const data = await messaging.getToken({ vapidKey: 'BIXQprj8LYKJ--MW1-Ise0e0WgwzMfX4R1FUep9AANLcQudgNkaB_E2rYcOIGyZJDhm1_bAYB6ITtJVVjDgMBhQ' }).then((currentToken) => {
        if (currentToken) {
          console.log("currentToken:  " + currentToken);
          // Send the token to your server and update the UI if necessary
          // ...
          return currentToken
        } else {
          // Show permission request UI
          console.log('No registration token available. Request permission to generate one.');
          // ...
        }

      }).catch((err) => {
        console.log('An error occurred while retrieving token. ', err);
        // ...
      });

      return data

  } catch (error) {
    return;
  }

};

export  {
    storage, firebase as default, messaging
}

